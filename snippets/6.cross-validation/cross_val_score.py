import os
project_path="/Users/caiooliveira/Projects/template_python_project/"
current_section="snippets/0.importing/"
os.chdir(project_path+current_section)
import functions

# Importing the libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

#importing data
filename = '../0.importing/creditcard_classification.py'
functions.callfile(filename)
exec(open(filename).read())
dataframe.head()

X = dataframe.iloc[:, 0:29];functions.view(X)
y = dataframe.iloc[:, 30];functions.view(y)

# Splitting the dataset into the Training set and Test set
from sklearn.cross_validation import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state = 0)


#Logistic Regression
from sklearn.linear_model import LogisticRegression
classifier = LogisticRegression(random_state = 0)

#cross validation
from sklearn.model_selection import cross_val_score
scores = cross_val_score(classifier, X_train, y_train, cv=5)
print(scores)