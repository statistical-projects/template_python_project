X = [[0], [1], [2], [3]]
y = [0, 0, 1, 1]

from sklearn.neighbors import KNeighborsRegressor
classifier = KNeighborsRegressor(n_neighbors=2)
classifier.fit(X, y) 

y_pred = classifier.predict([[1.5]])
print(y_pred)