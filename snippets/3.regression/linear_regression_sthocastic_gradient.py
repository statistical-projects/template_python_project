from sklearn.svm import SVC
import matplotlib.pyplot as plt
import numpy as np
from sklearn import linear_model
from sklearn.metrics import mean_squared_error, r2_score



from sklearn.linear_model import SGDClassifier
SGDC = SGDClassifier(penalty='l2', alpha=10**(-6), learning_rate='constant', eta0=0.1, tol=10**(-3), loss='log', average = False, random_state=0)
SGDC.fit(X_train, y_train)



accur_treino = SGDC.score(X_train, y_test)
accur_teste = SGDC.score(X_train, y_test)
print("Accuracy - Treino:", accur_treino)
print("Accuracy - Teste:", accur_teste)