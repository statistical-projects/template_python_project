###############################

########
# LASSO
########

features = [[0,0], [1, 1], [2, 2]]
target = [0, 1, 2]

from sklearn import linear_model
classifier = linear_model.Lasso(alpha=0.1)
classifier.fit(features, target)

print(classifier.coef_)
print(classifier.intercept_)

###############################

########
# RIGDE
########

features = [[0,0], [1, 1], [2, 2]]
target = [0, 1, 2]

from sklearn import linear_model
classifier = linear_model.Ridge(alpha=0.1)
classifier.fit(features, target)

print(classifier.coef_)
print(classifier.intercept_)

###############################

########
# ELASTIC NET
########

features = [[0,0], [1, 1], [2, 2]]
target = [0, 1, 2]

from sklearn import linear_model
classifier = linear_model.ElasticNet(alpha =1.0, l1_ratio = 0.5)
classifier.fit(features, target)

print(classifier.coef_)
print(classifier.intercept_)