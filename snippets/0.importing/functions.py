import numpy as np
import matplotlib.pyplot as plt
import pandas as pd


def callfile(fn):
    print(fn)
    exec(open(fn).read(), globals(), globals())


def view(pdarray):
    print(pd.DataFrame(pdarray).head())