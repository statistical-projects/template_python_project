import os
project_path="/Users/caiooliveira/Projects/template_python_project/snippets/"
os.chdir(project_path)
#import functions
exec(open('./0.importing/functions.py').read())

#importing data
filename = './0.importing/creditcard_classification.py'
callfile(filename)

x_train_list, y_train_list, x_test_list, y_test_list = list(), list(), list(), list()

from sklearn.model_selection import StratifiedShuffleSplit
kf = StratifiedShuffleSplit(n_splits=10, test_size=0.33, random_state=0)
kf.get_n_splits(X)
print(kf)

from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import f1_score

accuracy_scores = []
precision_scores = []
recall_scores = []
f1_scores = []

from sklearn.linear_model import LogisticRegression
classifier = LogisticRegression(random_state = 0)

for train_index, test_index in kf.split(X_df,y_df):
    print("TRAIN:", train_index, "TEST:", test_index)
    X_train, X_test = X_df.iloc[train_index], X_df.iloc[test_index]
    y_train, y_test = y_df.iloc[train_index], y_df.iloc[test_index]
    x_train_list.append(X_train);x_test_list.append(X_test);y_train_list.append(y_train);y_test_list.append(y_test)

    classifier.fit(X_train, y_train)
    y_pred = classifier.predict(X_test)
    
    accuracy_scores.append(accuracy_score(y_test, y_pred))
    precision_scores.append(precision_score(y_test, y_pred))
    recall_scores.append(recall_score(y_test, y_pred))
    f1_scores.append(f1_score(y_test, y_pred))


#y_train_list[2].describe() == y_train_list[4].describe()
x_train_list[1].describe() == x_train_list[2].describe()

#y_test_list[2].describe() == y_test_list[4].describe()
x_test_list[1].describe() == x_test_list[4].describe()


print('Accuracy', np.mean(accuracy_scores))
print('Precision', np.mean(precision_scores))
print('Recall', np.mean(recall_scores))
print('F1-measure', np.mean(f1_scores)) 

Accuracy 0.9989805166478536
Precision 0.7743011722010986
Recall 0.587037037037037
F1-measure 0.6633416222255518