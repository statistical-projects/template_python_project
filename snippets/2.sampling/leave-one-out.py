import os
project_path="/Users/caiooliveira/Projects/template_python_project/snippets/"
os.chdir(project_path)
#import functions
exec(open('./0.importing/functions.py').read())

#importing data
filename = './0.importing/creditcard_classification.py'
callfile(filename)

#define modelo

from sklearn.linear_model import LogisticRegression
classifier = LogisticRegression(random_state = 0)

#leave one out

X_train = X.iloc[0:900,:]
X_test = X.iloc[900:,:] 
y_train = y.iloc[0:900] 
y_test = y.iloc[900:]

X_train.describe()

X_df = X_train
y_df = y_train
X_validation = X_test
y_validation = y_test

from sklearn.model_selection import LeaveOneOut
lou = LeaveOneOut()
lou.get_n_splits(X_df)
print(lou)

from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import f1_score
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import roc_auc_score
from sklearn.metrics import r2_score
from statsmodels import robust #use MAD
from scipy.stats import ks_2samp #ks & pvalor

train_accuracy_scores = []
train_precision_scores = []
train_recall_scores = []
train_f1_scores = []
train_ks_scores = []
train_pvalue_scores = []
train_roc_auc_scores = []
train_auc_scores = []
test_accuracy_scores = []
test_precision_scores = []
test_recall_scores = []
test_f1_scores = []
test_ks_scores = []
test_pvalue_scores = []
test_roc_auc_scores = []
test_auc_scores = []
train_mae_scores = []
train_mse_scores = []
train_rmse_scores = []
train_mad_scores = []
train_r2_scores = []
test_mae_scores = []
test_mse_scores = []
test_rmse_scores = []
test_mad_scores = []
test_r2_scores = []
x_train_list, y_train_list, x_test_list, y_test_list = list(), list(), list(), list()

for train_index, test_index in lou.split(X_df):
    #print("TRAIN:", train_index, "TEST:", test_index)
    X_train_cross, X_test_cross = X_df.iloc[train_index], X_df.iloc[test_index]
    y_train_cross, y_test_cross = y_df.iloc[train_index], y_df.iloc[test_index]
    x_train_list.append(X_train_cross);x_test_list.append(X_test_cross);y_train_list.append(y_train_cross);y_test_list.append(y_test_cross)

    response = classifier.fit(X_train_cross, y_train_cross)
    
    y_pred_train_cross = classifier.predict(X_train_cross)
    y_pred_test_cross = classifier.predict(X_test_cross)

    # for classification problems
    
    train_accuracy_scores.append(accuracy_score(y_train_cross, y_pred_train_cross))
    train_precision_scores.append(precision_score(y_train_cross, y_pred_train_cross))#, average="micro"))
    train_recall_scores.append(recall_score(y_train_cross, y_pred_train_cross))#, average="micro"))
    train_f1_scores.append(f1_score(y_train_cross, y_pred_train_cross))#, average="micro"))
    train_ks_scores.append(ks_2samp(np.array(y_train_cross.iloc[:,0]), y_pred_train_cross).statistic)
    train_pvalue_scores.append(ks_2samp(np.array(y_train_cross.iloc[:,0]), y_pred_train_cross).pvalue)
    train_roc_auc_scores.append(roc_auc_score(y_train_cross.values.ravel(), y_pred_train_cross))

    test_accuracy_scores.append(accuracy_score(y_test_cross, y_pred_test_cross))
    test_precision_scores.append(precision_score(y_test_cross, y_pred_test_cross))#, average="micro"))
    test_recall_scores.append(recall_score(y_test_cross, y_pred_test_cross))#, average="micro"))
    test_f1_scores.append(f1_score(y_test_cross, y_pred_test_cross))#, average="micro"))
    test_ks_scores.append(ks_2samp(np.array(y_test_cross.iloc[:,0]), y_pred_test_cross).statistic)
    test_pvalue_scores.append(ks_2samp(np.array(y_test_cross.iloc[:,0]), y_pred_test_cross).pvalue)
    test_roc_auc_scores.append(roc_auc_score(y_test_cross.values.ravel(), y_pred_test_cross))

    # for linear problems

    train_mae_scores.append(mean_absolute_error(y_train_cross, y_pred_train_cross))
    train_mse_scores.append(mean_squared_error(y_train_cross, y_pred_train_cross))
    train_rmse_scores.append(np.sqrt(mean_squared_error(y_train_cross, y_pred_train_cross)))
    train_mad_scores.append(robust.mad(y_train_cross, y_pred_train_cross))
    train_r2_scores.append(r2_score(y_train_cross, y_pred_train_cross))

    test_mae_scores.append(mean_absolute_error(y_test_cross, y_pred_test_cross))
    test_mse_scores.append(mean_squared_error(y_test_cross, y_pred_test_cross))
    test_rmse_scores.append(np.sqrt(mean_squared_error(y_test_cross, y_pred_test_cross)))
    test_mad_scores.append(robust.mad(y_test_cross, y_pred_test_cross))
    test_r2_scores.append(r2_score(y_test_cross, y_pred_test_cross))
    

## classification
print('train Accuracy', np.mean(train_accuracy_scores))
print('train Precision', np.mean(train_precision_scores))
print('train Recall', np.mean(train_recall_scores))
print('train F1-measure', np.mean(train_f1_scores))
print('------------')
print('test Accuracy', np.mean(test_accuracy_scores))
print('test Precision', np.mean(test_precision_scores))
print('test Recall', np.mean(test_recall_scores))
print('test F1-measure', np.mean(test_f1_scores)) 

## regression
print('train mae', np.mean(train_mae_scores))
print('train mse', np.mean(train_mse_scores))
print('train rmse', np.mean(train_rmse_scores))
print('train mad', np.mean(train_mad_scores))
print('train r2', np.mean(train_r2_scores))
print('------------')
print('test mae', np.mean(test_mae_scores))
print('test mse', np.mean(test_mse_scores))
print('test rmse', np.mean(test_rmse_scores))
print('train mad', np.mean(test_mad_scores))
print('test r2', np.mean(test_r2_scores))


print('diferencas treino')
#y_train_list[2].describe() == y_train_list[4].describe()
x_train_list[1].describe() == x_train_list[2].describe()

print('diferencas teste')
#y_test_list[2].describe() == y_test_list[4].describe()
x_test_list[1].describe() == x_test_list[4].describe()