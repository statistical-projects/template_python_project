import numpy as np

def euclidean_distance(p, q):
    if(len(p)!=len(q)):
        print('problema nos pontos propostos, eles têm tamanho diferente')
        exit
    return np.sqrt(np.sum(np.subtract(p,q)**2))