# Aplicacao da formula de least squares (minimos quadrados)
# com a tecnica de regressao linear

import numpy as np
import matplotlib.pyplot as plt

x = [1,2,-1]
y = [1,5,-2]

X = sum(x)/len(x)
Y = sum(y)/len(y)

m = np.dot( np.subtract(x, X), np.subtract(y, Y) )/np.dot(np.subtract(x , X), np.subtract(x , X))
b = Y - m * X

m
b

abline_values = [m * i + b for i in x]

# Plot the best fit line over the actual values
plt.plot(x, y, 'ro')
plt.plot(x, abline_values, 'b')
plt.title(slope)
plt.show(block=False)
input('press <ENTER> to continue')