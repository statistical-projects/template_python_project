import os
project_path="/Users/caiooliveira/Projects/template_python_project/snippets/"
os.chdir(project_path)
#import functions
exec(open('raw_scripts/distancia_euclideana.py').read())
import numpy as np
import statistics


# definitions
# number of k

k = 3

# data points
# known datapoints
x = [9,5,4,7,8,3,2,1,8]
y = [2,5,4,7,8,5,4,7,6]
g = ['a', 'b', 'b', 'a', 'a', 'b', 'b', 'b', 'a']
# unknown datapoints
X = [4,5]
Y = [2,9]
G = {}

# get all the possible values for the factor variable
factors = list(set(g))

# steps
# 1. similarity
# to compute the distances of all the datapoints
# choose the k nearests points
# make a voting to select the mod of them
i=0
neighbors = {}
classes = {}
for i in range(0,len(X)):
    distances = {}
    for o in range(0,len(x)):
        distance = euclidean_distance(
            [x[o], y[o]], [X[i],Y[i]]
        )
        distances[o] = distance

    sorted = sorted(distances, key=distances.get)
    neighbors[i] = []
    neighbors[i] = sorted[:3]
    del distances
    del sorted

    classes_t = []
    for p in range(0,len(neighbors[i])):
        classes_t.append(
            factors.index(
                g[ neighbors[i][p] ]
            )
        )
    
    classes[i] = []
    classes[i] = classes_t
    del classes_t
    G[i] = factors[statistics.mode(classes[i])]