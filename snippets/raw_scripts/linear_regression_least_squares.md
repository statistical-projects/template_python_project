# Line of Best Fit (Least Square Method)

Segue abaixo as formulas para fazer o minimos quadrados

## Step 1 - encontrar os X e Y

$$
\overline{X} = {
    \sum \limits_{i = 1}^{n} {x_{i}}
    \over
    n
}$$

$$
\overline{Y} = {
    \sum \limits_{i = 1}^{n} {y_{i}}
    \over
    n
}$$

## Step 2 - encontrar calcular o coeficiente de inclinacao e o bias

Da formula da reta
$$
y = ax + b$$
Temos as variaveis `a` e `b`, sendo `a` o coeficiente de inclinacao e `b` a altura da reta

Calculamos entao `a` abaixo:

$$
m = {
    \sum \limits_{i = 1}^{n} (x_i - \overline{X})(y_i - \overline{Y})\over
    \sum \limits_{i = 1}^{n} (x_i - \overline{X})^2
}$$

E logo abaixo calculamos o `b`
$$
b = {
    \overline{Y} - m \overline{X}
}$$