# Euclidean Distance

Segue abaixo as formulas da distancia euclideana

## bi dimensional

$$
d(p,q) = d(q,p) = \sqrt{(p_x-q_x)^2 + (p_y-q_y)^2}
$$

## n dimensional

$$
d(p,q) = d(q,p) = \sqrt{(p_1-q_1)^2 + ... + (p_n-q_n)^2}
$$