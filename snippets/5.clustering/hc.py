# Hierarchical Clustering

import matplotlib.pyplot as plt
# Importing the libraries
import numpy as np
import pandas as pd
### model 02 - scipy
import scipy
# Using the dendrogram to find the optimal number of clusters
import scipy.cluster.hierarchy as sch
from matplotlib import pyplot as plt
from scipy.cluster.hierarchy import dendrogram
### model 01 - sklearn
from sklearn.cluster import AgglomerativeClustering
from sklearn.datasets import load_iris

# Importing the dataset
dataset = pd.read_csv('Mall_Customers.csv')
X = dataset.iloc[:, [3, 4]].values
# y = dataset.iloc[:, 3].values

# Splitting the dataset into the Training set and Test set
"""from sklearn.cross_validation import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state = 0)"""

# Feature Scaling
"""from sklearn.preprocessing import StandardScaler
sc_X = StandardScaler()
X_train = sc_X.fit_transform(X_train)
X_test = sc_X.transform(X_test)
sc_y = StandardScaler()
y_train = sc_y.fit_transform(y_train)"""


#opcao um - scipy
import scipy.cluster.hierarchy as sch
from matplotlib import pyplot as plt
L = sch.linkage(X, method = 'ward')
# description of HC
#|      1      |       2      |     3    |                    4                   |
#|-------------|--------------|----------|----------------------------------------|
#| cluster_01  |  cluster_02  | distance | how many singletons are in the cluster |
d = sch.dendrogram(L)
plt.title('Dendrogram')
plt.xlabel('Customers')
plt.ylabel('Euclidean distances')
plt.show()

ind = sch.fcluster(L, 5, 'distance')
#Quantos grupos são obtidos?
print("sao obtidos ", len(set(ind)), "clusters")


#opcao dois sklearn
# Fitting Hierarchical Clustering to the dataset
hc = AgglomerativeClustering(n_clusters = 5, affinity = 'euclidean', linkage = 'ward')
y_hc = hc.fit_predict(X)
L = sch.linkage(clust_df, method='complete')




# Visualising the clusters
plt.scatter(X[y_hc == 0, 0], X[y_hc == 0, 1], s = 100, c = 'red', label = 'Cluster 1')
plt.scatter(X[y_hc == 1, 0], X[y_hc == 1, 1], s = 100, c = 'blue', label = 'Cluster 2')
plt.scatter(X[y_hc == 2, 0], X[y_hc == 2, 1], s = 100, c = 'green', label = 'Cluster 3')
plt.scatter(X[y_hc == 3, 0], X[y_hc == 3, 1], s = 100, c = 'cyan', label = 'Cluster 4')
plt.scatter(X[y_hc == 4, 0], X[y_hc == 4, 1], s = 100, c = 'magenta', label = 'Cluster 5')
plt.title('Clusters of customers')
plt.xlabel('Annual Income (k$)')
plt.ylabel('Spending Score (1-100)')
plt.legend()
plt.show()

#visualising the dendogram


def plot_dendrogram(model, **kwargs):
    # Children of hierarchical clustering
    children = model.children_
    # Distances between each pair of children
    # Since we don't have this information, we can use a uniform one for plotting
    distance = np.arange(children.shape[0])
    # The number of observations contained in each cluster level
    no_of_observations = np.arange(2, children.shape[0]+2)
    # Create linkage matrix and then plot the dendrogram
    linkage_matrix = np.column_stack([children, distance, no_of_observations]).astype(float)
    # Plot the corresponding dendrogram
    dendrogram(linkage_matrix, **kwargs)

plt.title('Hierarchical Clustering Dendrogram')
plot_dendrogram(hc, labels=hc.labels_)
plt.show()
