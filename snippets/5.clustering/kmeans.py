# K-Means Clustering

# Importing the libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

# Importing the dataset
dataset = pd.read_csv('Mall_Customers.csv')
X = dataset.iloc[:, [3, 4]].values
# y = dataset.iloc[:, 3].values

# Splitting the dataset into the Training set and Test set
"""from sklearn.cross_validation import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state = 0)"""

# Feature Scaling
"""from sklearn.preprocessing import StandardScaler
sc_X = StandardScaler()
X_train = sc_X.fit_transform(X_train)
X_test = sc_X.transform(X_test)
sc_y = StandardScaler()
y_train = sc_y.fit_transform(y_train)"""

# Using the elbow method to find the optimal number of clusters
from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_samples, silhouette_score

wcss = [] #elbow
silhouette = []
#homogeneity = []
#completeness = []
#v_measure = []
#adjusted_rand = []
#adjusted_mutual_info = []

for i in range(2, 11):
    kmeans = KMeans(
        n_clusters = i,
        init = 'k-means++',
        #init = np.array([[1, 2, 3, 4], [-5, -6, -7, -8], [9, -10, 11, -12]]),
        #max_iter = 10,
        n_init=1
    )
    cluster_labels = kmeans.fit_predict(X)
    wcss.append(kmeans.inertia_)
    silhouette.append(silhouette_score(X, cluster_labels))
    #homogeneity.append(metrics.homogeneity_score(X, cluster_labels))
    #completeness.append(metrics.completeness_score(X, cluster_labels))
    #v_measure.append(metrics.v_measure_score(X, cluster_labels))
    #adjusted_rand.append(metrics.adjusted_rand_score(X, cluster_labels))
    #adjusted_mutual_info.append(metrics.adjusted_mutual_info_score(X, cluster_labels))

    
plt.plot(range(2, 11), wcss)
plt.title('The Elbow Method')
plt.xlabel('Number of clusters')
plt.ylabel('WCSS')
plt.show()

plt.plot(range(2, 11), silhouette)
plt.title('The silhouette Method')
plt.xlabel('Number of clusters')
plt.ylabel('silhouette')
plt.show()

''''
plt.plot(range(2, 11), homogeneity)
plt.title('The homogeneity Method')
plt.xlabel('Number of clusters')
plt.ylabel('homogeneity')
plt.show()

plt.plot(range(2, 11), completeness)
plt.title('The completeness Method')
plt.xlabel('Number of clusters')
plt.ylabel('completeness')
plt.show()

plt.plot(range(2, 11), v_measure)
plt.title('The v_measure Method')
plt.xlabel('Number of clusters')
plt.ylabel('v_measure')
plt.show()

plt.plot(range(2, 11), adjusted_rand)
plt.title('The adjusted_rand Method')
plt.xlabel('Number of clusters')
plt.ylabel('adjusted_rand')
plt.show()

plt.plot(range(2, 11), adjusted_mutual_info)
plt.title('The adjusted_mutual_info Method')
plt.xlabel('Number of clusters')
plt.ylabel('adjusted_mutual_info')
plt.show()
''''

# Fitting K-Means to the dataset
kmeans = KMeans(n_clusters = 5, init = 'k-means++', random_state = 42)
y_kmeans = kmeans.fit_predict(X)

# Visualising the clusters
plt.scatter(X[y_kmeans == 0, 0], X[y_kmeans == 0, 1], s = 100, c = 'red', label = 'Cluster 1')
plt.scatter(X[y_kmeans == 1, 0], X[y_kmeans == 1, 1], s = 100, c = 'blue', label = 'Cluster 2')
plt.scatter(X[y_kmeans == 2, 0], X[y_kmeans == 2, 1], s = 100, c = 'green', label = 'Cluster 3')
plt.scatter(X[y_kmeans == 3, 0], X[y_kmeans == 3, 1], s = 100, c = 'cyan', label = 'Cluster 4')
plt.scatter(X[y_kmeans == 4, 0], X[y_kmeans == 4, 1], s = 100, c = 'magenta', label = 'Cluster 5')
plt.scatter(kmeans.cluster_centers_[:, 0], kmeans.cluster_centers_[:, 1], s = 300, c = 'yellow', label = 'Centroids')
plt.title('Clusters of customers')
plt.xlabel('Annual Income (k$)')
plt.ylabel('Spending Score (1-100)')
plt.legend()
plt.show()