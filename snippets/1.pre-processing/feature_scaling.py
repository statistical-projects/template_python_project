import os
project_path="/Users/caiooliveira/Projects/template_python_project/snippets/"
os.chdir(project_path)
#import functions
exec(open('./0.importing/functions.py').read())

################
# Feature Scaling
# StandardScaler
################

# metodo 1 - fazendo para o dataset completo
#importing data
filename = './0.importing/creditcard_classification.py'
callfile(filename)
#realizando feature scaling
from sklearn.preprocessing import StandardScaler
sc_X = StandardScaler()
X = sc_X.fit_transform(X)
sc_y = StandardScaler()
y = sc_y.fit_transform(y.reshape(-1, 1))

# metodo 2 - fazendo para o train separado do testset
#importing data
filename = '../0.importing/creditcard_classification.py'
callfile(filename)
# Splitting the dataset into the Training set and Test set
from sklearn.cross_validation import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state = 0)
#realizando feature scaling
from sklearn.preprocessing import StandardScaler
sc_X = StandardScaler()
X_train = sc_X.fit_transform(X_train)
X_test = sc_X.transform(X_test)
sc_y = StandardScaler()
y_train = sc_y.fit_transform(y_train.reshape(-1, 1))
y_test = sc_y.transform(y_test.reshape(-1, 1))

################
# Feature Scaling
# MinMaxScaler
################

from sklearn.preprocessing import MinMaxScaler
scaler = MinMaxScaler()
X_train_scaled = scaler.fit_transform(X_train)
# we must apply the scaling to the test set that we computed for the training set
X_test_scaled = scaler.transform(X_test)