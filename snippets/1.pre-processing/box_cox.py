from scipy import stats
import numpy as np

data = np.fromstring('3.53    0 4.93    50 5.53    60 6.21    70 7.37    80 9.98    90 16.56   100', sep=' ').reshape(7, 2)

stats.boxcox(data[0,])
#(array([ 0.91024309,  1.06300488,  1.10938333,  1.15334193,  1.213348 ,1.30668122,  1.43178909]), -0.54874593147877893)