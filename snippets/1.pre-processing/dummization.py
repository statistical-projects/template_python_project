import os
project_path="/Users/caiooliveira/Projects/template_python_project/snippets/"
os.chdir(project_path)
#import functions
exec(open('./0.importing/functions.py').read())

#importing data
filename = './0.importing/startups_regression.py'
callfile(filename)

#############################
# dummization
#############################
# Encoding categorical data
# Encoding the Independent Variable
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
labelencoder_X = LabelEncoder()
var = 3 # <------- select the variable you want to create dummies
X[:, var] = labelencoder_X.fit_transform(X[:, var])


onehotencoder = OneHotEncoder(categorical_features = [var])
X = onehotencoder.fit_transform(X).toarray()
#TODO
# >qual a necessidade de transformar a variavel resposta?
# >isso nao pode fazer quando o resultado é regressivo certo? ou seja, vamos descobrir o valor da casa (exemplo)
# Encoding the Dependent Variable
labelencoder_y = LabelEncoder()
y = labelencoder_y.fit_transform(y)

functions.view(y)



## solucao mais eficaz

def create_dummies( df, colname ):
    col_dummies = pd.get_dummies(df[colname], prefix=colname)
    col_dummies.drop(col_dummies.columns[0], axis=1, inplace=True)
    df = pd.concat([df, col_dummies], axis=1)
    df.drop( colname, axis = 1, inplace = True )
    return df