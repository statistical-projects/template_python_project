import os
project_path="/Users/caiooliveira/Projects/template_python_project/snippets/"
os.chdir(project_path)
#import functions
exec(open('./0.importing/functions.py').read())

#importing data
filename = './0.importing/creditcard_classification.py'
callfile(filename)

#############################
# Taking care of missing data
#############################

#pandas
df = df.fillna(value=0)

#imputer
from sklearn.preprocessing import Imputer
imputer = Imputer(missing_values = 'NaN', strategy = 'mean', axis = 0)
imputer = imputer.fit(X[:,:])
X[:,:] = imputer.transform(X[:,:])