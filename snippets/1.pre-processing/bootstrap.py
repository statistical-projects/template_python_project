def bootstrap(input_array, bin_size, n_samples):
    from sklearn.utils import resample
    import statistics
    
    result_bootstrap = list()

    for i in range(n_samples):
        current_value = statistics.median(resample(input_array, n_samples = bin_size))
        result_bootstrap.append(current_value)

    result_bootstrap = pd.DataFrame(result_bootstrap)[0]
    return result_bootstrap

SalePrice = np.array(df['SalePrice'])
SalePrice_bootstrap = bootstrap(SalePrice, 100, len(SalePrice))
SalePrice_bootstrap.describe()

plt.hist(SalePrice_bootstrap)
plt.show()


## ======
## para dataframes

def bootstrap(input_df, n_samples = 0, bin_size = 0):
    from sklearn.utils import resample
    import statistics
    
    if(n_samples == 0):
        n_samples = len(input_df)
    if(bin_size == 0):
        bin_size = round(len(input_df) * 0.1)
    
    result_bootstrap = list()

    for i in range(n_samples):
        current_value = pd.DataFrame(resample(input_df.values, n_samples = bin_size)).describe()[1:2].values[0]
        result_bootstrap.append(current_value)

    result_bootstrap = pd.DataFrame(result)
    return result_bootstrap