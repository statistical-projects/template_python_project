# Import necessary modules
import keras
from keras.layers import Dense
from keras.models import Sequential
from keras.utils import to_categorical








# Convert the target to categorical: target
target = to_categorical(df.survived)

# Specify, compile, and fit the model
model = Sequential()
model.add(Dense(32, activation='relu', input_shape = (n_cols,)))
model.add(Dense(2, activation='softmax'))
model.summary()
model.compile(optimizer='sgd', #stochastic gradient descent
              loss='categorical_crossentropy', 
              metrics=['accuracy'])
model.fit(predictors, target, batch_size = 128, epochs=30, validation_split=0.3, verbose=True)

# Calculate predictions: predictions
predictions = model.predict(pred_data)











# ==============================================
# ========= testing new learning rates =========
# ==============================================



from keras.optimizers import SGD

def get_new_model(input_shape = input_shape):
    model = Sequential()
    model.add(Dense(100, activation='relu', input_shape = input_shape))
    model.add(Dense(100, activation='relu'))
    model.add(Dense(2, activation='softmax'))
    return(model)

# Create list of learning rates: lr_to_test
lr_to_test = [0.000001, 0.01, 1]

# Loop over learning rates
for lr in lr_to_test:
    print('\n\nTesting model with learning rate: %f\n'%lr )
    # Build new model to test, unaffected by previous models
    model = get_new_model()
    model.summary()
    # Create SGD optimizer with specified learning rate: my_optimizer
    my_optimizer = SGD(lr=lr)
    # Compile the model
    model.compile(optimizer = my_optimizer, loss='categorical_crossentropy', metrics=['accuracy'])
    # Fit the model
    model.fit(predictors, target, batch_size = 128, epochs=30, validation_split=0.3, verbose=True)









# ===============================================
# = setting stop parameter for gradient descent =
# ===============================================


# Import EarlyStopping
from keras.callbacks import EarlyStopping

# Save the number of columns in predictors: n_cols
n_cols = predictors.shape[1]
input_shape = (n_cols,)

# Specify the model
model = Sequential()
model.add(Dense(100, activation='relu', input_shape = input_shape))
model.add(Dense(100, activation='relu'))
model.add(Dense(2, activation='softmax'))
model.summary()

# Compile the model
model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])

# Define early_stopping_monitor
early_stopping_monitor = EarlyStopping(patience=2)

# Fit the model
model_training = model.fit(x=predictors, y=target, batch_size = 128, epochs=30, validation_split=0.3, verbose=True, callbacks=[early_stopping_monitor])

# Create the plot
plt.plot(model_training.history['val_loss'], 'r')
plt.xlabel('Epochs')
plt.ylabel('Validation score')
plt.show()

## https://stackoverflow.com/a/31157729/2200312
## https://machinelearningmastery.com/difference-between-a-batch-and-an-epoch/

#### what is an batch
## ------------------
## The batch size is a hyperparameter that defines the number of samples to work through before 
## updating the internal model parameters.
## 
## Batch Gradient Descent. Batch Size = Size of Training Set 
## Stochastic Gradient Descent. Batch Size = 1
## Mini-Batch Gradient Descent. 1 < Batch Size < Size of Training Set

#### what is an epoch
## ------------------
## The number of epochs is a hyperparameter that defines the number times that the learning algorithm 
## will work through the entire training dataset.

#### general rules
## ------------------
## The size of a batch must be more than or equal to one and less than or equal to the number of samples 
## in the training dataset.
##
## The number of epochs can be set to an integer value between one and infinity. You can run the algorithm 
## for as long as you like and even stop it using other criteria besides a fixed number of epochs, such as 
## a change (or lack of change) in model error over time.