# Python Data Science - master template
    Developed by Caio Oliveira ;)
    
## Structure

The Project has the following structure

- [wiki](wiki/index.md)
- [datasets](dataframes/)
- [snippets](snippets/)

### Wiki

All the concepts and things I have learnt throughout my entire study journey.

### Datasets

The best the most usefull datasets for training and studying

### Source

The best snippets for a fast and efficient way of writing data science projects

Regards,
Caio Oliveira ;)