# Out of time

## General

É uma tecnica de validacao para datasets de linha de tempo (time series).
De forma simplificada, ele segue o mesmo conceito do `out-of-sample` porem eé com um periodo de tempo.

A tecnica `out-of-sample` usa dados fora da amostra de treino para testar seu modelo (`hold-out`)
Na tecnica `out-of-time` simplesmente pegamos o dataset e quebramos em traino e teste.

Geralmente colocamos o treino com pelo menos 12 meses para quebrar sazonalidades.
O teste fica com os periodos mais atuais, de forma que aprendemos com o passado e reaplicamos para o atual.
A partir dai fazemos os `forecasts` com os dados que temos a resposta e usamos alguma metrica de `sample error`, como o MAD/MAE.

![forecast](forecast.png)

#### fonte

http://www.vanguardsw.com/business-forecasting-101/sample-testing-holdout-sample/

## External

https://people.duke.edu/~rnau/three.htm

A good way to test the assumptions of a model and to realistically compare its forecasting performance against other models is to perform out-of-sample validation, which means to withhold some of the sample data from the model identification and estimation process, then use the model to make predictions for the hold-out data in order to see how accurate they are and to determine whether the statistics of their errors are similar to those that the model made within the sample of data that was fitted. In the Forecasting procedure in Statgraphics, you are given the option to specify a number of data points to hold out for validation and a number of forecasts to generate into the future. The data which are not held out are used to estimate the parameters of the model. The model is then tested on data in the validation period, and forecasts are generated beyond the end of the estimation and validation periods. For example, consider a hypothetical time series Y of which a sample of 100 observations is available, as shown in the chart below. Suppose that a random-walk-with-drift model (which is specified as an "ARIMA(0,1,0) with constant" model in Statgraphics) is fitted to this series. If the last 20 values are held out for validation and 12 forecasts for the future are generated, the results look like this:

![forecast](https://people.duke.edu/~rnau/randwkf2.gif)

In general, the data in the estimation period are used to help select the model and to estimate its parameters. Forecasts made in this period are not completely "honest" because data on both sides of each observation are used to help determine the forecast. The one-step-ahead forecasts made in this period are usually called fitted values. (They are said to be "fitted" because our software estimates the parameters of the model so as to "fit" them as well as possible in a mean-squared-error sense.) The corresponding forecast errors are called residuals. The residual statistics (MSE, MAE, MAPE) may understate the magnitudes of the errors that will be made when the model is used to predict the future, because it is possible that the data have been overfitted--i.e, by relentlessly minimizing the mean squared error, the model may have inadvertently fitted some of the "noise" in the estimation period as well as the "signal." Overfitting is especially likely to occur when either (a) a model with a large number of parameters (e.g., a model using seasonal adjustment) has been fitted to a small sample of data and/or (b) the model has been selected from a large set of potential models precisely by minimizing the mean squared error in the estimation period (e.g., when stepwise or all-subsets regression has been used with a large set of potential regressors).

