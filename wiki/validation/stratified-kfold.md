# Stratified K-fold

## General

Vamos dizer que temos um dataset de 100 observacoes, 90 esta na classe `A` e 10 esta na classe `B`.
Nesse exemplo, imagine um 10-fold CV. Voce pode terminar construindo modelos com pouquissimas observacoes contendo a classe b (ou ate com nenhuma).
Nesse caso, encontramos um problema do k-fold. Ele necessita que os dados estejam balanceados.

Para resolver isso existe o `Stratified K-fold`. Ele permite que os grupos sejam gerados porem ele garante que os grupos tenham pessoas de todas as classes.

#### fonte
[why-use-stratified-cross-validation-why-does-this-not-damage-variance-related-b](https://stats.stackexchange.com/questions/117643/why-use-stratified-cross-validation-why-does-this-not-damage-variance-related-b/117649)

## evidencia

Partindo de uma base de cartao de credito

    ./0.importing/creditcard_classification.py
    Time        V1        V2        V3  ...         V27       V28  Amount  Class
    0   0.0 -1.359807 -0.072781  2.536347  ...    0.133558 -0.021053  149.62      0
    1   0.0  1.191857  0.266151  0.166480  ...   -0.008983  0.014724    2.69      0
    2   1.0 -1.358354 -1.340163  1.773209  ...   -0.055353 -0.059752  378.66      0
    3   1.0 -0.966272 -0.185226  1.792993  ...    0.062723  0.061458  123.50      0
    4   2.0 -1.158233  0.877737  1.548718  ...    0.219422  0.215153   69.99      0

    [5 rows x 31 columns]

Executo o codigo para fazer **K-fold normal**

````python
from sklearn.model_selection import KFold
kf = KFold(n_splits=10)
kf.get_n_splits(X_df)
print(kf)

for train_index, test_index in kf.split(X_df):
    print("TRAIN:", train_index, "TEST:", test_index)
    X_train, X_test = X_df.iloc[train_index], X_df.iloc[test_index]
    y_train, y_test = y_df.iloc[train_index], y_df.iloc[test_index]
    x_train_list.append(X_train);x_test_list.append(X_test);y_train_list.append(y_train);y_test_list.append(y_test)
    #model fit
    #model train
    #model evaluation

x_train_list[1].describe() == x_train_list[2].describe()

x_test_list[1].describe() == x_test_list[4].describe()

````

Ao final do codigo realizei dois describes.
O primeiro mostra colunas que estao iguais (ou seja, com zero variancia) da base de treino

    Out[22]: 
            Time     V1     V2     V3   ...      V26    V27    V28  Amount
    count   True   True   True   True   ...     True   True   True    True
    mean   False  False  False  False   ...    False  False  False   False
    std    False  False  False  False   ...    False  False  False   False
    min     True  False  False   True   ...     True   True   True    True
    25%     True  False  False  False   ...    False  False  False   False
    50%     True  False  False  False   ...    False  False  False   False
    75%     True  False  False  False   ...    False  False  False   False
    max     True   True   True   True   ...     True   True  False    True

    [8 rows x 30 columns]

Conclui-se que a base praticamente nao mudou. 
Porem quando olhamos a base de teste, o cenario muda:

    Out[23]: 
            Time     V1     V2     V3   ...      V26    V27    V28  Amount
    count   True   True   True   True   ...     True   True   True    True
    mean   False  False  False  False   ...    False  False  False   False
    std    False  False  False  False   ...    False  False  False   False
    min    False  False  False  False   ...    False  False  False    True
    25%    False  False  False  False   ...    False  False  False   False
    50%    False  False  False  False   ...    False  False  False   False
    75%    False  False  False  False   ...    False  False  False   False
    max    False  False  False  False   ...    False  False  False   False

    [8 rows x 30 columns]

Quando realizamos o `Stratified Kfold`, o cenario que tenho eé que as bases sao mais variaveis, ou seja, como bases independentes:

````python
#importing data
filename = './0.importing/creditcard_classification.py'
callfile(filename)

x_train_list, y_train_list, x_test_list, y_test_list = list(), list(), list(), list()

from sklearn.model_selection import StratifiedKFold
kf = StratifiedKFold(n_splits=10)
kf.get_n_splits(X)
print(kf)

for train_index, test_index in kf.split(X_df,y_df):
    print("TRAIN:", train_index, "TEST:", test_index)
    X_train, X_test = X_df.iloc[train_index], X_df.iloc[test_index]
    y_train, y_test = y_df.iloc[train_index], y_df.iloc[test_index]
    x_train_list.append(X_train);x_test_list.append(X_test);y_train_list.append(y_train);y_test_list.append(y_test)
    #model fit
    #model train
    #model evaluation


#y_train_list[2].describe() == y_train_list[4].describe()
x_train_list[1].describe() == x_train_list[2].describe()

#y_test_list[2].describe() == y_test_list[4].describe()
x_test_list[1].describe() == x_test_list[4].describe()
````

    x_train_list[1].describe() == x_train_list[2].describe()
    Out[28]: 
            Time     V1     V2     V3   ...      V26    V27    V28  Amount
    count  False  False  False  False   ...    False  False  False   False
    mean   False  False  False  False   ...    False  False  False   False
    std    False  False  False  False   ...    False  False  False   False
    min     True  False  False   True   ...     True   True   True    True
    25%     True  False  False  False   ...    False  False  False   False
    50%    False  False  False  False   ...    False  False  False   False
    75%     True  False  False  False   ...    False  False  False   False
    max     True   True   True   True   ...     True   True  False    True

    [8 rows x 30 columns]

    x_test_list[1].describe() == x_test_list[4].describe()
    Out[29]: 
            Time     V1     V2     V3   ...      V26    V27    V28  Amount
    count  False  False  False  False   ...    False  False  False   False
    mean   False  False  False  False   ...    False  False  False   False
    std    False  False  False  False   ...    False  False  False   False
    min    False  False  False  False   ...    False  False  False    True
    25%    False  False  False  False   ...    False  False  False   False
    50%    False  False  False  False   ...    False  False  False   False
    75%    False  False  False  False   ...    False  False  False   False
    max    False  False  False  False   ...    False  False  False   False

    [8 rows x 30 columns]

## external

Stratification seeks to ensure that each fold is representative of all strata of the data. Generally this is done in a supervised way for classification and aims to ensure each class is (approximately) equally represented across each test fold (which are of course combined in a complementary way to form training folds).

The intuition behind this relates to the bias of most classification algorithms. They tend to weight each instance equally which means overrepresented classes get too much weight (e.g. optimizing F-measure, Accuracy or a complementary form of error).  Stratification is not so important for an algorithm that weights each class equally (e.g. optimizing Kappa, Informedness or ROC AUC) or according to a cost matrix (e.g. that is giving a value to each class correctly weighted and/or a cost to each way of misclassifying). See, e.g. 
D. M. W. Powers (2014), What the F-measure doesn't measure: Features, Flaws, Fallacies and Fixes. http://arxiv.org/pdf/1503.06410

One specific issue that is important across even unbiased or balanced algorithms, is that they tend not to be able to learn or test a class that isn't represented at all in a fold, and furthermore even the case where only one of a class is represented in a fold doesn't allow generalization to performed resp. evaluated. However even this consideration isn't universal and for example doesn't apply so much to one-class learning, which tries to determine what is normal for an individual class, and effectively identifies outliers as being a different class, given that cross-validation is about determining statistics not generating a specific classifier.

On the other hand, supervised stratification compromises the technical purity of the evaluation as the labels of the test data shouldn't affect training, but in stratification are used in the selection of the training instances. Unsupervised stratification is also possible based on spreading similar data around looking only at the attributes of the data, not the true class. See, e.g.
http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.469.8855
N. A. Diamantidis, D. Karlis, E. A. Giakoumakis (1997),
Unsupervised stratification of cross-validation for accuracy estimation.

Stratification can also be applied to regression rather than classification, in which case like the unsupervised stratification, similarity rather than identity is used, but the supervised version uses the known true function value.

Further complications are rare classes and multilabel classification, where classifications are being done on multiple (independent) dimensions.  Here tuples of the true labels across all dimensions can be treated as classes for the purpose of cross-validation. However, not all combinations necessarily occur, and some combinations may be rare. Rare classes and rare combinations are a problem in that a class/combination that occurs at least once but less than K times (in K-CV) cannot be represented in all test folds. In such cases, one could instead consider a form of stratified boostrapping (sampling with replacement to generate a full size training fold with repetitions expected and 36.8% expected unselected for testing, with one instance of each class selected initially without replacement for the test fold).

Another approach to multilabel stratification is to try to stratify or bootstrap each class dimension separately without seeking to ensure representative selection of combinations. With L labels and N instances and Kkl instances of class k for label l, we can randomly choose (without replacement) from the corresponding set of labeled instances Dkl approximately N/LKkl instances. This does not ensure optimal balance but rather seeks balance heuristically. This can be improved by barring selection of labels at or over quota unless there is no choice (as some combinations do not occur or are rare).  Problems tend to mean either that there is too little data or that the dimensions are not independent.

#### Fonte

https://stats.stackexchange.com/a/161522/200592


## Example

````python
from sklearn.model_selection import StratifiedKFold
kf = StratifiedKFold(n_splits=10)
kf.get_n_splits(X)
print(kf)

for train_index, test_index in kf.split(X,y):
    print("TRAIN:", train_index, "TEST:", test_index)
    X_train, X_test = X.iloc[train_index], X.iloc[test_index]
    y_train, y_test = y.iloc[train_index], y.iloc[test_index]
    x_train_list.append(X_train);x_test_list.append(X_test);y_train_list.append(y_train);y_test_list.append(y_test)
    #model fit
    #model train
    #model evaluation
````

