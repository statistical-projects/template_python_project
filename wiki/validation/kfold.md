# K-fold

## General

De forma simplificada o K-fold vem para resolver alguns problemas do hold-out.
Ao invés de quebrar a base em 2, usando somente 70% dos dados para treinar, o K-fold usa 100% dos dados para treinar e  testar através de `k` iteracoes, quebrando a base em `k` sub-grupos.

A cada iteracao ele escolhe um dos grupos como teste e o resto como treino, e realiza `k` treinos e `k` validacoes.
**O numero de `k` é definido pelo usuário**. Geralmente colocamos 10 (mais conhecido como `10-fold cross validation`).

Após todas as iteracoes, geralmente armazenamos todas as metricas de valuation do modelo e tiramos a media ou mediana.
Mas esse fica a critério do cientista.

![kfold][kfold]

### step by step

1. Shuffle the dataset randomly.
2. 2.Split the dataset into k groups
3. For each unique group:
    1. Take the group as a hold out or test data set
    2. Take the remaining groups as a training data set
    3. Fit a model on the training set and evaluate it on the test set
    4. Retain the evaluation score and discard the model
4. Summarize the skill of the model using the sample of model evaluation scores

#### fonte:
https://machinelearningmastery.com/k-fold-cross-validation/

| Pros                                   | Cons                        |
| -------------------------------------- | --------------------------- |
| No parametric or theoretic assumptions | Computationally intensive   |
| Given enough data, highly accurate     | Must choose the fold size   |
| Conceptually simple                    | Potential conservative bias |

## Escolhendo `K`

The k value must be chosen carefully for your data sample.

A poorly chosen value for k may result in a mis-representative idea of the skill of the model, such as a score with a high variance (that may change a lot based on the data used to fit the model), or a high bias, (such as an overestimate of the skill of the model).

Three common tactics for choosing a value for k are as follows:

- Representative: The value for k is chosen such that each train/test group of data samples is large enough to be statistically representative of the broader dataset.
- k=10: The value for k is fixed to 10, a value that has been found through experimentation to generally result in a model skill estimate with low bias a modest variance.
- k=n: The value for k is fixed to n, where n is the size of the dataset to give each test sample an opportunity to be used in the hold out dataset. This approach is called leave-one-out cross-validation.

>The choice of k is usually 5 or 10, but there is no formal rule. As k gets larger, the difference in size between the training set and the resampling subsets gets smaller. As this difference decreases, the bias of the technique becomes smaller
— Page 70, Applied Predictive Modeling, 2013.

### Implicacoes do numero de folds
Algumas fontes dizem que quanto menor o numero de folds, mais enviesado *(biased)* ficará o dataset, porem menos variavel.
Por outro lado, se pegar o outro extremo, usando o maior numero de folds (`leave-one-out`) sua estimativa de erro ficaraá menos enviesada, poreém com uma variancia maior.

#### fonte:
https://machinelearningmastery.com/k-fold-cross-validation/

## external

http://scott.fortmann-roe.com/docs/MeasuringError.html

>In some cases the cost of setting aside a significant portion of the data set like the holdout method requires is too high. As a solution, in these cases a resampling based technique such as cross-validation may be used instead.
>
>Cross-validation works by splitting the data up into a set of n folds. So, for example, in the case of 5-fold cross-validation with 100 data points, you would create 5 folds each containing 20 data points. Then the model building and error estimation process is repeated 5 times. Each time four of the groups are combined (resulting in 80 data points) and used to train your model. Then the 5th group of 20 points that was not used to construct the model is used to estimate the true prediction error. In the case of 5-fold cross-validation you would end up with 5 error estimates that could then be averaged to obtain a more robust estimate of the true prediction error.
>
>As can be seen, cross-validation is very similar to the holdout method. Where it differs, is that each data point is used both to train models and to test a model, but never at the same time. Where data is limited, cross-validation is preferred to the holdout set as less data must be set aside in each fold than is needed in the pure holdout method. Cross-validation can also give estimates of the variability of the true error estimation which is a useful feature. However, if understanding this variability is a primary goal, other resampling methods such as Bootstrapping are generally superior.
>
>On important question of cross-validation is what number of folds to use. Basically, the smaller the number of folds, the more biased the error estimates (they will be biased to be conservative indicating higher error than there is in reality) but the less variable they will be. On the extreme end you can have one fold for each data point which is known as Leave-One-Out-Cross-Validation. In this case, your error estimate is essentially unbiased but it could potentially have high variance. Understanding the Bias-Variance Tradeoff is important when making these decisions. Another factor to consider is computational time which increases with the number of folds. For each fold you will have to train a new model, so if this process is slow, it might be prudent to use a small number of folds. Ultimately, it appears that, in practice, 5-fold or 10-fold cross-validation are generally effective fold sizes.

https://youtu.be/CmEqvD_ov2o

[![cross validation](https://img.youtube.com/vi/CmEqvD_ov2o/0.jpg)](http://www.youtube.com/watch?v=CmEqvD_ov2o)

## Example

````python
from sklearn.model_selection import KFold
kf = KFold(n_splits=10)
kf.get_n_splits(X)
print(kf)

for train_index, test_index in kf.split(X):
    print("TRAIN:", train_index, "TEST:", test_index)
    X_train, X_test = X.iloc[train_index], X.iloc[test_index]
    y_train, y_test = y.iloc[train_index], y.iloc[test_index]
    x_train_list.append(X_train);x_test_list.append(X_test);y_train_list.append(y_train);y_test_list.append(y_test)
    #model fit
    #model train
    #model evaluation
````

outra forma de se fazer

````python
#Logistic Regression
from sklearn.linear_model import LogisticRegression
classifier = LogisticRegression(random_state = 0)

#cross validation
from sklearn.model_selection import cross_val_score
scores = cross_val_score(classifier, X_train, y_train, cv=5)
print(scores)
````

## sources

https://machinelearningmastery.com/k-fold-cross-validation/

[kfold]: kfold.png