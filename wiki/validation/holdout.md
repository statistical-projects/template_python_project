# Hold-out

## General

O hold out é o mais basico dos cross validations.
Ele basicamente quebra a base em treino e teste, ou treino e hold.

![alt text][holdout]

O modelo vai fazer o fit no treino e vai testar sua hipotese no test set.

Geralmente se quebra em 70% - 30%, sendo 70% o treino e o restante o teste. Porem isso depende muito tambem do tamanho do dataset.

| Pros                                   | Cons                                                                                 |
| -------------------------------------- | ------------------------------------------------------------------------------------ |
| No parametric or theoretic assumptions | Potential conservative bias                                                          |
| Given enough data, highly accurate     | Tempting to use the holdout set prior to model completion resulting in contamination |
| Very simple to implement               | Must choose the size of the holdout set (70%-30% is a common split)                  |
| Conceptually simple                    |

## External explanation

http://scott.fortmann-roe.com/docs/MeasuringError.html

>This technique is really a gold standard for measuring the model's true prediction error. As defined, the model's true prediction error is how well the model will predict for new data. By holding out a test data set from the beginning we can directly measure this.
>
>The cost of the holdout method comes in the amount of data that is removed from the model training process. For instance, in the illustrative example here, we removed 30% of our data. This means that our model is trained on a smaller data set and its error is likely to be higher than if we trained it on the full data set. The standard procedure in this case is to report your error using the holdout set, and then train a final model using all your data. The reported error is likely to be conservative in this case, with the true error of the full model actually being lower. Such conservative predictions are almost always more useful in practice than overly optimistic predictions.
>
>One key aspect of this technique is that the holdout data must truly not be analyzed until you have a final model. A common mistake is to create a holdout set, train a model, test it on the holdout set, and then adjust the model in an iterative process. If you repeatedly use a holdout set to test a model during development, the holdout set becomes contaminated. Its data has been used as part of the model selection process and it no longer gives unbiased estimates of the true model prediction error.

https://youtu.be/CmEqvD_ov2o

[![cross validation](https://img.youtube.com/vi/CmEqvD_ov2o/0.jpg)](http://www.youtube.com/watch?v=CmEqvD_ov2o)


## Example

````python
from sklearn.model_selection import train_test_split

train_X, test_X, train_y, test_y = train_test_split(docs_to_train.data, docs_to_train.target,test_size = 500)
````

## *sources*

http://scott.fortmann-roe.com/docs/MeasuringError.html

[holdout]: holdout.png