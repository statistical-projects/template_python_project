Wiki of Data Science - *My personal study*
============

## Study Paths (TOCs)

- [Table of Contents - Prova Cientista (provida pelo itau)](TOCs/table_of_contents_prova_itau.md)
- [Trilha Alternativa (caio)](TOCs/trilha_estudos.md)

----------

## Study Records

### Statistics

### Sampling

### Machine Learning

#### Regression

- Linear Regression
- Logistic Regression
- Polynomial Regression
- Stepwise Regression
- Ridge Regression
- Lasso Regression
- ElasticNet Regression

#### Regularization

- [Lasso & Ridge](machineleaning/regularization/lasso_ridge.md)

### Validation

- [hold-out](validation/holdout.md)
- k-fold
  - [kfold](validation/kfold.md)
  - [stratitified](validation/stratified-kfold.md)
  - [stratitified shuffle](validation/stratified-shuffle-split.md)
- [leave-one-out](validation/leaveoneout.md)
- [out-of-time](validation/outoftime.md)