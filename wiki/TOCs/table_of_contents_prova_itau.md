

# Lista de assuntos do Itau

## Computação

- Algorithms toolbox
- Algorithms on graphs
- Data structures
- Algorithms on strings
- Advanced algorithms and complexity

## Matematica e estatistica

- Matrix computations
- Linear algebra
- Matrix decomposition
- Introduction to probability and data
- Inferential statistics
- Bayesian statistics
- Linear regression and modelling
- Statistics with r capstone

## Otimização

- Fundamentals of optimization
- First order optimizations methods
- Optimatily and duality
- Seconds-order optimization methods
- Special topics of optimizations

## Machine learning

- Machine learning basics
- Machine learning intermediate
- Machine learning advanced
- Neural network and deep learning
- Improving deep neural networks
- Convolutional neural networks

## Banco de dados

- Sql for data science
- Introduction to databases
- Database design
- Big data introductions
- Big data concepts
- Big data algorithms
- hadoop
- Spark
- Impala

## Dados não estruturados

- Intro on untrustured data
- Text mining
- Neural language processing
- Audio processing
- Image processing
- Probabilistic graphical models representation
- Probabilistic graphical models inference
- Probabilistic graphical models learning