# Trilha de assuntos

## Validation

- [x]  leave one out
  - [ ]  esse é aconselhado para classificacao?
- [x]  holdout
  - [ ]  esse é aconselhado a regressao?
- [x]  kfold
  - [x] Stratified Cross Fold
  - [x] ShuffleSplit Stratified k-fold
- [ ]  out of time

## regularization

- [x]  L1 (lasso a1)
- [x]  L2 (ridge + param de regualizacao)

## optimization

- [ ] grid search

## Metrics

- [ ]  rmse
- [ ]  mse
- [ ]  mae
- [ ]  mad
- [ ]  ks
- [ ]  r^2
- [ ]  f1
- [ ]  recall
- [ ]  auc

## Statistics

- [ ]  distribuicao empirica
- [ ]  distancia entre funcoes
- [ ]  calcular desvio padrao amostral
- [ ]  intervalo de confianca
- [ ]  probabilidades cruzadas

## Clustering

- [ ]  k means
  - [x]  numero minimo de grupos
  - [x]  qual o resultado final
  - [x]  inicializacao aleatoria
  - [ ]  kmedian kmoda kmedoid bisect kmeans, qual a diferenca
  - [ ]  Silhouette - pode definir o numero ideal de grupos
- [ ]  linkage - hierarquicos
  - [ ]  single complete e average linkage, qual a diferenca
  - [ ]  tratamento de outliers nos linkages e hierarquicos
  - [ ]  tamanho das bases aconselhadas (milhoes, bilhoes?)
- [ ]  outros
  - [ ]  rand index
  - [ ]  dbscan - como acelerar o tempo de processamento

## Classification

- [ ]  naive bayes
  - [ ]  onde é aconselhado seu uso?
  - [ ]  aconselhado a grandes volumes de features ?
- [ ]  knn
  - [ ]  aconselhado ter maior ou menor numero de k
- [ ]  arvore decisao
  - [ ]  quanto maior a profundidade, maior chance de overfit?
  - [ ]  missings inviabilizam a sua execucao?
  - [ ]  capacidade de generalizacao - mesmo com 1 observação porfolha?
  - [ ]  particoes ?
- [ ]  rede neural
  - [ ]  one vs one / one vs rest
- [ ]  RMSE
  - [ ]  o que é? Onde é aconselhado seu uso?
- [ ]  svm
  - [ ]  grau do polinonio, deve ser definido para todos os kernels?
- [ ]  mlp - multi layer perceptron
  - [ ]  o que é? Onde é aconselhado seu uso?
- [ ]  em geral
  - [ ]  quanto maior o numero de features independentes melhor o desempenho

## Regression

- [ ]  linear
  - [ ]  suposicoes? Districuicao normal e iid?
- [ ]  knn
  - [ ]  é aconselhado fazer uma votacao simples nos valores dos k vizinhos?
  - [ ]  numero de k proximo a m tende a fazer underfiting
- [ ]  random forest
  - [ ]  em um modelo simplista, podemos calcular a media dos valores encontrados em cada arvore?
- [ ]  em geral
  - [ ]  são aeftados por outliers e missings?
  - [ ]  qual coeficiente é responsavel pela inclinacao da reta?
  - [ ]  so podem ser aplicados quando a variavel resposta apresenta distribuicao normal?

## Ensamble

- [ ]  em geral
  - [ ]  quanto mais correlacionadas as predicoes individuais, piors o desempenho da predicao final combinada
  - [ ]  não serve para regressao, somente para classificacao?