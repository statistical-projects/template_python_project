## **Validação Cruzada**

### Hold Out: 

separa-se a base de modelagem em treino e teste, porém isso resulta em alto bias, pois o modelo aprende tudo muito específico na base de treino e erra na base de teste. Essa técnica tende a superestimar a métrica de teste.

### Leave One Out: 

é um caso específico de K-Fold com K = n.  Computacionalmente caro de ser processar. Em relação ao Hold-out tem um bias bem menor, porém tem uma variância maior, pois pode-se provar que realizar a média de valores correlacionados resulta em alta variância.

### K-Fold: 

realiza-se K divisões na base onde usa-se K-n folds para treinar o modelo e K para validar. Esse método mostra-se empiricamente melhor que ambos os anteriores para uma base onde não se tem uma ampla distribuição no teste e treino, como por exemplo treinar o modelo na população e testa-lo também na população, aí não se torna tão necessária a re-amostragem.

## **Árvore Regressão e Decisão**

### Regressao

divide os dados pensando visando minimizar a função RSS.

<img src="https://tex.s2cms.ru/svg/%0A%5Csum_%7Bj%3D1%7D%5E%7Bj%7D%20%5Csum_%7Bi%20%5Cin%20R_%7Bj%7D%7D%20(y_i%20-%20%5Chat%7By%7D_%7BR_%7Bj%7D%7D)%0A" alt="\sum_{j=1}^{j} \sum_{i \in R_{j}} (y_i - \hat{y}_{R_{j}})" />

Aonde yR é a média da resposta para as observações de treino pertencentes ao box J. Como é computacionalmente inviável tentar todas as partições existentes do espaço, por isso usa-se uma abordagem de cima para baixa chamada “recursive binary splitting”. A árvore não é totalmente otimizada pois ele escolhe a melhor variável e melhor quebra para aquele determinado passo, e não visando o final do algoritmo.

Para fazer a recursive binary splitting é necessário escolher a variável V e o ponto de corte PC que mais minimiza o RSS.  Então para cada V e PC calcula-se a função a abaixo e escolhe-se a que mais minimiza o RSS

<img src="https://tex.s2cms.ru/svg/%0A%5Csum_%7Bi%3A%20x_%7B1%7D%20%5Cin%20R_%7B1%7D%20(j_%7B1%7D%20s)%7D%20(y_%7Bi%7D%20-%20%5Chat%7By%7D_%7BR_%7B1%7D%7D)%5E2%20%2B%20%5Csum_%7Bi%3A%20x_%7B1%7D%20%5Cin%20R_%7B2%7D(j_%7B1%7Ds)%7D%20(y_%7Bi%7D%20-%20%5Chat%7By%7D_%7BR_%7B2%7D%7D)%5E2%0A" alt="\sum_{i: x_{1} \in R_{1} (j_{1} s)} (y_{i} - \hat{y}_{R_{1}})^2 + \sum_{i: x_{1} \in R_{2}(j_{1}s)} (y_{i} - \hat{y}_{R_{2}})^2" />

Árvores podem crescer indefinitivamente, o que pode gerar overfitting, o que aumenta o erro na base de teste. Para resolver esse problema usa-se a poda das árvores, que visa otimizar o crescimento da árvore que reduz RSS com a complexidade de se ter muitas folhas, assim reduzindo o tamanho da árvore calculando sub-árvores e validando o valor do erro do teste para cada uma.

<img src="https://tex.s2cms.ru/svg/%0A%5Csum_%7Bm%3D1%7D%5E%7B%7CT%7C%7D%20%5Csum_%7Bi%3A%20x_%7B1%7D%20%5Cin%20R_%7Bm%7D%7D%20(y_%7B1%7D%20-%20%5Chat%7By%7D_%7BR_%7Bm%7D%7D)%5E2%20%2B%20a%7CT%7C%0A" alt="\sum_{m=1}^{|T|} \sum_{i: x_{1} \in R_{m}} (y_{1} - \hat{y}_{R_{m}})^2 + a|T|" />

### **Classificação:** 

é muito similar a árvore de regressão, porém ao invés de RSS ou MSE, usa a taxa de erro da classificação

### **Gini Index:** 

é uma métrica da variância total ao longo das K classes: ( <img src="https://tex.s2cms.ru/svg/%5Chat%7BP%7D_%7Bmk%7D" alt="\hat{P}_{mk}" /> representa a proporção das observações de treino na região <img src="https://tex.s2cms.ru/svg/m_%7Bth%7D" alt="m_{th}" /> pertencente a <img src="https://tex.s2cms.ru/svg/k_%7Bth%7D" alt="k_{th}" /> classe). Pode se mostrar que se <img src="https://tex.s2cms.ru/svg/%5Chat%7BP%7Dmk" alt="\hat{P}mk" /> tende a zero ou a um o gini será pequeno.  Por esses motivos o gini é considerado uma métrica de pureza do nó, um valor pequeno significa que o nó tem contém predominantemente observações de uma única classe.

<img src="https://tex.s2cms.ru/svg/%20G%20%3D%20%5Csum_%7Bk%3D1%7D%5E%7BK%7D%20%5Chat%7BP%7D_%7Bmk%7D%20(1%20-%20%5Chat%7BP%7D_%7Bmk%7D)_%7B1%7D%20" alt=" G = \sum_{k=1}^{K} \hat{P}_{mk} (1 - \hat{P}_{mk})_{1} " />

### **Cross-entropy:** 

é outra medida de pureza e pode-se mostrar que se <img src="https://tex.s2cms.ru/svg/%5Chat%7BP%7D_%7Bmk%7D" alt="\hat{P}_{mk}" /> tende a ser todo 0 ou todo 1 então a entropia tende a zero também.

<img src="https://tex.s2cms.ru/svg/%20D%20%3D%20-%20%5Csum_%7Bk%3D1%7D%5E%7BK%7D%20%5Chat%7BP%7D_%7Bmk%7D%20%5Clog%20%5Chat%7BP%7D_%7Bmk%7D" alt=" D = - \sum_{k=1}^{K} \hat{P}_{mk} \log \hat{P}_{mk}" />

### **Benefícios/Malefícios:**

-São altamente interpretáveis (conjunto de condições);

-Usa variáveis categóricas sem precisar de tratamento ou transformar em dummies;

-Processamento ágil;

-Por sua simplicidade, não consegue atingir grande performance.

### **CART**: 

a ideia para crescer uma árvore é escolher a quebra dentre todas as possíveis quebras, em todos os nós afim de que o nó filho resulte no mais puro possível. Esse algoritmo é univariável ou seja, só olha uma variável explicativa para fazer a quebra e não a combinação de algumas.

Se X é nominal categórico de I categorias então existem <img src="https://tex.s2cms.ru/svg/%202%5E%7Bi-1%7D%20-%201%20" alt=" 2^{i-1} - 1 " /> possíveis quebras. Se X é ordinal categórico ou continuo com K diferentes valores então existem K-1 diferentes quebras de X.

1 - Ache cada a melhor quebra de cada preditor;

2 - Para cada variável contínua ou ordinal ordene seus valores de forma crescente. Depois varie em cada valor existente testando a métrica de quebra possível;

Para cada variável categórica examina cada sub conjunto de categorias testando a métrica de quebra possível;

3 - Ao longe de todas as variáveis e todas as quebras possíveis selecione a melhor e realize a quebra;

4 - Realize a melhor quebra até que a regra de parada seja atingida.

Para Y categóricas existem 3 possíveis quebras Gini Criterion/Twoing Criterion/Order Twoing Criterion.

Já para Y continuo usa-se o critério de LSD Least Squares Deviation.

Critérios de parada:

 - Se um nó chegou a 100% de pureza;

 - Se a profundidade chegou ao máximo definido;

 - Se o tamanho do nó chegou ao tamanho mínimo definido;

 - Se a quebra resulta em um nó filho com o tamanho menor que o definido para um nó ou folha;

 - Se na melhor quebra obtida o acréscimo na métrica de quebra não é maior que o definido pelo usuário para haver a quebra.

### **Missing values:** 

Se uma variável dependente de uma amostra é missing, esse valor será ignorado. Se todas as variáveis explicativas forem missing, esse caso será ignorado. Se o peso da amostra for missing, zero ou negativo, esse caso será ignorado. Se a frequência do peso for missing, zero ou negativa, esse caso será ignorado.

O método de substituição de quebra é usado para lidar com variáveis explicativas missing. Se existe uma quebra onde X < s, porém X é missing, então testa-se a próxima melhor variável da amostra para quebra. Se todas forem missing então usa-se maioria de votos para seleciona a amostra.

## **Bagging**

Bagging é o processo de re-amostragem com finalidade de reduzir a variância dos modelos para não haver overfitting, normalmente utilizado com bootstrap. É largamente utilizado com árvores de decisão devido ao baixo bias e alta variância. No caso de regressão realiza a média dos diversos valores obtidos nas amostras do bootstrap, já para classificação realiza a maioria de votos.

### **Out-of-bag Error Estimation:** 

pode-se usar o bagging para validar a métrica de teste sem ser necessário uma das técnicas conhecidas de validação cruzada. Como é sabido o bootstrap pega cerca de 2/3 da população para criar sua amostra, o restante pode ser usado para validar a métrica de teste, ou seja, se obtém um treinamento na amostra gerada pelo bootstrap, e no que não for usado aplica-se o modelo e avalia por alguma métrica, dessa forma cada amostra terá um conjunto de predições, que para um modelo de regressão pode se aplicar a média e de classificação a maioria de votos. Pode se provar que se usar o Out-of-bag error estimation com B suficientemente grande o erro estimado será próximo ou igual ao LOOCV.

### **Importância das variáveis**: 

pode-se chegar a esse valor por variável somando o quanto cada vez que uma variável foi usada como quebra, o quanto ela minimizou a métrica de quebra, MSE para regressão e Gini ou Entropia para classificação.

## **Random Forest**

Random forest possui uma sutil vantagem sobre bagging divido ao fato de que o mesmo sorteia um percentual das variáveis explicativas para realizar cada árvore. Isso garante que as árvores não sejam tão correlacionadas entre si, pois caso exista um preditor muito forte em uma única arvore o mesmo teria maior poder de predição sobre outros preditores, porem em um conjunto de árvores onde ele pode não ser usado outros preditores tomam espaço.

*Usa-se o número de preditores por arvore pequeno quando se tem variáveis explicativas correlacionadas.

## **Boosting**

Boosting é um algoritmo sequencial, ele usa informações do modelo anterior para fazer o próximo. Ele não usa o bagging (amostras bootstrap), ao invés disso cada modelo é aplicado em uma versão modificada do dataset.

Funcionalidade: Aplica-se um modelo fraco, por exemplo uma árvore com profundidade máxima de 3, após isso escolhe um parâmetro para ponderar esse modelo. Com os resíduos desse modelo fraco aplica-se um outro modelo fraco onde a resposta do mesmo é o resíduo do outro e se atribui um peso a esse novo modelo, e assim sucessivamente até algum critério de parada. O parâmetro <img src="https://tex.s2cms.ru/svg/%20%5Clambda%20" alt=" \lambda " /> que seria o learning rate ou shrinkage ajusta o quanto cada novo modelo irá agregar ao modelo final, ao ponto de que se esse parâmetro for muito pequeno pode-se nunca convergir, e em casos muito altos pode até divergir o modelo.

*Boosting pode dar overfitting se o numero de estimadores for muito alto, devido a não realizar bagging o modelo pode aprender muito com a base a ponto de ficar extremamente overfittado na base de treino.

*Existe um trade off entre número de estimadores e o learning rate, se o learning rate for muito pequeno é mais adequado termos vários estimadores.

*Boosting usa todas as linhas e colunas do seu dataset

*Visa primeiro reduzir o bias e depois a variância

### **AdaBoosting:** 

transforma preditores fracos em um forte modificando em cada iteração o peso das predições anteriores e focando em cada iteração em acertar as que foram erradas na iteração anterior, quanto melhor a performance de cada preditor fraco, maior o peso <img src="https://tex.s2cms.ru/svg/%20%5Calpha%20" alt=" \alpha " /> de cada preditor.

### **GradientBoosting:** 

transforma preditores fracos em fortes calculando, em cada iteração, os pseudo resíduos do modelo através do gradiente descendente que permite facilitar o uso de funções custo difíceis ou caras de se calcular a cada iteração. O gradiente descendente caminha de acordo com o learning rate, o que regular o quanto cada preditor fraco deve influenciar, ou ajudar a caminhar para o mínimo global.

## **KMEANS**

-Kmeans++: inicia os pontos de forma que o segundo cluster é escolhido aleatoriamente, porém com probabilidade de o segundo plano estar próximo de ||Cn - X ||^2. Ou seja, a probabilidade de um próximo ponto virar centroide é a distancia dele para o centroide anterior ao quadrado. Na segunda iteração a probabilidade de um ponto ser inicializado como centroide é calculada para o centroide mais próximo ou seja:

Sejam nossos dados: [0, 1, 2, 3, 4]. Escolhemos aleatoriamente que C1 é o ponto 0. A probabilidade que o próximo centroide seja C2 seja X é proporcional a ||Cn - X ||^2.Então, P(c2 = 1) = 1a, P(c2 = 2) = 4a, P(c2 = 3) = 9a, P(c2 = 4) = 16a, onde a = 1/(1+4+9+16).

Suponha agora que c2=4. Então, P(c3 = 1) = 1a, P(c3 = 2) = 4a, P(c3 = 3) = 1a, onde a = 1/(1+4+1).

## **Hierárquico**

Algoritmos hierárquicos tendem a crescer a distancia inter cluster ou a soma dos quadrados das distancias do cluster.

-Ward Linkage: distância entre centroides de clusters.  O Ward tende a crescer o SS o mínimo possível. Tende a criar clusters igualmente distribuídos, ou seja, muitas vezes com forma arredondada.

-Single Linkage: usa a menor distância entre os clusters. Tende a juntar todo mundo que tiver um único ponto próximo um do outro. Se preocupa mais com separar os dados do que a densidade dos clusters ou balanceamento.

-Complete Linkage: usa a maior distancia entre os clusters, tende a ter cluster bem densos e poucos clusters.

-Average Linkage: usa a distancia de todos os pontos de um cluster c1 para outro cluster c2. Acaba se comportando de forma parecida com o Ward.

## **Regressão Linear**

-Tenta-se ajustar os dados de forma a regredir os valores a uma reta ou um hiperplano linear visando minimizar o RSS (Residuals Sum os Squares). Para tal estima-se n parâmetros. Um dos métodos para estimar esses parâmetros é o cálculo dos mínimos quadrados (Least Squares).

<img src="https://tex.s2cms.ru/svg/%0A%5Cbeta_%7B1%7D%20%3D%20%5Cfrac%7B%5Csum_%7Bi%3D1%7D%5E%7Bn%7D%20(x_%7B1%7D%20-%20%5C%3D%7Bx%7D)(y_%7B1%7D%20-%20%5C%3D%7By%7D)%7D%7B%20%5Csum_%7Bi%3D1%7D%5E%7Bn%7D%20(%20x_%7B1%7D%20-%20%5C%3D%7Bx%7D%20)%5E2%20%7D%0A" alt="\beta_{1} = \frac{\sum_{i=1}^{n} (x_{1} - \={x})(y_{1} - \={y})}{ \sum_{i=1}^{n} ( x_{1} - \={x} )^2 }" />

<img src="https://tex.s2cms.ru/svg/%0A%5Cbeta_%7B2%7D%20%5C%3D%7By%7D%20-%20%5Chat%7B%5Cbeta%7D_%7B1%7D%5C%3D%7Bx%7D_%7B1%7D%0A" alt="\beta_{2} \={y} - \hat{\beta}_{1}\={x}_{1}" />

Como não se tem os coeficientes reais para a população é necessário estima-los de forma amostral o que ser obtido através do Standard Error (SE). Se realizarmos n modelos com n tendendo a infinito e para cada modelo usarmos uma amostra para calcular os betas pode-se chegar de forma segura a uma boa aproximação dos parâmetros.

Para se calcula SE se usa: <img src="https://tex.s2cms.ru/svg/%20Var(%5Chat%7B%5Cmu%7D)%5E2%20%3D%20%5Cfrac%7B%5Csigma%5E2%7D%7Bn%7D" alt=" Var(\hat{\mu})^2 = \frac{\sigma^2}{n}" />, onde pode-se observar que quanto maior o n menor o desvio padrão.

<img src="https://tex.s2cms.ru/svg/%0ASE(%5Chat%7B%5Cbeta%7D_%7B0%7D)%5E2%20%3D%20%5Csigma%5E2%20%5CBigg%20%5B%20%7B%5Cfrac%7B1%7D%7Bn%7D%20%2B%20%5Cfrac%7B%5C%3D%7Bx%7D%5E2%7D%7B%5Csum_%7Bi%3D1%7D%5E%7Bn%7D%20(x_%7Bi%7D%20-%20%5C%3D%7Bx%7D)%5E2%7D%20%5CBigg%20%5D%20%2C%20SE(%5Chat%7B%5Cbeta%7D_1)%5E2%20%3D%20%7B%5Cfrac%7B%5Csigma%5E%7B2%7D%7D%7B%5Csum_%7Bi%3D1%7D%5E%7Bn%7D%20(x_i%20-%20%5C%3D%7Bx%7D)%5E2%7D%0A" alt="SE(\hat{\beta}_{0})^2 = \sigma^2 \Bigg [ {\frac{1}{n} + \frac{\={x}^2}{\sum_{i=1}^{n} (x_{i} - \={x})^2} \Bigg ] , SE(\hat{\beta}_1)^2 = {\frac{\sigma^{2}}{\sum_{i=1}^{n} (x_i - \={x})^2}" />

Para um intervalo de confiança de 95% chega aos valores de <img src="https://tex.s2cms.ru/svg/%20%5Chat%7B%5Cbeta%7D_1%20%5Cpm%202%20SE(%5Chat%7B%5Cbeta_1%7D)%20" alt=" \hat{\beta}_1 \pm 2 SE(\hat{\beta_1}) " /> da mesma forma que <img src="https://tex.s2cms.ru/svg/%20%5Chat%7B%5Cbeta_n%7D%20%5Cpm%202SE(%5Chat%7B%5Cbeta_n%7D)%20" alt=" \hat{\beta_n} \pm 2SE(\hat{\beta_n}) " />.

**Potênciais Problemas na regressão:**

### **Não linearidade nos dados**: 

na regressão linear espera-se que os dados se distribuam e comportem de forma linear, se essa premissa não é verdadeira a assertividade do modelo fica comprometida. Para visualizar melhor essa falta de linearidade podemos plotar o gráfico dos resíduos com o preditor <img src="https://tex.s2cms.ru/svg/%20x_i%20" alt=" x_i " />, ou no case de multiplas variáveis podemos plotar os resíduos versus os valores preditos. Idealmente esse gráfico não deverám apresentar nenhum padrão, porém em caso de não linearidade poderá indicar um padrão na distribuição. Para corrigirmos essa premissa podemos transformar os preditores que se comportam de forma não linear com <img src="https://tex.s2cms.ru/svg/%20%5Csqrt%7BX%7D%2C%20%5Clog%7BX%7D%20" alt=" \sqrt{X}, \log{X} " /> ou <img src="https://tex.s2cms.ru/svg/%20X%5E2%20" alt=" X^2 " />

### **Correlação entre os termos de erro** 
<img src="https://tex.s2cms.ru/svg/%20e%20" alt=" e " />: a regressão assume que os error <img src="https://tex.s2cms.ru/svg/%20e_1%2C%20e_2%20...%20" alt=" e_1, e_2 ... " /> Não são correlacionados, ou seja, <img src="https://tex.s2cms.ru/svg/%20e_1%20" alt=" e_1 " /> ser positivo não influencia em nada em <img src="https://tex.s2cms.ru/svg/%20e_%7Bi%2B1%7D%20" alt=" e_{i+1} " />.  Os erros padrão <img src="https://tex.s2cms.ru/svg/%20SE%20" alt=" SE " /> são calculado em cima dessa premissa, se essas premissas forem violadas a estimativa do erro padrão estará sendo subestimada, como consequencia os intervalor de confiança e predições serão mais estreitos (menores) que realmente estamos supondo. Por exemplo um intervalo de confiança estimado de 95% pode na verdade ter uma probabilidade de contar os valores muito menor do que 0,95. Esse fenômeno pode constuma ocorrer no contexto de séries temporais, ou quando o tempo ou intervalo de tempo determina a sequencia de valores. Para determinar se existe essa correlação podemos plotar nossos residuos ao longo do tempo, se não existir a correlação os valores não deverão ter um padrão discernivel.

### **OBS:** 

a matriz de erros quando não temos homocedascidade (erro normalmente distribuido com média zero e desvio padrão constante) não é mais um matriz com a diagonal constante e os demais valores iguais a zero. Temos agora uma matriz com heterocedascidade, ou seja variância variável (diagonal), e os demais valores podem diferir de zero ou seja, pode existir correlação entre os erros, essa chama-se matriz de covariancia dos erros.  Quando temos esse tipo de fenômeno o Ordinary Least Squares não é mais um modelo adequado, ou seja, ele não conseguira convergir para os melhores coeficientes, para isso pode-se usar o Generalized Least Squares, que é uma tranformação por uma matriz de covariancia semi positiva definida, com variancias não constantes na diagonal e uma ou mais covariancias diferente de zero. Para satisfazer a premissa citada, podemos multiplicar ambos os lados da equação da regressão linear por essa matriz. Assim o GLS é o OLS multiplicando ambos os lados da equação da regressão pela matriz de covariancia dos erros com variancia variável e correlação entre outros valores. Como essa matriz de covariancia normalmente não é conhecida podemos estima-la de forma a que precisamos calcular a média das respostas e a média dos erros afim de chegarmos a: <img src="https://tex.s2cms.ru/svg/%20%5Chat%7B%5CSigma%7D%20%3D%20(%20N%20-%20i%20)%5E%7B-1%7D.%5Csum_%7Bt%3D1%7D%5E%7BN%7D%20(y_%7Bt%7D%20-%20%5C%3D%7By%7D)%20(y_%7Bt%7D%20-%20%5C%3D%7By%7D)%5E%7BT%7D%20" alt=" \hat{\Sigma} = ( N - i )^{-1}.\sum_{t=1}^{N} (y_{t} - \={y}) (y_{t} - \={y})^{T} " /> assim: <img src="https://tex.s2cms.ru/svg/%20%5Cbeta_%7BEGLS%7D%20%3D%20(X%5ET%20%5Chat%7B%5CSigma%7D%5E%7B-1%7DX)%5C%3D%7By%7D%20" alt=" \beta_{EGLS} = (X^T \hat{\Sigma}^{-1}X)\={y} " />

### **Variância não constante do erros**: 

os intervalos de confiança, erros padrão e teste de hipotese seguem essa premissa. Pode-se obersar esse fênomeno através pela presença de um formato de funíl no gráfico dos resíduos pelos valores preditos. Quando esse tipo de problema ocorre uma possível solução é transformar a resposta usando uma função concava como raiz ou log.

### **Presença de Outliers**: 

a presença de outliers além de poder modificar o formato da curva, dependendo da quantidade de outliers e de pontos no modelo, modifica bem o cálculo do SER, que é usado para calcular os intervalos de confiança e o p-value, o que pode causar uma má leitura dos parâmetros do modelo, além de pode invalidar um projeto por ter grande inflência nas métricas da regressão que que usam o RSS e o RSE. Gráficos como box-plot ou o gráfico de residuos por valores preditos podem dar um grande insight de outlier. Uma forma de identificar outlier pode ser pelo studentized residuals que se obtém dividindo cada erro pelo erro padrão, os valores absolutos acima de 3 são possíveis candidatos a outlier.

### **Ponto com grande influência (poder)**: 

são pontos onde alguma variável explicativa é um outlier, esse tipo de problema tende a impactar o Least Squares bem mais que outliers propriamente dito. Para computar pontos de alta influência podemos usar a high leverage Statistc que varia de <img src="https://tex.s2cms.ru/svg/%20%5Cfrac%7B1%7D%7Bn%7D%20" alt=" \frac{1}{n} " /> até 1, onde ela mede a distancia de cada ponto para a média dos pontos de forma normalizada. A leverage média para todos os pontos é de <img src="https://tex.s2cms.ru/svg/%20%5Cfrac%7Bp%20%2B%201%7D%7Bn%7D%20" alt=" \frac{p + 1}{n} " /> ou seja qualquer valor acima deste pode ser considerado um ponto de influencia alta.

### **Colinearidade:** 

quando temos variáveis que se correlacionam entre si é muito difícil separar o impacto de cada uma. Quando isso acontece temos um problema onde o gráfico de contorno do Least Squares fica muito instável, ou seja, para pequenas variações dos valores, temos uma alteração grande no gráfico de contorno, de forma a aumentar a incerteza dos coeficientes da regressão. Como a colinearidade reduz a assertividade da estimativa dos coeficientes da regressão, isso acaba por crescer muito o erro padrão para os betas. Na presença de coolinearidade podemos acabar rejeitando a hipotese nula, ou seja, colinearidade reduz a chance de acharmos um beta diferente de zero. Podemos verificar a colinearidade pela matriz de correlação entre as variáveis, porém as vezes temos correlação entre 3 variáveis o que fica inviável de detectar com o método anterior, para isso podemos calcular o VIF que é a proporção da variancia de betaX quando é fitado um modelo com todas as variáveis dividido por quando temos só ela. O menos valor do VIF é 1, quando não temos multicolinearidade, e a regra empirica é que quando esse valor excede 5 ou 10 ai temos um problema. Para lidar com esse problema ou dropamos uma das variáveis ou combinamos as mesmas.

## **Classificação Linear**

Até podemos usar a regressão linear para modelar uma classificação ao invés de uma logística, porém apenas para casos binários, pois para 3 ou mais classes fica difícil predizer pois a regressão linear leva esses valores como ordinais. Além de os valores variarem além do intervalo de [0,1], o que dá uma interpretação mais complicada.

No modelo de regressão logística precisamos de uma função de transformação que mantenha os valores de `[0,1]`, para isso existem diversas funções, aqui usaremos a logística <img src="https://tex.s2cms.ru/svg/%20p(x)%20%3D%20%5Cfrac%7B%20_%7Be%7D%20%5Cbeta_%7B0%7D%20%2B%20%5Cbeta_%7B1%7D%20X%20%7D%7B1%20%2B%20_%7Be%7D%20%5Cbeta_%7B0%7D%20%2B%20%5Cbeta_%7B1%7D%20X%20%7D%20" alt=" p(x) = \frac{ _{e} \beta_{0} + \beta_{1} X }{1 + _{e} \beta_{0} + \beta_{1} X } " /> para fitar o modelo usamos um método chamado _maximum likelihood._ Com um pouco de manipulação pode-se chegar a <img src="https://tex.s2cms.ru/svg/%20%5Cfrac%7Bp(x)%7D%7B1%20-%20p(x)%7D%20%3D%20_%7Be%7D%20%5Cbeta_%7B0%7D%20%2B%20%5Cbeta_%7B1%7D%20X%20%20" alt=" \frac{p(x)}{1 - p(x)} = _{e} \beta_{0} + \beta_{1} X  " /> que é denominado “odds” ou chance, que pode variar de 0 a <img src="https://tex.s2cms.ru/svg/%20%5Cinfty%20" alt=" \infty " /> . Aqui temos que <img src="https://tex.s2cms.ru/svg/%20%5Clog(%5Cfrac%7Bp(x)%7D%7B1-p(x)%7D)%20%3D%20%5Cbeta_%7B0%7D%20%2B%20%5Cbeta_%7B1%7D%20X%20" alt=" \log(\frac{p(x)}{1-p(x)}) = \beta_{0} + \beta_{1} X " /> é o log-odds, como na regressão linear o coeficiente <img src="https://tex.s2cms.ru/svg/%20%5Cbeta%20" alt=" \beta " /> é o quanto variar o X em uma unidade implica na variação de y, aqui na logística o <img src="https://tex.s2cms.ru/svg/%20%5Cbeta%20" alt=" \beta " /> é o quanto variar o X em um unidade implica na variação no log-odds, ou a multiplicação no odds por <img src="https://tex.s2cms.ru/svg/%20e%5E%7B%5Cbeta%7D%20" alt=" e^{\beta} " />.

Maximum Likelihood procura dar maior valor de <img src="https://tex.s2cms.ru/svg/%20p(x)%20" alt=" p(x) " /> para valores que são o evento procurado, e um menor <img src="https://tex.s2cms.ru/svg/%20p(x)%20" alt=" p(x) " /> para o evento não procurado, fazendo assim: <img src="https://tex.s2cms.ru/svg/%20%5Cell(%5Cbeta_0%2C%20%5Cbeta_1)%20%3D%20%5CPi_%7Bi%3Ay_1%7D%20%3D%201%20p(x_i)%20%5CPi_%7Bi%5E%7B'%7D%3Ay%5E%7B'%7D%20%3D%200%7D%20(1%20-%20p(x))%20" alt=" \ell(\beta_0, \beta_1) = \Pi_{i:y_1} = 1 p(x_i) \Pi_{i^{'}:y^{'} = 0} (1 - p(x)) " />.O Intercepto na regressão logística não costuma ser de grande interesse, ele é usado para ajustar a média das probabilidades fitadas à proporção de uns

### **Linear Discriminant Analysis**

Para classificação multiclass podemos utilizar um método que lida melhor com várias classes. Para esse caso nos modelamos a distribuição do preditores X separadamente em cada uma das classes e usa o teorema de Bayes para tornar isso em probabilidades. Quando essas distribuições são normais esse modelo assimila-se muito a regressão logística. Quando temos classes bem separadas a regressão logística tende a ser instável. Se temos poucas amostras e as distribuições são normais, mais uma vez o LDA é mais estável.

-Usando o teorema de Bayes: vamos assumi que <img src="https://tex.s2cms.ru/svg/%20%5Cpi_k%20" alt=" \pi_k " /> representa a probabilidade a priori de que uma amostra escolhida ao acaso pertença a classe K. Assumamos também que <img src="https://tex.s2cms.ru/svg/%20f_k%20(x)%20%3D%20Pr(X%20%3D%20x%20%7C%20Y%20%3D%20k)%20" alt=" f_k (x) = Pr(X = x | Y = k) " /> é a função densidade de um ponto aleatório que pertença a K. Ou seja <img src="https://tex.s2cms.ru/svg/%20f_k(x)%20" alt=" f_k(x) " /> terá um valor próximo a um caso X = x para K e pequeno caso contrário. Então por Bayes podemos concluir <img src="https://tex.s2cms.ru/svg/%20Pr(%20X%20%3D%20x%20%7C%20Y%20%3D%20k%20)%20%3D%20%5Cfrac%7B%5Cpi_k%20f_k%20(x)%7D%7B%5Csum_%7Bi%3D1%7D%5E%7BK%7D%20%5Cpi_%7B%5Cint%7D%20f_%7B%5Cint%7D%20(k)%7D%20" alt=" Pr( X = x | Y = k ) = \frac{\pi_k f_k (x)}{\sum_{i=1}^{K} \pi_{\int} f_{\int} (k)} " /> onde podemos resumir <img src="https://tex.s2cms.ru/svg/%20Pr(X%20%3D%20x%20%7C%20Y%20%3D%20k)%20%3D%20P_k(x)%20" alt=" Pr(X = x | Y = k) = P_k(x) " /> sendo a probabilidade a posteriori, ou seja , a probabilidade de um ponto pertencer a classe K dado o valor do preditor X.

Agora precisamos definir <img src="https://tex.s2cms.ru/svg/%20f%0A_k(x)%20" alt=" _k(x) " />, e para tal devemos fazer algumas suposições, tal como consideramos que essa função de densidade é uma gaussiana, para isso temos:

<img src="https://tex.s2cms.ru/svg/%0Af_k(x)%20%3D%20%5Cfrac%7B1%7D%7B%20%5Csqrt%7B2%5Cpi%5Csigma_k%7D%20%7D%20e%5E%7B%5Cbig%20(%20-%20%5Cfrac%7B1%7D%7B2%5Csigma%5E2_k%7D%20(x%20-%20%5Cmu_k)%20%5Cbig%20)%7D%0A" alt="f_k(x) = \frac{1}{ \sqrt{2\pi\sigma_k} } e^{\big ( - \frac{1}{2\sigma^2_k} (x - \mu_k) \big )}" />

Onde `$$` e `$$` são a média e a variância respectivamente do preditor X de dada classe K.

Assim podemos concluir: `$$`

Aplicando log para a função acima temos a função discriminante:

`$$`, e tem no nome linear pois essa é uma função linear em X. Estima-se os valores da média variância e quando não se tem o `$$` pode-se estima-lo de acordo com a proporção de cada classe da variável resposta, o valor do discriminante é calculado para cada classe, e o valor mais alto será associado a classe.

Para o caso de mais de um preditor temos que as variáveis tem distribuição normal entre elas e uma matriz de covariância. A distribuição multi-gaussiana:

`$$`

E a função descriminante:

`$$`

### **Quadratic Discriminant Analysis**

Comporta-se da mesma forma que o LDA, porém agora considera que cada classe possui sua própria matriz de covariância, o que é computacionalmente mais caro (vai de p*(p+1)/2 para isso vezes k), porém quando essa premissa não é mantida é mais indicado usar o QDA. O QDA acaba sendo mais flexível porem acaba tendo maior variância e menor bias.

### **Subset Selection**

Nesse método fitamos todas as possibilidades de regressões para todos os p preditores. Ao final olhamos todas as possíveis combinações e escolhemos a com a melhor métrica. Computacionalmente `$$`. Este modelo torna-se computacionalmente inviável, além de que quando p cresce indefinidamente teremos alta variância dos coeficientes, tal como overfitting.

### **Foward Selection**

Diferente do subset selection esse modelo é computacionalmente viável para p com grandes valores, pois sua complexidade computacional é 1 + p(p+1)/2. Esse método começa com apenas uma variável e vai até p para cada vez que há um incremento no modelo em alguma métrica, seja RSS ou `$$`. Esse método pode ser usado em casos em que `$$` porém, será possível apenas construir modelo até `$$`, como o método utilizado aqui é o Least Square e o mesmo não irá dar uma solução única quando `$$`.

### **Backward Selection**

Assim como o Foward, o Backward é viável computacionalmente, pois sua complexidade é a`$$` Porém, diferente do Foward, esse método começa com todas as variáveis e vai retirando-as de forma a otimizar a métrica de validação. Esse método no caso precisa que `$$`, pois como se começa com todas as variáveis se `$$` então logo no primeiro modelo o Least Square dará mais de uma solução possível.

### **Métodos de seleção dos Sub-modelos**

`$$`, aonde `$$` é a estimativa do erro associado com cada métrica aplicada na resposta (RSS ou `$$`). Essa métrica adiciona uma penalidade de `$$` ao RSS de treino afim de ajustar a métrica já que a mesmo superestima a métrica de teste. A penalidade aumenta junto com o aumento de preditores. Quanto menor `$$` melhor o modelo se ajustará ao treino.

### **AIC**: 

é um critério definido para uma larga escala de modelos que usam o Maximum Likelihood. (Para o caso de modelos com erros gaussianos o LS é igual ao Maximum Likelihood).

`$$` para modelos com LS o `$$` e o AIC são similares.

### **BIC**: 

esse método no caso parte de um ponto de vista Bayesiano. `$$`. Quanto menor o valor, melhor o modelo. Devida a formulação do BIC ele tende a modelos com menos variáveis do que `$$` pois o mesmo penaliza mais modelos com n > 7.

`$$`: Diferente das outra métricas aqui quanto maior o valor melhor o modelo. Essa métrica penaliza o `$$` por incluir variáveis que não melhoram o modelo, como ruídos, assim temos um preço a pagar para cada variável que adicionamos ao modelo.

## **Ridge Pregression**

É um método de regularização que visa penalizar os <img src="https://tex.s2cms.ru/svg/%20%5Cbeta%20" alt=" \beta " /> da regressão de forma que :<img src="https://tex.s2cms.ru/svg/%20%5Csum_%7Bi%3D1%7D%5En(y_i%20-%20%5Cbeta_0%20-%20%5CSigma_%7Bj%3D1%7D%5Ep%20x_%7Bij%7D)%5E2%20%2B%20%5Clambda%20%5Csum_%7Bj%3D1%7D%5E%7Bp%7D%20%5Cbeta_j%5E2%20%3D%20RSS%20%2B%20%5Clambda%20%5Csum_%7Bj%3D1%7D%5Ep%20%5Cbeta_j%5E2%20" alt=" \sum_{i=1}^n(y_i - \beta_0 - \Sigma_{j=1}^p x_{ij})^2 + \lambda \sum_{j=1}^{p} \beta_j^2 = RSS + \lambda \sum_{j=1}^p \beta_j^2 " /> . Ou seja, o segundo termo só é minimizado se os valores de <img src="https://tex.s2cms.ru/svg/%20%5Cbeta%20" alt=" \beta " /> são pequenos, dessa forma para minimizar a equação toda <img src="https://tex.s2cms.ru/svg/%20%5Cbeta%20" alt=" \beta " /> tem que ser grande o suficiente para reduzir o <img src="https://tex.s2cms.ru/svg/%20RSS%20" alt=" RSS " /> mas pequeno o suficiente para não crescer o segundo termo. O coeficiente <img src="https://tex.s2cms.ru/svg/%20%5Clambda%20" alt=" \lambda " /> define o quanto o segundo termo vai penalizar os coeficientes, onde se <img src="https://tex.s2cms.ru/svg/%20%5Clambda%20%5Crightarrow%20%5Cinfty%20" alt=" \lambda \rightarrow \infty " />  os valores dos coeficientes tenderão a zero, e caso <img src="https://tex.s2cms.ru/svg/%20%5Clambda%20%3D%200%20" alt=" \lambda = 0 " />, teremos o Least Square convencional. A notação <img src="https://tex.s2cms.ru/svg/%20%7C%7C%20%5Cbeta%7C%7C_2%20%3D%20%5Csqrt%7B%5CSigma_%7Bj%3D1%7D%5Ep%20%5Cbeta_j%5E2%7D" alt=" || \beta||_2 = \sqrt{\Sigma_{j=1}^p \beta_j^2}" /> é a norma <img src="https://tex.s2cms.ru/svg/%20%5Cell2%20" alt=" \ell2 " /> que mede a distância do quanto o <img src="https://tex.s2cms.ru/svg/%20%5Cbeta%20" alt=" \beta " /> está longe do zero. Quanto maior <img src="https://tex.s2cms.ru/svg/%20%5Clambda%20" alt=" \lambda " /> menor será a norma <img src="https://tex.s2cms.ru/svg/%20%5Cell2%20" alt=" \ell2 " />, da mesma forma que <img src="https://tex.s2cms.ru/svg/%20%5Cfrac%7B%20%7C%7C%20%5Chat%5Cbeta_%7B%5Clambda%7D%5E%7BR%7D%20%7C%7C_2%20%7D%7B%20%7C%7C%20%5Chat%5Cbeta%20%7C%7C_2%20%7D%20" alt=" \frac{ || \hat\beta_{\lambda}^{R} ||_2 }{ || \hat\beta ||_2 } " />. Esse último valor varia de 1 (quando lambda é zero) até 0 (quando lambda é 1), assim podemos ver essa métrica como o quanto o coeficiente ridge foi reduzido à zero. Um valor pequeno significa que ele foi reduzido a bem próximo de zero. Os valores de redução de uma regressão ridge não irão depender apenas de lambda, mas também da escala de seus próprios dados, pois uma variável em reais mil e reais um será mais ou menos reduzida pela ridge dependendo de sua escala, tal como da escala de outras variáveis. Dessa forma é melhor utilizar a Ridge quando os preditores tiverem todos escalados. <img src="https://tex.s2cms.ru/svg/%20%5C~%7Bx%7D_%7Bij%7D%20%3D%20%5Cfrac%7B%20x_%7Bij%7D%20%7D%7B%20%5Csqrt%7B%20%5Cfrac%7B1%7D%7Bn%7D%20%5CSigma_%7Bi%3D1%7D%5En%20(x_%7Bij%7D%20-%20%5C%3D%7Bx%7D_j)%5E2%20%7D%20%7D%20" alt=" \~{x}_{ij} = \frac{ x_{ij} }{ \sqrt{ \frac{1}{n} \Sigma_{i=1}^n (x_{ij} - \={x}_j)^2 } } " />.

Ridge Regression mostra-se melhor do que o OLS pois o mesmo consegue variar a flexibilidade do modelo, ou seja, consegue aumentar a bias, porém reduzir a variância, o que reduz o MSE que é propósito final do modelo. Outra vantagem do Ridge sobre o OLS é que para valores onde p é próximo de n ou até maior o OLS pode não ter uma solução única, onde o Ridge consegue alcançar valores bons fazendo uma troca entre bias e variância. Além disso ele possui uma complexidade computacional não tão alta pois encontrar um bom valor de lambda não é difícil como computar o subset selection.

## **Lasso Pregression**

Ridge possui uma óbvia desvantagem sobre Subset/Foward/Backward selection, ele força a regressão a sempre usar todos os preditores, pois a não ser que lambda tenda infinito, os coeficientes nunca serão exatamente iguais a zero. Já com a Lasso Regression podemos forçar alguns coeficientes a serem igual a zero com um lambda suficientemente grande.

<img src="https://tex.s2cms.ru/svg/%0A%5Csum_%7Bi%3D1%7D%5En%20%5Cbig%20(%20y_i%20-%20%5Cbeta_0%20-%20%5Csum_%7Bj%3D1%7D%5Ep%20%5Cbeta_j%20x_%7Bij%7D%20%5Cbig%20)%20%5E%202%20%2B%20%5Clambda%20%5Csum_%7Bj%3D1%7D%5Ep%20%5Cbeta_j%5E2%20%3D%20RSS%20%2B%20%5Clambda%20%5Csum_%7Bj%3D1%7D%5Ep%7C%5Cbeta_j%7C%0A" alt="\sum_{i=1}^n \big ( y_i - \beta_0 - \sum_{j=1}^p \beta_j x_{ij} \big ) ^ 2 + \lambda \sum_{j=1}^p \beta_j^2 = RSS + \lambda \sum_{j=1}^p|\beta_j|" />

Diferente de ridge que usa a norma <img src="https://tex.s2cms.ru/svg/%20%5Cell1%20" alt=" \ell1 " /> dada por <img src="https://tex.s2cms.ru/svg/%20%5Csum%20%7C%5Cbeta_j%7C%20" alt=" \sum |\beta_j| " />.

Lasso pode zerar os coeficientes devido em sua formulação o formato do mesmo é um diamante, e do ridge é uma esfera, o que significa que as elipses de RSS constantemente irão encontrar as constrições de Lasso no eixo, ou seja, quando algum coeficiente é zero.

No caso em que temos p>n, Lasso irá selecionar no máximo n variáveis antes de saturar, por causa da natureza do dos métodos convexos de otimização. Genericamente ele não funciona tão bem para casos onde p>n.

Se existir no conjunto de p variáveis duas variáveis muito correlacionadas, Lasso tende a zerar uma delas sem se importar com qual e manter apenas a outra, ignorando a relevância da outra.

Para casos onde existe muita correlação entre os preditores é observado empiricamente que a regularização Ridge se sobressai a Lasso.

### **Observações Lasso e Ridge**

*Ridge leva em consideração a correlação entre os preditores, o que faz com que ele funcione diferente quando o mesmo acontece. Ridge tenta encolher os coeficientes de forma proporcional, ao caso em que quando temos preditores correlacionados ele tenta penalizar um mais que o outro a fim de isolar seus efeitos.

*Para ambos os métodos no paper sempre foi feito a normalização dos dados.

*No ridge o que parece ter motivado o autor foi o fato de OLS não funcionar bem quando existe correlação entre os preditores, ai tentou-se reduzir a importância dos coeficientes, ao mesmo tempo que queria-se reduzir o MSE, e como OLS é um modelo com bias muito baixo, tanto ridge quanto Lasso propuseram aumentar o bias pouco o suficiente que faria com que a variância caísse bem, assim reduzindo o MSE.

*Quando a correlação entre os preditores é zero, ridge realiza o encolhimento de forma proporcional.

## **Principal Components Analysis**

Dizemos que <img src="https://tex.s2cms.ru/svg/%20Z_1%2C%20Z_2%2C%20...%2C%20Z_m%20" alt=" Z_1, Z_2, ..., Z_m " /> representa <img src="https://tex.s2cms.ru/svg/%20M%20%3C%20p%20" alt=" M &lt; p " /> combinações lineares dos preditores p. Ou seja <img src="https://tex.s2cms.ru/svg/%20Z_m%20%3D%20%5CSigma_%7Bj%3D1%7D%5Ep%20%5Cphi_%7Bjm%7D%20X_j%20" alt=" Z_m = \Sigma_{j=1}^p \phi_{jm} X_j " /> para as constantes <img src="https://tex.s2cms.ru/svg/%20%5Cphi_%7B1m%7D%2C%20%5Cphi_%7B2m%7D%2C%20...%2C%20%5Cphi_%7Bpm%7D%2C%20m%20%3D%201%2C%20...%2C%20M%20" alt=" \phi_{1m}, \phi_{2m}, ..., \phi_{pm}, m = 1, ..., M " />. Assim podemos depois fitar a regressão linear <img src="https://tex.s2cms.ru/svg/%20y_i%20%3D%20%5Ctheta_0%20%2B%20%5CSigma_%7Bm%3D1%7D%5EM%20%5Ctheta_m%20Z_%7Bim%7D%20%2B%20%5Cin_i%2C%20i%20%3D%201%2C%20...%2C%20n%20" alt=" y_i = \theta_0 + \Sigma_{m=1}^M \theta_m Z_{im} + \in_i, i = 1, ..., n " /> 

PCA tenta achar uma representação em poucas dimensões dos dados que contenha o máximo possível de sua variância. Cada uma das dimensões encontradas pelo PCA é uma combinação linear das p dimensões. O primeiro componente é a combinação normalizada de

<img src="https://tex.s2cms.ru/svg/%20Z_1%20%3D%20%5Cphi_%7B11%7D%20X_1%20%2B%20%5Cphi_%7B21%7D%20X_2%20%2B%20...%20%2B%20%5Cphi_%7Bp1%7DX_p%20" alt=" Z_1 = \phi_{11} X_1 + \phi_{21} X_2 + ... + \phi_{p1}X_p " />, que possui a maior variância. Por normalização dizemos que <img src="https://tex.s2cms.ru/svg/%20%5CSigma_j%5Ep%20%5Cphi_%7Bj1%7D%5E2%20%3D%201" alt=" \Sigma_j^p \phi_{j1}^2 = 1" /> Nos referimos a <img src="https://tex.s2cms.ru/svg/%20%5Cphi%20" alt=" \phi " /> como os pesos do principal componente. Junto eles formam o vetor de pesos do principal componente <img src="https://tex.s2cms.ru/svg/%20%5Cphi_1%20%3D%20(%5Cphi_%7B11%7D%20%2B%20%5Cphi_%7B21%7D%20%2B%20...%20%2B%20%5Cphi_p1)%5ET" alt=" \phi_1 = (\phi_{11} + \phi_{21} + ... + \phi_p1)^T" /> Para calcular os principais componentes, como o que importa é a variância, devemos fazer com que nossos preditores tenham média zero. Ou seja temos que: <img src="https://tex.s2cms.ru/svg/%20maximizar%20%5Cbigg%5C%7B%20%5Cfrac%7B1%7D%7Bn%7D%20%5CSigma_i%3D1%5En%20(%5CSigma_%7Bj%3D1%7D%5Ep%20%5Cphi_%7Bj1%7Dx_%7Bij%7D)%20%5Cbigg%5C%7D%20" alt=" maximizar \bigg\{ \frac{1}{n} \Sigma_i=1^n (\Sigma_{j=1}^p \phi_{j1}x_{ij}) \bigg\} " />. Ou seja, maximizar a variância sobre as n amostras ao longo de <img src="https://tex.s2cms.ru/svg/%5CSigma_%7Bj%3D1%7D%5Ep%20%5Cphi_%7Bj1%7D%5E2" alt="\Sigma_{j=1}^p \phi_{j1}^2" />. O primeiro componente simboliza a direção no espaço na qual os dados mais variam. Para o segundo componente usamos a combinação linear de <img src="https://tex.s2cms.ru/svg/%20X_1%20...%20X_n%20" alt=" X_1 ... X_n " /> que tenha a máxima variância, porém que não seja correlacionado com o primeiro componente, ou seja, tem que ser ortogonal ao PC1.

Os principais componentes promovem superfícies lineares em baixa dimensão que são próximas das observações.

Em PCA os dados precisam ser individualmente normalizados ou caso contrário o primeiro componente pode ficar enviesado pela variável com maior variância individual.

Para saber o quanto da variância cada componente contempla devemos primeiro considerar para os dados normalizados a variância total como <img src="https://tex.s2cms.ru/svg/%20%5CSigma_%7Bj%3D2%7D%5Ep%20Var(X_j)%20%3D%20%5CSigma_%7Bj%3D1%7D%5Ep%20%5Cfrac%7B1%7D%7Bn%7D%20%5CSigma_%7Bi%3D1%7D%5En%20x_%7Bij%7D%5E2%20" alt=" \Sigma_{j=2}^p Var(X_j) = \Sigma_{j=1}^p \frac{1}{n} \Sigma_{i=1}^n x_{ij}^2 " /> e a variância explicada por cada componente como <img src="https://tex.s2cms.ru/svg/%20%5Cfrac%7B1%7D%7Bn%7D%20%5CSigma_%7Bi%3D1%7D%5En%20z_%7Bim%7D%5E2%20%3D%20%5Cfrac%7B1%7D%7Bn%7D%20%5CSigma_%7Bi%3D1%7D%5En%20%5Cbigg(%20%5CSigma_%7Bj%3D1%7D%5Ep%20%5Cphi_%7Bjm%7D%20x_%7Bij%7D%20%5Cbigg)%5E2%20" alt=" \frac{1}{n} \Sigma_{i=1}^n z_{im}^2 = \frac{1}{n} \Sigma_{i=1}^n \bigg( \Sigma_{j=1}^p \phi_{jm} x_{ij} \bigg)^2 " />.x

## **Support Vector Machine**

### **Hiperplano**: 

é um plano em p-1 dimensões <img src="https://tex.s2cms.ru/svg/%20%5Cbeta_0%20%2B%20%5Cbeta_0X_0%20%2B%20%5Cbeta_2X_2%20%2B%20...%20%2B%20%5Cbeta_nX_n%20" alt=" \beta_0 + \beta_0X_0 + \beta_2X_2 + ... + \beta_nX_n " />, onde quando essa equação se iguala a zero podemos considerar que o ponto pertence ao hiperplano. Quando temos essa equação >0 podemos dizer que o ponto se encontra a cima do hiperplano, e quando é <0 podemos dizer que está abaixo. Ou seja, estamos separando o hiperespaço em dois sub hiperespaços de acordo com nosso hiperplano. Um pode mostrar que conseguimos saber em qual parte do espaço está apenas pelo sinal a direita da inequação.

### **Classificador de margem máxima**: 

as margens são espaços do hiperespaço onde é possível separar linearmente as classes com um hiperplano de forma que existem infinitas possibilidades de movimentação desse hiperplano sem encostar em nenhum ponto. Elas são escolhidas a partir do mínimo ponto mais distante de separação das classes. Dessa forma o hiperplano separador é o plano intermediário das máximas margens. Observa-se que os pontos separáveis mais próximos são os únicos que influenciam as margens, e por isso são conhecidos como vetores de suporte.

### **Construção da máxima margem**: 

maximizar M sujeito a

<img src="https://tex.s2cms.ru/svg/%20%7B%20%5Csum_%7Bj%3D1%7D%5Ep%5Cbeta_j%5E2%20%3D%201%2C%20y_i(%5Cbeta_0%20%2B%20%5Cbeta_1X_1%20%2B%20...%20%2B%20%5Cbeta_pX_ip)%20%5Cgeq%20M%20%7D%20%5Cquad%20%5Cforall%20%5Cquad%20%7Bi%20%3D%201%2C%20...%2C%20n%7D%20" alt=" { \sum_{j=1}^p\beta_j^2 = 1, y_i(\beta_0 + \beta_1X_1 + ... + \beta_pX_ip) \geq M } \quad \forall \quad {i = 1, ..., n} " /> . A última etapa da otimização garante que todos os pontos estejam na classe correta, e no mínimo a M distância do hiperplano, que seria a distância até as margens.

*O tamanho das margens pode ser considerado o nível de confiança do modelo. E quando pequenas variações mudam drasticamente o hiperplano, provavelmente teremos overfitting.

Para casos onde os dados não são perfeitamente linearmente separáveis é chamado de classificador por vetores de suporte, ou soft margem classificador. Nesse caso permitimos que alguns pontos estejam do lado errado da margem ou até do hiperplano, visando robustez para novos dados, e um erro geral menor.  Para esse caso devemos maximizar M sujeito a

<img src="https://tex.s2cms.ru/svg/%20%20%5Csum_%7Bj%3D1%7D%5Ep%5Cbeta_j%5E2%20%3D%201%2C%20y_i(%5Cbeta_0%20%2B%20%5Cbeta_1X_1%20%2B%20...%20%2B%20%5Cbeta_pX_ip)%20%5Cgeq%20M%20(1%20-%20%5Cin_%7Bi%7D%20%5Cgeq%200%2C%20%5Csum_%7Bi%3Dn%7D%5En%20%5Cin_i%20%5Cleq%20C%20" alt="  \sum_{j=1}^p\beta_j^2 = 1, y_i(\beta_0 + \beta_1X_1 + ... + \beta_pX_ip) \geq M (1 - \in_{i} \geq 0, \sum_{i=n}^n \in_i \leq C " />, onde C é um hiperparâmetro não negativo. Os valores de erros nos dizem aonde estão esses erros em relação ao hiperplano, se for igual a 0 então a ith observação está no lado correto, se o erro é maior que zero então ela está do lado errado da margem, e se for maior que um está do lado errado do hiperplano. O parâmetro C decide quantas amostras podem estar do lado errado do hiperplano, valores altos significam que ficamos mais tolerantes a errar a classificação e quando C igual a 0 temos o classificador de margem máxima. Nesse classificador apenas pontos que caem na margem ou erroneamente na margem interferem nos vetores de suporte, os que violam o hiperplano não são considerados.

Complexidade: **O**(max(n,d) min (n,d)^2), onde n é o número de pontos e d o número de dimensões.

## **KNN**

KNN é um método bem simplista com complexidade de <img src="https://tex.s2cms.ru/svg/%20O(n%5E2)%20" alt=" O(n^2) " />. Ele funciona classificando um ponto ou regredindo para seus <img src="https://tex.s2cms.ru/svg/%20k%20" alt=" k " /> vizinhos mais próximos. Para uma classificação pode ser uma votação e para regressão uma média. Além da complexidade computacional, para bases de treino muito extensas torna-se pesado carregar a base toda em memória para calcular todas as distâncias. Algumas abordagens para isso foram levantadas como o CNN (Condensed Nearest Neighbours), que retira dados com um padrão muito próximo, e até o Reduced Nearest Neighbours que retira pontos que não alteram o erro do modelo.

Normalmente é usada a distância euclidiana para calcular as distâncias, ou em caso de variáveis binárias podemos usar a Manhattan. E por se tratar de distância devemos normalizar os dados para que um dado com grande range não influencie mais que os outros. Além de termos que tratar missing, por não existir distância até ele.

Podemos usar diversas distâncias como: euclidiana, Manhattan, Minkowski, Mahalanobis , Jaccard, ShivChev e outras.

Para classificação a probabilidade de determinada classe é a probabilidade frequentista dos vizinhos, então se temos 3 vizinhos e dois votam na classe Y1 então a probabilidade de ser Y1 é 2/3. Caso haja empate ou é escolhido aleatoriamente a classe ou é adicionado mais um vizinho para tirar o empate. Depende do algoritmo.

## **Fuzzy C Means**

O fuzzy c means é um algoritmo de clusterização que possibilita indicar mais de um cluster por ponto do espaço, devida a sua habilidade de calcular a probabilidade de um determinado ponto pertencer a um cluster, ele escolhe um thershold para classificar como de dois clusters. Funciona como o Kmeans, mas calcula a probabilidade de estar em cada cluster, e qual tiver mais probabilidade é indicado para recalcular os centroides.

## Complexidades Computacionais:

**Kmeans**: <img src="https://tex.s2cms.ru/svg/%20O(iknd)%20" alt=" O(iknd) " />: n pontos d dimensões k cluster e i inicializações;

**Hierárquico**: <img src="https://tex.s2cms.ru/svg/%20O(n%5E2)%20" alt=" O(n^2) " />: n pontos

**DBScan**: <img src="https://tex.s2cms.ru/svg/%20O(n%5E2)%20" alt=" O(n^2) " /> pode ser reduzido <img src="https://tex.s2cms.ru/svg/O(n%20*%20%5Clog(n))" alt="O(n * \log(n))" />

**SVM**: <img src="https://tex.s2cms.ru/svg/%20O(%5Ctext%7Bmax%7D(n%2Cd)%20%5Ctext%7Bmin%7D(n%2Cd)%5E2)%20" alt=" O(\text{max}(n,d) \text{min}(n,d)^2) " />, onde n é o número  de pontos e d o número de dimensões.

**Tree**: <img src="https://tex.s2cms.ru/svg/%20O(d%20*%20%5Ctext%7Blog%7D(n))%20" alt=" O(d * \text{log}(n)) " />: com n pontos e d dimensões

**Random Forest**: <img src="https://tex.s2cms.ru/svg/%20O(e%20*%20d%20*%20%5Ctext%7Blog%7D(n))%20" alt=" O(e * d * \text{log}(n)) " />: com n pontos e d dimensões e “e” estimadores se não for paralelo

**Boosting Tree**: <img src="https://tex.s2cms.ru/svg/O(e%20*%20d%20*%20%5Ctext%7Blog%7D(n))" alt="O(e * d * \text{log}(n))" />: com n pontos e d dimensões e “e” estimadores.

**KNN**: <img src="https://tex.s2cms.ru/svg/O(n*d%2Bk*n)" alt="O(n*d+k*n)" /> ou <img src="https://tex.s2cms.ru/svg/O(n*d*k)" alt="O(n*d*k)" />  com n pontos e d dimensões e k vizinhos.

**Naive Bayes**: <img src="https://tex.s2cms.ru/svg/O(n*d)" alt="O(n*d)" />

**Linear Regression**: <img src="https://tex.s2cms.ru/svg/%20O(d%5E2%20*%20(n%2Bd))%20" alt=" O(d^2 * (n+d)) " />