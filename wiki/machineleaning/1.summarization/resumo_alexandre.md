# Modelos

## Regressoes

### Arvore

Divide os dados pensando visando minimizar a função RSS (Residuals Sum of Squares) .

$$\sum_{j=1}^{j} \sum_{i \in R_{j}} (y_i - \hat{y}_{R_{j}})$$

Aonde yR é a média da resposta para as observações de treino pertencentes ao box J. Como é computacionalmente inviável tentar todas as partições existentes do espaço, por isso usa-se uma abordagem de cima para baixa chamada “recursive binary splitting”. A árvore não é totalmente otimizada pois ele escolhe a melhor variável e melhor quebra para aquele determinado passo, e não visando o final do algoritmo.

Para fazer a recursive binary splitting é necessário escolher a variável V e o ponto de corte PC que mais minimiza o RSS. Então para cada V e PC calcula-se a função a abaixo e escolhe-se a que mais minimiza o RSS

$$\sum_{i: x_{1} \in R_{1} (j_{1} s)} (y_{i} - \hat{y}_{R_{1}})^2 + \sum_{i: x_{1} \in R_{2}(j_{1}s)} (y_{i} - \hat{y}_{R_{2}})^2$$Árvores podem crescer indefinitivamente, o que pode gerar overfitting, o que aumenta o erro na base de teste. Para resolver esse problema usa-se a poda das árvores, que visa otimizar o crescimento da árvore que reduz RSS com a complexidade de se ter muitas folhas, assim reduzindo o tamanho da árvore calculando sub-árvores e validando o valor do erro do teste para cada uma.

$$\sum_{m=1}^{|T|} \sum_{i: x_{1} \in R_{m}} (y_{1} - \hat{y}_{R_{m}})^2 + a|T|$$

#### Pros & cons

| Algorithm               | Best at                                                                     | Pros                                                                                                                                                                     | Cons                                                                                              | 
|-------------------------|-----------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------| 
| Arvores de Decisao      | classificacao de estrelas / galaxias                                        | rapido                                                                                                                                                                   | arvores complexas são dificeis de interpretar                                                     | 
|                         | diagnostico medico                                                          | robusta para "noise" e valores missing                                                                                                                                   | é possivel duplicar uma sub-arvore dentro de uma mesma estrutura de arvore                       | 
|                         | analise de credito                                                          | eé bastante acurada (precisa)                                                                                                                                            | pode overfitar facilmente                                                                         | 
|                         |                                                                             | eé um modelo considerado "non-parametric" isso eé, ele não assume nenhuma caracteristica dos dados (exemplo: que seja distribuicao normal, que tenha linearidade, etc..) |                                                                                                   | 
|                         |                                                                             | no distribution requirement                                                                                                                                              |                                                                                                   | 
|                         |                                                                             | eé um metodo heiristico (isso eé, ele aprende na pratica consigo mesmo)                                                                                                  |                                                                                                   | 
|                         |                                                                             | não sofre com multicolineraidade (parametros correlacionados)                                                                                                            |                                                                                                   | 



### **Regressão Linear**

Linear Regression establishes a relationship between  **dependent variable (Y)**  and one or more  **independent variables (X)**  using a  **best fit straight line**  (also known as regression line).

It is represented by an equation  $Y=a+bX + e$, where a is intercept, b is slope of the line and e is error term. This equation can be used to predict the value of target variable based on given predictor variable(s).

[![Linear_Regression](https://www.analyticsvidhya.com/wp-content/uploads/2015/08/Linear_Regression1.png)](https://www.analyticsvidhya.com/wp-content/uploads/2015/08/Linear_Regression1.png)

The difference between simple linear regression and multiple linear regression is that, multiple linear regression has (>1) independent variables, whereas simple linear regression has only 1 independent variable. Now, the question is “How do we obtain best fit line?”.

#### How to obtain best fit line (Value of a and b)?

This task can be easily accomplished by Least Square Method. It is the most common method used for fitting a regression line. It calculates the best-fit line for the observed data by minimizing the sum of the squares of the vertical deviations from each data point to the line. Because the deviations are first squared, when added, there is no cancelling out between positive and negative values.

$$
{\min_w ||Xw-y||_2}^2
$$

[![reg_error](https://www.analyticsvidhya.com/wp-content/uploads/2015/08/reg_error.gif)](https://www.analyticsvidhya.com/wp-content/uploads/2015/08/reg_error.gif)

We can evaluate the model performance using the metric **R-square**. To know more details about these metrics, you can read: Model Performance metrics [Part 1](https://www.analyticsvidhya.com/blog/2015/01/model-performance-metrics-classification/), [Part 2](https://www.analyticsvidhya.com/blog/2015/01/model-perform-part-2/) .

#### Important Points:

-   There must be  **linear relationship**  between independent and dependent variables
-   Multiple regression suffers from  **multicollinearity, autocorrelation, heteroskedasticity**.
-   Linear Regression is very sensitive to  **Outliers**. It can terribly affect the regression line and eventually the forecasted values.
-   Multicollinearity can increase the variance of the coefficient estimates and make the estimates very sensitive to minor changes in the model. The result is that the coefficient estimates are unstable
-   In case of multiple independent variables, we can go with  **forward selection**,  **backward elimination**  and  **step wise approach**  for selection of most significant independent variables.

-----------

Tenta-se ajustar os dados de forma a regredir os valores a uma reta ou um hiperplano linear visando minimizar o RSS (Residuals Sum os Squares). Para tal estima-se n parâmetros. Um dos métodos para estimar esses parâmetros é o cálculo dos mínimos quadrados (Least Squares).

$$\beta_{1} = \frac{\sum_{i=1}^{n} (x_{1} - \bar{x})(y_{1} - \bar{y})}{ \sum_{i=1}^{n} ( x_{1} - \bar{x} )^2 }$$

$$\beta_{2} \bar{y} - \hat{\beta}_{1}\bar{x}_{1}$$

Como não se tem os coeficientes reais para a população é necessário estima-los de forma amostral o que ser obtido através do Standard Error (SE). Se realizarmos n modelos com n tendendo a infinito e para cada modelo usarmos uma amostra para calcular os betas pode-se chegar de forma segura a uma boa aproximação dos parâmetros.

Para se calcula SE se usa: $Var(\hat{\mu})^2 = \frac{\sigma^2}{n}$, onde pode-se observar que quanto maior o n menor o desvio padrão.

$$SE(\hat{\beta}_{0})^2 = \sigma^2 \Bigg [ \frac{1}{n} + \frac{\bar{x}^2}{\sum_{i=1}^{n} (x_{i} - \bar{x})^2} \Bigg ] , SE(\hat{\beta}_1)^2 = \frac{ \sigma^{2} }{ \sum_{i=1}^{n} (x_i - \bar{x})^2 }$$

Para um intervalo de confiança de 95% chega aos valores de $\hat{\beta}_1 \pm 2 SE(\hat{\beta_1})$ da mesma forma que $\hat{\beta_n} \pm 2SE(\hat{\beta_n})$.

#### **Potênciais Problemas na regressão:**

##### **Não linearidade nos dados**:

na regressão linear espera-se que os dados se distribuam e comportem de forma linear, se essa premissa não é verdadeira a assertividade do modelo fica comprometida. Para visualizar melhor essa falta de linearidade podemos plotar o gráfico dos resíduos com o preditor $x_i$, ou no case de multiplas variáveis podemos plotar os resíduos versus os valores preditos. Idealmente esse gráfico não deverám apresentar nenhum padrão, porém em caso de não linearidade poderá indicar um padrão na distribuição. Para corrigirmos essa premissa podemos transformar os preditores que se comportam de forma não linear com $\sqrt{X}, \log{X}$ ou $X^2$

##### **Correlação entre os termos de erro**

$e$: a regressão assume que os error $e_1, e_2 ...$ Não são correlacionados, ou seja, $e_1$ ser positivo não influencia em nada em $e_{i+1}$. Os erros padrão $SE$ são calculado em cima dessa premissa, se essas premissas forem violadas a estimativa do erro padrão estará sendo subestimada, como consequencia os intervalor de confiança e predições serão mais estreitos (menores) que realmente estamos supondo. Por exemplo um intervalo de confiança estimado de 95% pode na verdade ter uma probabilidade de contar os valores muito menor do que 0,95. Esse fenômeno pode constuma ocorrer no contexto de séries temporais, ou quando o tempo ou intervalo de tempo determina a sequencia de valores. Para determinar se existe essa correlação podemos plotar nossos residuos ao longo do tempo, se não existir a correlação os valores não deverão ter um padrão discernivel.

###### **OBS:**

a matriz de erros quando não temos homocedascidade (erro normalmente distribuido com média zero e desvio padrão constante) não é mais um matriz com a diagonal constante e os demais valores iguais a zero. Temos agora uma matriz com heterocedascidade, ou seja variância variável (diagonal), e os demais valores podem diferir de zero ou seja, pode existir correlação entre os erros, essa chama-se matriz de covariancia dos erros. Quando temos esse tipo de fenômeno o Ordinary Least Squares não é mais um modelo adequado, ou seja, ele não conseguira convergir para os melhores coeficientes, para isso pode-se usar o Generalized Least Squares, que é uma tranformação por uma matriz de covariancia semi positiva definida, com variancias não constantes na diagonal e uma ou mais covariancias diferente de zero. Para satisfazer a premissa citada, podemos multiplicar ambos os lados da equação da regressão linear por essa matriz. Assim o GLS é o OLS multiplicando ambos os lados da equação da regressão pela matriz de covariancia dos erros com variancia variável e correlação entre outros valores. Como essa matriz de covariancia normalmente não é conhecida podemos estima-la de forma a que precisamos calcular a média das respostas e a média dos erros afim de chegarmos a: $\hat{\Sigma} = ( N - i )^{-1}.\sum_{t=1}^{N} (y_{t} - \bar{y}) (y_{t} - \bar{y})^{T}$ assim: $\beta_{EGLS} = (X^T \hat{\Sigma}^{-1}X)\bar{y}$

##### **Variância não constante do erros**:

os intervalos de confiança, erros padrão e teste de hipotese seguem essa premissa. Pode-se obersar esse fênomeno através pela presença de um formato de funíl no gráfico dos resíduos pelos valores preditos. Quando esse tipo de problema ocorre uma possível solução é transformar a resposta usando uma função concava como raiz ou log.

##### **Presença de Outliers**:

a presença de outliers além de poder modificar o formato da curva, dependendo da quantidade de outliers e de pontos no modelo, modifica bem o cálculo do SER, que é usado para calcular os intervalos de confiança e o p-value, o que pode causar uma má leitura dos parâmetros do modelo, além de pode invalidar um projeto por ter grande inflência nas métricas da regressão que que usam o RSS e o RSE. Gráficos como box-plot ou o gráfico de residuos por valores preditos podem dar um grande insight de outlier. Uma forma de identificar outlier pode ser pelo studentized residuals que se obtém dividindo cada erro pelo erro padrão, os valores absolutos acima de 3 são possíveis candidatos a outlier.

##### **Ponto com grande influência (poder)**:

são pontos onde alguma variável explicativa é um outlier, esse tipo de problema tende a impactar o Least Squares bem mais que outliers propriamente dito. Para computar pontos de alta influência podemos usar a high leverage Statistc que varia de $\frac{1}{n}$ até 1, onde ela mede a distancia de cada ponto para a média dos pontos de forma normalizada. A leverage média para todos os pontos é de $\frac{p + 1}{n}$ ou seja qualquer valor acima deste pode ser considerado um ponto de influencia alta.

##### **Colinearidade:**

quando temos variáveis que se correlacionam entre si é muito difícil separar o impacto de cada uma. Quando isso acontece temos um problema onde o gráfico de contorno do Least Squares fica muito instável, ou seja, para pequenas variações dos valores, temos uma alteração grande no gráfico de contorno, de forma a aumentar a incerteza dos coeficientes da regressão. Como a colinearidade reduz a assertividade da estimativa dos coeficientes da regressão, isso acaba por crescer muito o erro padrão para os betas. Na presença de coolinearidade podemos acabar rejeitando a hipotese nula, ou seja, colinearidade reduz a chance de acharmos um beta diferente de zero. Podemos verificar a colinearidade pela matriz de correlação entre as variáveis, porém as vezes temos correlação entre 3 variáveis o que fica inviável de detectar com o método anterior, para isso podemos calcular o VIF que é a proporção da variancia de betaX quando é fitado um modelo com todas as variáveis dividido por quando temos só ela. O menos valor do VIF é 1, quando não temos multicolinearidade, e a regra empirica é que quando esse valor excede 5 ou 10 ai temos um problema. Para lidar com esse problema ou dropamos uma das variáveis ou combinamos as mesmas.


#### Pros & cons

| Algorithm               | Best at                                                                     | Pros                                                                                                                                                                     | Cons                                                                                              | 
|-------------------------|-----------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------| 
| regressao linear        | criar predicoes de baseline                                                 | facil de entender e de explicar                                                                                                                                          | muito complicado para fitar em funcoes não lineares                                               | 
|                         | predicoes econometricas                                                     | raramente overfita (It seldom overfits)                                                                                                                                  | sofre com outliers                                                                                | 
|                         | modelar respostas de marketing                                              | usando L1 e L2 eé bom com selecao de features (feature selectin)                                                                                                         | pontos de alta influencia                                                                         | 
|                         |                                                                             | rapido para treinar                                                                                                                                                      | assume a linearidade da funcao & normalidade dos dados                                            | 
|                         |                                                                             | facil de treinar em big data gracas a sua versao estocastica                                                                                                             | assume que os erros nao sao correlacionados                                                       | 



### Polynomial Regression

A regression equation is a polynomial regression equation if the power of independent variable is more than 1. The equation below represents a polynomial equation:

$$
y=a+b*x^2
$$

In this regression technique, the best fit line is not a straight line. It is rather a curve that fits into the data points.

[![Polynomial](https://www.analyticsvidhya.com/wp-content/uploads/2015/08/Polynomial.png)](https://www.analyticsvidhya.com/wp-content/uploads/2015/08/Polynomial.png)

#### Important Points:

-   While there might be a temptation to fit a higher degree polynomial to get lower error, this can result in over-fitting. Always plot the relationships to see the fit and focus on making sure that the curve fits the nature of the problem. Here is an example of how plotting can help:

[![underfitting-overfitting](https://www.analyticsvidhya.com/wp-content/uploads/2015/02/underfitting-overfitting.png)](https://www.analyticsvidhya.com/wp-content/uploads/2015/2/underfitting-overfitting.png)

-   Especially look out for curve towards the ends and see whether those shapes and trends make sense. Higher polynomials can end up producing wierd results on extrapolation.

### Stepwise Regression

This form of regression is used when we deal with multiple independent variables. In this technique, the selection of independent variables is done with the help of an automatic process, which involves _no_  human intervention.

This feat is achieved by observing statistical values like R-square, t-stats and AIC metric to discern significant variables. Stepwise regression basically fits the regression model by adding/dropping co-variates one at a time based on a specified criterion. Some of the most commonly used Stepwise regression methods are listed below:

-   Standard stepwise regression does two things. It adds and removes predictors as needed for each step.
-   Forward selection starts with most significant predictor in the model and adds variable for each step.
-   Backward elimination starts with all predictors in the model and removes the least significant variable for each step.

The aim of this modeling technique is to maximize the prediction power with minimum number of predictor variables. It is one of the method to handle [higher dimensionality](https://www.analyticsvidhya.com/blog/2015/07/dimension-reduction-methods/)  of data set.

### **Local regression** or **local polynomial regression** (LOWESS / LOESS)

This is a method for fitting a smooth curve between two variables, or fitting a smooth surface between an outcome and up to four predictor variables.

The procedure originated as LOWESS (LOcally WEighted Scatter-plot Smoother). Since then it has been extended as a modelling tool because it has some useful statistical properties ([Cleveland, 1998](https://www.statsdirect.com/help/references/reference_list.htm)).

The polynomial is fitted using [weighted least squares](https://en.wikipedia.org/wiki/Weighted_least_squares "Weighted least squares"), giving more weight to points near the point whose response is being estimated and less weight to points further away. The value of the regression function for the point is then obtained by evaluating the local polynomial using the explanatory variable values for that data point. The LOESS fit is complete after regression function values have been computed for each of the ${\displaystyle n}$ data points. Many of the details of this method, such as the degree of the polynomial model and the weights, are flexible. The range of choices for each part of the method and typical defaults are briefly discussed next.

![](https://upload.wikimedia.org/wikipedia/commons/thumb/d/d2/Loess_curve.svg/220px-Loess_curve.svg.png)

#### Advantages

As discussed above, the biggest advantage LOESS has over many other methods is the fact that it does not require the specification of a function to fit a model to all of the data in the sample. Instead the analyst only has to provide a smoothing parameter value and the degree of the local polynomial. In addition, LOESS is very flexible, making it ideal for modeling complex processes for which no theoretical models exist. These two advantages, combined with the simplicity of the method, make LOESS one of the most attractive of the modern regression methods for applications that fit the general framework of least squares regression but which have a complex deterministic structure.

Although it is less obvious than for some of the other methods related to linear least squares regression, LOESS also accrues most of the benefits typically shared by those procedures. The most important of those is the theory for computing uncertainties for prediction and calibration. Many other tests and procedures used for validation of least squares models can also be extended to LOESS models[_[citation needed](https://en.wikipedia.org/wiki/Wikipedia:Citation_needed "Wikipedia:Citation needed")_].

#### Disadvantages

LOESS makes less efficient use of data than other least squares methods. It requires fairly large, densely sampled data sets in order to produce good models. This is because LOESS relies on the local data structure when performing the local fitting. Thus, LOESS provides less complex data analysis in exchange for greater experimental costs.

Another disadvantage of LOESS is the fact that it does not produce a regression function that is easily represented by a mathematical formula. This can make it difficult to transfer the results of an analysis to other people. In order to transfer the regression function to another person, they would need the data set and software for LOESS calculations. In  [nonlinear regression](https://en.wikipedia.org/wiki/Nonlinear_regression "Nonlinear regression"), on the other hand, it is only necessary to write down a functional form in order to provide estimates of the unknown parameters and the estimated uncertainty. Depending on the application, this could be either a major or a minor drawback to using LOESS. In particular, the simple form of LOESS can not be used for mechanistic modelling where fitted parameters specify particular physical properties of a system.

Finally, as discussed above, LOESS is a computationally intensive method (with the exception of evenly spaced data, where the regression can then be phrased as a non-causal  [finite impulse response](https://en.wikipedia.org/wiki/Finite_impulse_response "Finite impulse response")  filter). LOESS is also prone to the effects of outliers in the data set, like other least squares methods. There is an iterative,  [robust](https://en.wikipedia.org/wiki/Robust_statistics "Robust statistics")  version of LOESS [Cleveland (1979)] that can be used to reduce LOESS' sensitivity to  [outliers](https://en.wikipedia.org/wiki/Outliers "Outliers"), but too many extreme outliers can still overcome even the robust method.

#### pros and cons

| Algorithm               | Best at                                                                     | Pros                                                                                                                                                                     | Cons                                                                                              | 
|-------------------------|-----------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------| 
| regressao local (LOESS) | fitar uma curva linear                                                      | simples de rodar                                                                                                                                                         | nao tem uma formula especifica, o que dificulta a transfrencia                                    | 
|                         |                                                                             | capaz que fitar curvas complexas                                                                                                                                         | < tem uma analise mais superficial sobre os dados e aumenta consideravelmente o custo da funcao   | 
|                         |                                                                             |                                                                                                                                                                          | necessita de uma quantidade grande de dados para produzir resultados bons                         | 

## Regularization

### **Lasso & Ridge Pregression**

#### Ridge

Ridge Regression is a technique used when the data suffers from multicollinearity ( independent variables are highly correlated). In multicollinearity, even though the least squares estimates (OLS) are unbiased, their variances are large which deviates the observed value far from the true value. By adding a degree of bias to the regression estimates, ridge regression reduces the standard errors.

Above, we saw the equation for linear regression. Remember? It can be represented as:

$$
y=a+ b*x
$$

This equation also has an error term. The complete equation becomes:

$y=a+b*x+e \text{(error term)}$,  error term is the value needed to correct for a prediction error between the observed and predicted value

$=> y=a+y= a+ b1x1+ b2x2+....+e$, for multiple independent variables.

In a linear equation, prediction errors can be decomposed into two sub components. First is due to the  **biased**and second is due to the **variance**. Prediction error can occur due to any one of these two or both components. Here, we’ll discuss about the error caused due to variance.

Ridge regression solves the multicollinearity problem through [shrinkage parameter](https://en.wikipedia.org/wiki/Shrinkage_estimator)  λ (lambda). Look at the equation below.

[![Ridge](https://www.analyticsvidhya.com/wp-content/uploads/2015/08/Ridge2.png)](https://www.analyticsvidhya.com/wp-content/uploads/2015/08/Ridge2.png)

In this equation, we have two components. First one is least square term and other one is lambda of the summation of β2 (beta- square) where β is the coefficient. This is added to least square term in order to shrink the parameter to have a very low variance.

##### Important Points:

-   The assumptions of this regression is same as least squared regression except normality is not to be assumed
-   It shrinks the value of coefficients but doesn’t reaches zero, which suggests no feature selection feature
-   This is a regularization method and uses  [l2 regularization](https://en.wikipedia.org/wiki/Regularization_(mathematics)).

------------

Ridge regression is an extension for linear regression. It’s basically a regularized linear regression model. The λ parameter is a scalar that should be learned as well, using a method called **cross validation** that will be discussed in another post.

A super important fact we need to notice about ridge regression is that it enforces the βcoefficients to be lower, but it **does not** enforce them to be zero. That is, it will not get rid of irrelevant features but rather **minimize their impact on the trained model**.

[![](https://codingstartups.com/wp-content/uploads/2017/08/ridge.png)](https://codingstartups.com/wp-content/uploads/2017/08/ridge.png)

------------

É um método de regularização que visa penalizar os $\beta$ da regressão de forma que :$\sum_{i=1}^n(y_i - \beta_0 - \Sigma_{j=1}^p x_{ij})^2 + \lambda \sum_{j=1}^{p} \beta_j^2 = RSS + \lambda \sum_{j=1}^p \beta_j^2$ . Ou seja, o segundo termo só é minimizado se os valores de $\beta$ são pequenos, dessa forma para minimizar a equação toda $\beta$ tem que ser grande o suficiente para reduzir o $RSS$ mas pequeno o suficiente para não crescer o segundo termo. O coeficiente $\lambda$ define o quanto o segundo termo vai penalizar os coeficientes, onde se $\lambda \rightarrow \infty$ os valores dos coeficientes tenderão a zero, e caso $\lambda = 0$, teremos o Least Square convencional. A notação $$ || \beta||_2 = \sqrt{\Sigma_{j=1}^p \beta_j^2}$é a norma$ \ell2 $que mede a distância do quanto o$ \beta $está longe do zero. Quanto maior$ \lambda $menor será a norma$ \ell2 $$, da mesma forma que $\frac{ || \hat\beta_{\lambda}^{R} ||_2 }{ || \hat\beta ||_2 }$. Esse último valor varia de 1 (quando lambda é zero) até 0 (quando lambda é 1), assim podemos ver essa métrica como o quanto o coeficiente ridge foi reduzido à zero. Um valor pequeno significa que ele foi reduzido a bem próximo de zero. Os valores de redução de uma regressão ridge não irão depender apenas de lambda, mas também da escala de seus próprios dados, pois uma variável em reais mil e reais um será mais ou menos reduzida pela ridge dependendo de sua escala, tal como da escala de outras variáveis. Dessa forma é melhor utilizar a Ridge quando os preditores tiverem todos escalados. $\~{x}_{ij} = \frac{ x_{ij} }{ \sqrt{ \frac{1}{n} \Sigma_{i=1}^n (x_{ij} - \bar{x}_j)^2 } }$.

Ridge Regression mostra-se melhor do que o OLS pois o mesmo consegue variar a flexibilidade do modelo, ou seja, consegue aumentar a bias, porém reduzir a variância, o que reduz o MSE que é propósito final do modelo. Outra vantagem do Ridge sobre o OLS é que para valores onde p é próximo de n ou até maior o OLS pode não ter uma solução única, onde o Ridge consegue alcançar valores bons fazendo uma troca entre bias e variância. Além disso ele possui uma complexidade computacional não tão alta pois encontrar um bom valor de lambda não é difícil como computar o subset selection.

#### **Lasso**

Similar to Ridge Regression, Lasso (Least Absolute Shrinkage and Selection Operator) also penalizes the absolute size of the regression coefficients. In addition, it is capable of reducing the variability and improving the accuracy of linear regression models. Look at the equation below: 

[![Lasso](https://www.analyticsvidhya.com/wp-content/uploads/2015/08/Lasso-300x107.png)](https://www.analyticsvidhya.com/wp-content/uploads/2015/08/Lasso.png)

Lasso regression differs from ridge regression in a way that it uses absolute values in the penalty function, instead of squares. This leads to penalizing (or equivalently constraining the sum of the absolute values of the estimates) values which causes some of the parameter estimates to turn out **exactly zero**. Larger the penalty applied, further the estimates get shrunk towards absolute zero. This results to variable selection out of given n variables.

##### Important Points:

-   The assumptions of this regression is same as least squared regression except normality is not to be assumed
-   It shrinks coefficients to zero (exactly zero), which certainly helps in feature selection
-   This is a regularization method and uses  [l1 regularization](https://en.wikipedia.org/wiki/Regularization_(mathematics))
-   If group of predictors are highly correlated, lasso picks only one of them and shrinks the others to zero

------------

The only difference from Ridge regression is that the regularization term is in absolute value. But this difference has a huge impact on the trade-off we’ve discussed before. Lasso method overcomes the disadvantage of Ridge regression by not only punishing high values of the coefficients β but actually setting them to zero if they are not relevant. Therefore, you might end up with fewer features included in the model than you started with, which is a huge advantage.

[![](https://codingstartups.com/wp-content/uploads/2017/08/lasso.png)](https://codingstartups.com/wp-content/uploads/2017/08/lasso.png)

-----------

Ridge possui uma óbvia desvantagem sobre Subset/Foward/Backward selection, ele força a regressão a sempre usar todos os preditores, pois a não ser que lambda tenda infinito, os coeficientes nunca serão exatamente iguais a zero. Já com a Lasso Regression podemos forçar alguns coeficientes a serem igual a zero com um lambda suficientemente grande.

$$\sum_{i=1}^n \big ( y_i - \beta_0 - \sum_{j=1}^p \beta_j x_{ij} \big ) ^ 2 + \lambda \sum_{j=1}^p \beta_j^2 = RSS + \lambda \sum_{j=1}^p|\beta_j|$$

Diferente de ridge que usa a norma $\ell1$ dada por $\sum |\beta_j|$.

Lasso pode zerar os coeficientes devido em sua formulação o formato do mesmo é um diamante, e do ridge é uma esfera, o que significa que as elipses de RSS constantemente irão encontrar as constrições de Lasso no eixo, ou seja, quando algum coeficiente é zero.

No caso em que temos p>n, Lasso irá selecionar no máximo n variáveis antes de saturar, por causa da natureza do dos métodos convexos de otimização. Genericamente ele não funciona tão bem para casos onde p>n.

Se existir no conjunto de p variáveis duas variáveis muito correlacionadas, Lasso tende a zerar uma delas sem se importar com qual e manter apenas a outra, ignorando a relevância da outra.

Para casos onde existe muita correlação entre os preditores é observado empiricamente que a regularização Ridge se sobressai a Lasso.

#### **Observações Lasso e Ridge**

 - Ridge leva em consideração a correlação entre os preditores, o que faz com que ele funcione diferente quando o mesmo acontece. Ridge tenta encolher os coeficientes de forma proporcional, ao caso em que quando temos preditores correlacionados ele tenta penalizar um mais que o outro a fim de isolar seus efeitos.

 - Para ambos os métodos no paper sempre foi feito a normalização dos dados.

 - No ridge o que parece ter motivado o autor foi o fato de OLS não funcionar bem quando existe correlação entre os preditores, ai tentou-se reduzir a importância dos coeficientes, ao mesmo tempo que queria-se reduzir o MSE, e como OLS é um modelo com bias muito baixo, tanto ridge quanto Lasso propuseram aumentar o bias pouco o suficiente que faria com que a variância caísse bem, assim reduzindo o MSE.

 - Quando a correlação entre os preditores é zero, ridge realiza o encolhimento de forma proporcional.

#### Pros & cons

| Algorithm               | Best at                                                                     | Pros                                                                                                                                                                     | Cons                                                                                              | 
|-------------------------|-----------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------| 

### ElasticNet Regression

ElasticNet is hybrid of Lasso and Ridge Regression techniques. It is trained with L1 and L2 prior as regularizer. Elastic-net is useful when there are multiple features which are correlated. Lasso is likely to pick one of these at random, while elastic-net is likely to pick both.

$$
\hat{\beta} = argmin_{\beta}( || y - X\beta || ^2 + \lambda_2 || \beta ||^2 + \lambda_1 ||\beta||_1 )
$$

A practical advantage of trading-off between Lasso and Ridge is that, it allows Elastic-Net to inherit some of Ridge’s stability under rotation.

#### Important Points:

-   It encourages group effect in case of highly correlated variables
-   There are no limitations on the number of selected variables
-   It can suffer with double shrinkage

## Classificacoes

### Arvore

é muito similar a árvore de regressão, porém ao invés de RSS ou MSE, usa a taxa de erro da classificação

#### **Gini Index:**

Gini é considerado uma métrica de pureza do nó.

##### Nao confunda:

-   **Gini impurity**  is a measure of misclassification, which applies in a multiclass classifier context.
-   **Gini coefficient**  applies to binary classification and requires a classifier that can in some way rank examples according to the likelihood of being in a positive class.

##### Em geral

é uma métrica da variância total ao longo das K classes: ( $\hat{P}_{mk}$ representa a proporção das observações de treino na região $m_{th}$ pertencente a $k_{th}$ classe). Pode se mostrar que se $\hat{P}mk$ tende a zero ou a um o gini será pequeno. Por esses motivos o gini é considerado uma métrica de pureza do nó, um valor pequeno significa que o nó tem contém predominantemente observações de uma única classe.

$G = \sum_{k=1}^{K} \hat{P}_{mk} (1 - \hat{P}_{mk})_{1}$

#### **Cross-entropy:**

é outra medida de pureza e pode-se mostrar que se $\hat{P}_{mk}$ tende a ser todo 0 ou todo 1 então a entropia tende a zero também.

$$ D = - \sum_{k=1}^{K} \hat{P}_{mk} \log \hat{P}_{mk}$$

#### **Benefícios/Malefícios:**

-São altamente interpretáveis (conjunto de condições);

-Usa variáveis categóricas sem precisar de tratamento ou transformar em dummies;

-Processamento ágil;

-Por sua simplicidade, não consegue atingir grande performance.

#### **CART** (Classification and Rregression Trees):

a ideia para crescer uma árvore é escolher a quebra dentre todas as possíveis quebras, em todos os nós afim de que o nó filho resulte no mais puro possível. Esse algoritmo é univariável ou seja, só olha uma variável explicativa para fazer a quebra e não a combinação de algumas.

Se X é nominal categórico de I categorias então existem $2^{i-1} - 1$ possíveis quebras. Se X é ordinal categórico ou continuo com K diferentes valores então existem K-1 diferentes quebras de X.

 1.  Ache cada a melhor quebra de cada preditor;
 2.  Para cada variável contínua ou ordinal ordene seus valores de forma crescente. Depois varie em cada valor existente testando a métrica de quebra possível;
Para cada variável categórica examina cada sub conjunto de categorias testando a métrica de quebra possível;
 3.  Ao longe de todas as variáveis e todas as quebras possíveis selecione a melhor e realize a quebra;
 4. Realize a melhor quebra até que a regra de parada seja atingida.
Para Y categóricas existem 3 possíveis quebras Gini Criterion/Twoing Criterion/Order Twoing Criterion.
Já para Y continuo usa-se o critério de LSD Least Squares Deviation.

Critérios de parada:

- Se um nó chegou a 100% de pureza;

- Se a profundidade chegou ao máximo definido;

- Se o tamanho do nó chegou ao tamanho mínimo definido;

- Se a quebra resulta em um nó filho com o tamanho menor que o definido para um nó ou folha;

- Se na melhor quebra obtida o acréscimo na métrica de quebra não é maior que o definido pelo usuário para haver a quebra.

#### **Missing values:**

Se uma variável dependente de uma amostra é missing, esse valor será ignorado. Se todas as variáveis explicativas forem missing, esse caso será ignorado. Se o peso da amostra for missing, zero ou negativo, esse caso será ignorado. Se a frequência do peso for missing, zero ou negativa, esse caso será ignorado.

O método de substituição de quebra é usado para lidar com variáveis explicativas missing. Se existe uma quebra onde X < s, porém X é missing, então testa-se a próxima melhor variável da amostra para quebra. Se todas forem missing então usa-se maioria de votos para seleciona a amostra.


#### Pros & cons

| Algorithm               | Best at                                                                     | Pros                                                                                                                                                                     | Cons                                                                                              | 
|-------------------------|-----------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------| 
| Arvores de Decisao      | classificacao de estrelas / galaxias                                        | rapido                                                                                                                                                                   | arvores complexas são dificeis de interpretar                                                     | 
|                         | diagnostico medico                                                          | robusta para "noise" e valores missing                                                                                                                                   | eé possivel duplicar uma sub-arvore dentro de uma mesma estrutura de arvore                       | 
|                         | analise de credito                                                          | eé bastante acurada (precisa)                                                                                                                                            | pode overfitar facilmente                                                                         | 
|                         |                                                                             | eé um modelo considerado "non-parametric" isso eé, ele não assume nenhuma caracteristica dos dados (exemplo: que seja distribuicao normal, que tenha linearidade, etc..) |                                                                                                   | 
|                         |                                                                             | no distribution requirement                                                                                                                                              |                                                                                                   | 
|                         |                                                                             | eé um metodo heiristico (isso eé, ele aprende na pratica consigo mesmo)                                                                                                  |                                                                                                   | 
|                         |                                                                             | não sofre com multicolineraidade (parametros correlacionados)                                                                                                            |                                                                                                   | 


### **Classificação Linear**

Até podemos usar a regressão linear para modelar uma classificação ao invés de uma logística, porém apenas para casos binários, pois para 3 ou mais classes fica difícil predizer pois a regressão linear leva esses valores como ordinais. Além de os valores variarem além do intervalo de `[0,1]`, o que dá uma interpretação mais complicada.

No modelo de regressão logística precisamos de uma função de transformação que mantenha os valores de `[0,1]`, para isso existem diversas funções, aqui usaremos a logística $p(x) = \frac{ _{e} \beta_{0} + \beta_{1} X }{1 + _{e} \beta_{0} + \beta_{1} X }$ para fitar o modelo usamos um método chamado _maximum likelihood._ Com um pouco de manipulação pode-se chegar a $\frac{p(x)}{1 - p(x)} = _{e} \beta_{0} + \beta_{1} X$ que é denominado “odds” ou chance, que pode variar de 0 a $\infty$ . Aqui temos que $\log(\frac{p(x)}{1-p(x)}) = \beta_{0} + \beta_{1} X$ é o log-odds, como na regressão linear o coeficiente $\beta$ é o quanto variar o X em uma unidade implica na variação de y, aqui na logística o $\beta$ é o quanto variar o X em um unidade implica na variação no log-odds, ou a multiplicação no odds por $e^{\beta}$.

Maximum Likelihood procura dar maior valor de $p(x)$ para valores que são o evento procurado, e um menor $p(x)$ para o evento não procurado, fazendo assim: $\ell(\beta_0, \beta_1) = \Pi_{i:y_1} = 1 p(x_i) \Pi_{i^{'}:y^{'} = 0} (1 - p(x))$.O Intercepto na regressão logística não costuma ser de grande interesse, ele é usado para ajustar a média das probabilidades fitadas à proporção de uns

#### **Linear Discriminant Analysis**

Linear Discriminant Analysis (LDA) is most commonly used as dimensionality reduction technique in the pre-processing step for pattern-classification and machine learning applications. The goal is to project a dataset onto a lower-dimensional space with good class-separability in order avoid overfitting (“curse of dimensionality”) and also reduce computational costs.

Ronald A. Fisher formulated the Linear Discriminant in 1936 (The Use of Multiple Measurements in Taxonomic Problems), and it also has some practical uses as classifier. The original Linear discriminant was described for a 2-class problem, and it was then later generalized as “multi-class Linear Discriminant Analysis” or “Multiple Discriminant Analysis” by C. R. Rao in 1948 (The utilization of multiple measurements in problems of biological classification)

The general LDA approach is very similar to a Principal Component Analysis (for more information about the PCA, see the previous article Implementing a Principal Component Analysis (PCA) in Python step by step), but in addition to finding the component axes that maximize the variance of our data (PCA), we are additionally interested in the axes that maximize the separation between multiple classes (LDA).

So, in a nutshell, often the goal of an LDA is to project a feature space (a dataset n-dimensional samples) onto a smaller subspace k (where k≤n−1) while maintaining the class-discriminatory information. 
In general, dimensionality reduction does not only help reducing computational costs for a given classification task, but it can also be helpful to avoid overfitting by minimizing the error in parameter estimation (“curse of dimensionality”).

#### Summarizing the LDA approach in 5 steps

Listed below are the 5 general steps for performing a linear discriminant analysis; we will explore them in more detail in the following sections.

1.  Compute the  $d$ -dimensional mean vectors for the different classes from the dataset.
2.  Compute the scatter matrices (in-between-class and within-class scatter matrix).
3.  Compute the eigenvectors $(e_1,e_2,...,e_d)$ and corresponding eigenvalues $(λ_1,λ_2,...,λ_d)$ for the scatter matrices.
4.  Sort the eigenvectors by decreasing eigenvalues and choose  kk  eigenvectors with the largest eigenvalues to form a  $d×k$  dimensional matrix  $W$  (where every column represents an eigenvector).
5.  Use this  $d×k$  eigenvector matrix to transform the samples onto the new subspace. This can be summarized by the matrix multiplication:  $Y=X×W$  (where  $x$  is a  $n×d$ - dimensional matrix representing the  $n$  samples, and  $y$  are the transformed  $n×k$ - dimensional samples in the new subspace).

----


![dummy](https://www.bytefish.de/static/images/blog/pca_lda_with_gnu_octave/thumbs/principal.jpg)

[![dummy](https://www.bytefish.de/static/images/blog/pca_lda_with_gnu_octave/thumbs/pc2.jpg)](https://www.bytefish.de/static/images/blog/pca_lda_with_gnu_octave/pc2.png)

--------

Para classificação multiclass podemos utilizar um método que lida melhor com várias classes. Para esse caso nos modelamos a distribuição do preditores X separadamente em cada uma das classes e usa o teorema de Bayes para tornar isso em probabilidades. Quando essas distribuições são normais esse modelo assimila-se muito a regressão logística. Quando temos classes bem separadas a regressão logística tende a ser instável. Se temos poucas amostras e as distribuições são normais, mais uma vez o LDA é mais estável.

-Usando o teorema de Bayes: vamos assumi que $\pi_k$ representa a probabilidade a priori de que uma amostra escolhida ao acaso pertença a classe K. Assumamos também que $f_k (x) = Pr(X = x | Y = k)$ é a função densidade de um ponto aleatório que pertença a K. Ou seja $f_k(x)$ terá um valor próximo a um caso X = x para K e pequeno caso contrário. Então por Bayes podemos concluir $Pr( X = x | Y = k ) = \frac{\pi_k f_k (x)}{\sum_{i=1}^{K} \pi_{\int} f_{\int} (k)}$ onde podemos resumir $Pr(X = x | Y = k) = P_k(x)$ sendo a probabilidade a posteriori, ou seja , a probabilidade de um ponto pertencer a classe K dado o valor do preditor X.

Agora precisamos definir $f_k(x)$, e para tal devemos fazer algumas suposições, tal como consideramos que essa função de densidade é uma gaussiana, para isso temos:

$$f_k(x) = \frac{1}{ \sqrt{2\pi\sigma_k} } e^{\big ( - \frac{1}{2\sigma^2_k} (x - \mu_k) \big )}$$

Onde `$$` e `$$` são a média e a variância respectivamente do preditor X de dada classe K.

Assim podemos concluir: `$$`

Aplicando log para a função acima temos a função discriminante:

`$$`, e tem no nome linear pois essa é uma função linear em X. Estima-se os valores da média variância e quando não se tem o `$$` pode-se estima-lo de acordo com a proporção de cada classe da variável resposta, o valor do discriminante é calculado para cada classe, e o valor mais alto será associado a classe.

Para o caso de mais de um preditor temos que as variáveis tem distribuição normal entre elas e uma matriz de covariância. A distribuição multi-gaussiana:

`$$`

E a função descriminante:

`$$`

#### **Quadratic Discriminant Analysis**

Comporta-se da mesma forma que o LDA, porém agora considera que cada classe possui sua própria matriz de covariância, o que é computacionalmente mais caro (vai de p*(p+1)/2 para isso vezes k), porém quando essa premissa não é mantida é mais indicado usar o QDA. O QDA acaba sendo mais flexível porem acaba tendo maior variância e menor bias.

#### Pros & cons - falta preencher

| Algorithm               | Best at                                                                     | Pros                                                                                                                                                                     | Cons                                                                                              | 
|-------------------------|-----------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------| 


### **Support Vector Machine**

#### **Hiperplano**:

É um plano em p-1 dimensões $\beta_0 + \beta_0X_0 + \beta_2X_2 + ... + \beta_nX_n$, onde quando essa equação se iguala a zero podemos considerar que o ponto pertence ao hiperplano. Quando temos essa equação >0 podemos dizer que o ponto se encontra a cima do hiperplano, e quando é <0 podemos dizer que está abaixo. Ou seja, estamos separando o hiperespaço em dois sub hiperespaços de acordo com nosso hiperplano. Um pode mostrar que conseguimos saber em qual parte do espaço está apenas pelo sinal a direita da inequação.

#### **Classificador de margem máxima**:

As margens são espaços do hiperespaço onde é possível separar linearmente as classes com um hiperplano de forma que existem infinitas possibilidades de movimentação desse hiperplano sem encostar em nenhum ponto. Elas são escolhidas a partir do mínimo ponto mais distante de separação das classes. Dessa forma o hiperplano separador é o plano intermediário das máximas margens. Observa-se que os pontos separáveis mais próximos são os únicos que influenciam as margens, e por isso são conhecidos como vetores de suporte.

#### **Construção da máxima margem**:

maximizar M sujeito a ${ \sum_{j=1}^p\beta_j^2 = 1, y_i(\beta_0 + \beta_1X_1 + ... + \beta_pX_ip) \geq M } \quad \forall \quad {i = 1, ..., n}$ .

A última etapa da otimização garante que todos os pontos estejam na classe correta, e no mínimo a M distância do hiperplano, que seria a distância até as margens.

*O tamanho das margens pode ser considerado o nível de confiança do modelo. E quando pequenas variações mudam drasticamente o hiperplano, provavelmente teremos overfitting.

Para casos onde os dados não são perfeitamente linearmente separáveis é chamado de classificador por vetores de suporte, ou soft margem classificador. Nesse caso permitimos que alguns pontos estejam do lado errado da margem ou até do hiperplano, visando robustez para novos dados, e um erro geral menor. Para esse caso devemos maximizar M sujeito a

$\sum_{j=1}^p\beta_j^2 = 1, y_i(\beta_0 + \beta_1X_1 + ... + \beta_pX_ip) \geq M (1 - \in_{i} \geq 0, \sum_{i=n}^n \in_i \leq C$, onde C é um hiperparâmetro não negativo. Os valores de erros nos dizem aonde estão esses erros em relação ao hiperplano, se for igual a 0 então a ith observação está no lado correto, se o erro é maior que zero então ela está do lado errado da margem, e se for maior que um está do lado errado do hiperplano. O parâmetro C decide quantas amostras podem estar do lado errado do hiperplano, valores altos significam que ficamos mais tolerantes a errar a classificação e quando C igual a 0 temos o classificador de margem máxima. Nesse classificador apenas pontos que caem na margem ou erroneamente na margem interferem nos vetores de suporte, os que violam o hiperplano não são considerados.

Complexidade: **O**(max(n,d) min (n,d)^2), onde n é o número de pontos e d o número de dimensões.

#### Pros & cons

| Algorithm               | Best at                                                                     | Pros                                                                                                                                                                     | Cons                                                                                              | 
|-------------------------|-----------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------| 
| Support Vector Machines | reconhecimento de caracteres                                                | criacao automatica de features não lineares                                                                                                                              | dificil de interpretar quando aplicado kernels não lineares                                       | 
|                         | reconheciento de imagens                                                    | consege resolver complexas funcoes nao lineares                                                                                                                          | depois de 10 000 exemplos ele sofre de morosidade por ser muito custoso computacionalmente        | 
|                         | classificacao de textos                                                     |                                                                                                                                                                          |                                                                                                   | 


### **KNN** - K-nearest neighbors

KNN é um método bem simplista com complexidade de $O(n^2)$. Ele funciona classificando um ponto ou regredindo para seus $k$ vizinhos mais próximos. Para uma classificação pode ser uma votação e para regressão uma média. Além da complexidade computacional, para bases de treino muito extensas torna-se pesado carregar a base toda em memória para calcular todas as distâncias. Algumas abordagens para isso foram levantadas como o CNN (Condensed Nearest Neighbours), que retira dados com um padrão muito próximo, e até o Reduced Nearest Neighbours que retira pontos que não alteram o erro do modelo.

Normalmente é usada a distância euclidiana para calcular as distâncias, ou em caso de variáveis binárias podemos usar a Manhattan. E por se tratar de distância devemos normalizar os dados para que um dado com grande range não influencie mais que os outros. Além de termos que tratar missing, por não existir distância até ele.

Podemos usar diversas distâncias como: euclidiana, Manhattan, Minkowski, Mahalanobis , Jaccard, ShivChev e outras.

Para classificação a probabilidade de determinada classe é a probabilidade frequentista dos vizinhos, então se temos 3 vizinhos e dois votam na classe Y1 então a probabilidade de ser Y1 é 2/3. Caso haja empate ou é escolhido aleatoriamente a classe ou é adicionado mais um vizinho para tirar o empate. Depende do algoritmo.

#### Pros & cons

| Algorithm               | Best at                                                                     | Pros                                                                                                                                                                     | Cons                                                                                              | 
|-------------------------|-----------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------| 
| K-nearest Neighbors     | visao computacional                                                         | treino rapido e preguicoso (basicamente ele armazena a base)                                                                                                             | predicao lenta e morosa (porque sempre roda a base de treino inteira)                             | 
|                         | tageamento de multilabel                                                    | Pode facilmente lidar com problemas complexos de multiclasse                                                                                                             | pode falhar ao prever corretamente, devido maldicao da dimensionalidade (curse of dimencionality) | 
|                         | sistemas de recomendacao                                                    |                                                                                                                                                                          |                                                                                                   | 
|                         | probelmas de spll checking                                                  |                                                                                                                                                                          |                                                                                                   | 


  

## Ensambles

### **Random Forest**

Random forest possui uma sutil vantagem sobre bagging divido ao fato de que o mesmo sorteia um percentual das variáveis explicativas para realizar cada árvore. Isso garante que as árvores não sejam tão correlacionadas entre si, pois caso exista um preditor muito forte em uma única arvore o mesmo teria maior poder de predição sobre outros preditores, porem em um conjunto de árvores onde ele pode não ser usado outros preditores tomam espaço.

*Usa-se o número de preditores por arvore pequeno quando se tem variáveis explicativas correlacionadas.

#### Pros & cons

| Algorithm               | Best at                                                                     | Pros                                                                                                                                                                     | Cons                                                                                              | 
|-------------------------|-----------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------| 
| Random Forest           | apto para qualquer tipo de problema de machine learning                     | consegue trabalhar paralelizado                                                                                                                                          | dificil de interpretar                                                                            | 
|                         | Bioinformatics                                                              | raramente overfita (It seldom overfits)                                                                                                                                  | eé mais fraco para prever valores de calda da distribuicao                                        | 
|                         |                                                                             | lida automaticamente com missing value                                                                                                                                   | em problemas de multiclass ele enviesa em direcao as classes mais frequentes                      | 
|                         |                                                                             | Não precisa transformar nenhuma variavel                                                                                                                                 |                                                                                                   | 
|                         |                                                                             | Não preicsa ficar mexendo nos parametros do modelo                                                                                                                       |                                                                                                   | 
|                         |                                                                             | Pode ser usado facilmente e vai ter excelentes resultados                                                                                                                |                                                                                                   | 


### **Boosting**

Boosting é um algoritmo sequencial, ele usa informações do modelo anterior para fazer o próximo. Ele não usa o bagging (amostras bootstrap), ao invés disso cada modelo é aplicado em uma versão modificada do dataset.

Funcionalidade: Aplica-se um modelo fraco, por exemplo uma árvore com profundidade máxima de 3, após isso escolhe um parâmetro para ponderar esse modelo. Com os resíduos desse modelo fraco aplica-se um outro modelo fraco onde a resposta do mesmo é o resíduo do outro e se atribui um peso a esse novo modelo, e assim sucessivamente até algum critério de parada. O parâmetro $\lambda$ que seria o learning rate ou shrinkage ajusta o quanto cada novo modelo irá agregar ao modelo final, ao ponto de que se esse parâmetro for muito pequeno pode-se nunca convergir, e em casos muito altos pode até divergir o modelo.

*Boosting pode dar overfitting se o numero de estimadores for muito alto, devido a não realizar bagging o modelo pode aprender muito com a base a ponto de ficar extremamente overfittado na base de treino.

*Existe um trade off entre número de estimadores e o learning rate, se o learning rate for muito pequeno é mais adequado termos vários estimadores.

*Boosting usa todas as linhas e colunas do seu dataset

*Visa primeiro reduzir o bias e depois a variância

#### **AdaBoosting:**

transforma preditores fracos em um forte modificando em cada iteração o peso das predições anteriores e focando em cada iteração em acertar as que foram erradas na iteração anterior, quanto melhor a performance de cada preditor fraco, maior o peso $\alpha$ de cada preditor.

##### Pros & cons

| Algorithm               | Best at                                                                     | Pros                                                                                                                                                                     | Cons                                                                                              | 
|-------------------------|-----------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------| 
| Adaboost                | detecao de faces                                                            | automaticamente lida com missing values                                                                                                                                  | sensível a noisy data e outliers                                                                  | 
|                         |                                                                             | não precisa transformar nenhuma variavel                                                                                                                                 | nunca eé o melhor para prever classes                                                             | 
|                         |                                                                             | não overfita facilmente                                                                                                                                                  |                                                                                                   | 
|                         |                                                                             | tem que mexer em alguns parametros                                                                                                                                       |                                                                                                   | 
|                         |                                                                             | It can leverage many different weak-learners                                                                                                                             |                                                                                                   | 


#### **GradientBoosting:**

transforma preditores fracos em fortes calculando, em cada iteração, os pseudo resíduos do modelo através do gradiente descendente que permite facilitar o uso de funções custo difíceis ou caras de se calcular a cada iteração. O gradiente descendente caminha de acordo com o learning rate, o que regular o quanto cada preditor fraco deve influenciar, ou ajudar a caminhar para o mínimo global.

##### Pros & cons

| Algorithm               | Best at                                                                     | Pros                                                                                                                                                                     | Cons                                                                                              | 
|-------------------------|-----------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------| 
| Gradient Boosting       | Apto para qualquer tipo de problema de machine learning                     | Consegue aproximar a maioria dos problemas não lineares                                                                                                                  | Se executar muitas vezes ele overfita facilmente                                                  | 
|                         | mecanismos de busca (resolvendo problemas de aprendizagem para rankeamento) | O melhor para prever classes                                                                                                                                             | sensível a noisy data e outliers                                                                  | 
|                         |                                                                             | automaticamente lida com missing values                                                                                                                                  | Não funciona legal sem parameter tunning                                                          | 
|                         |                                                                             | Não precisa transformar nenhuma variavel                                                                                                                                 |                                                                                                   | 


## Clusterizacoes

### **KMEANS**

-Kmeans++: inicia os pontos de forma que o segundo cluster é escolhido aleatoriamente, porém com probabilidade de o segundo plano estar próximo de $||Cn - X ||^2$. Ou seja, a probabilidade de um próximo ponto virar centroide é a distancia dele para o centroide anterior ao quadrado. Na segunda iteração a probabilidade de um ponto ser inicializado como centroide é calculada para o centroide mais próximo ou seja:

Sejam nossos dados: `[0, 1, 2, 3, 4]`. Escolhemos aleatoriamente que C1 é o ponto 0. A probabilidade que o próximo centroide seja C2 seja X é proporcional a $||Cn - X ||^2$.Então, $P(c2 = 1) = 1a, P(c2 = 2) = 4a, P(c2 = 3) = 9a, P(c2 = 4) = 16$ a, onde a = $\frac{1}{1+4+9+16}$.

Suponha agora que $c2=4$. Então, $P(c3 = 1) = 1a, P(c3 = 2) = 4a, P(c3 = 3) = 1a$, onde a = $\frac{1}{1+4+1}$.

#### Pros & cons

| Algorithm               | Best at                                                                     | Pros                                                                                                                                                                     | Cons                                                                                              | 
|-------------------------|-----------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------| 
| K-means                 | segmentacao                                                                 | rapido para encontrar clusters                                                                                                                                           | sofre com multicolinearidade                                                                      | 
|                         |                                                                             | consegue detectar outliers em um hiper-espaco multidimensional                                                                                                           | clusters são sempre esfericos, não consegue detectar outro tipo de formato                        | 
|                         |                                                                             |                                                                                                                                                                          | solucoes instaveis, depende muito da sua inicializacao                                            | 


### **Hierárquico**

Algoritmos hierárquicos tendem a crescer a distancia inter cluster ou a soma dos quadrados das distancias do cluster.

#### Dissimilaridade entre exemplos:

Existem dois tipos, o **aglomerativo** e **divisivo**.

O metodo **Agglomerative Hierarchical clustering** permite os clusters serem lidos de baixo para cima (bottom-up) e segue de forma que o programa sempre vai ler o sub componente e depois o parente masi proximo. 

Por outro lado o **divisivo** usa um approach top-bottom, de forma que o parente (mae) eé visitado antes do no filho.

Isso eé usado para criar a **dissimiliradidade entre os exemplos**. Agora abaixo vera como fazer isso para os clusters ja definidos.

#### Dissimilaridade de clusters:

##### Ward Linkage: 

Distância entre centroides de clusters. O Ward tende a crescer o SS o mínimo possível. Tende a criar clusters igualmente distribuídos, ou seja, muitas vezes com forma arredondada.

##### Single Linkage: 

usa a menor distância entre os clusters. Tende a juntar todo mundo que tiver um único ponto próximo um do outro. Se preocupa mais com separar os dados do que a densidade dos clusters ou balanceamento.

##### Complete Linkage: 

usa a maior distancia entre os clusters, tende a ter cluster bem densos e poucos clusters.

##### Average Linkage: 

usa a distancia de todos os pontos de um cluster c1 para outro cluster c2. Acaba se comportando de forma parecida com o Ward.

#### Pros & cons

| Algorithm               | Best at                                                                     | Pros                                                                                                                                                                     | Cons                                                                                              | 
|-------------------------|-----------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------| 
| Hierarquical clustering | clustering | pode fitar clusters nao esfericos | nao lida bem com bases grandes |                                                  |                                                                                            | 
|                                   |                                                   | clusters bem definidos e representados pelo dendograma | não lida com missing data                                                                  | 
|                                   |                                                   | facil de entender facil de explicas                    | o algoritmo tambem fica pesado para computar distanias de muitos tipos de dados diferentes | 
|                                   |                                                   |                                                        | raramente eé a melhor solucao                                                              | 



### **Fuzzy C Means**

O fuzzy c means é um algoritmo de clusterização que possibilita indicar mais de um cluster por ponto do espaço, devida a sua habilidade de calcular a probabilidade de um determinado ponto pertencer a um cluster, ele escolhe um thershold para classificar como de dois clusters. Funciona como o Kmeans, mas calcula a probabilidade de estar em cada cluster, e qual tiver mais probabilidade é indicado para recalcular os centroides.

#### Pros & cons - falta preencher

| Algorithm               | Best at                                                                     | Pros                                                                                                                                                                     | Cons                                                                                              | 
|-------------------------|-----------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------| 


# Data Prep

## Reducao de dimencionalidade

### **PCA - Principal Components Analysis**

Principal Component Analysis, or PCA, is a statistical method used to reduce the number of variables in a dataset. It does so by lumping highly correlated variables together. Naturally, this comes at the expense of accuracy. However, if you have 50 variables and realize that 40 of them are highly correlated, you will gladly trade a little accuracy for simplicity.

#### **How does PCA work?**

Say you have a dataset of two variables, and want to simplify it. You will probably not see a pressing need to reduce such an already succinct dataset, but let us use this example for the sake of simplicity.

The two variables are:

1.  Dow Jones Industrial Average, or DJIA, a stock market index that constitutes 30 of America’s biggest companies, such as Hewlett Packard and Boeing.
2.  S&P 500 index, a similar aggregate of 500 stocks of large American-listed companies. It contains many of the companies that the DJIA comprises.

Not surprisingly, the DJIA and FTSE are highly correlated. Just look at how their daily readings move together. To be precise, the below is a plot of their daily % changes.

[![DJIA vs S&P](https://coolstatsblog.files.wordpress.com/2015/03/djia-vs-snp2.png?w=750)](https://coolstatsblog.files.wordpress.com/2015/03/djia-vs-snp2.png)

DJIA vs S&P

The above points are represented in 2 axes: X and Y. In theory, PCA will allow us to represent the data along one axis. This axis will be called the  _principal component_, and is represented by the black line.

[![DJIA vs S&P with principle component line](https://coolstatsblog.files.wordpress.com/2015/03/djia-vs-snp-with-line.png?w=750)](https://coolstatsblog.files.wordpress.com/2015/03/djia-vs-snp-with-line.png)

DJIA vs S&P with principal component line

>Note 1: In reality, you will not use PCA to transform two-dimensional data into one-dimension. Rather, you will simplify data of higher dimensions into lower dimensions.

>Note 2: Reducing the data to a single dimension/axis will reduce accuracy somewhat. This is because the data is not neatly hugging the axis. Rather, it varies about the axis. But this is the trade-off we are making with PCA, and perhaps we were never concerned about needle-point accuracy. The above linear model would do a fine job of predicting the movement of a stock and making you a decent profit, so you wouldn’t complain too much.

Do you remember we graphically identified the principal component for our data?

[![DJIA vs S&P with principle component line](https://coolstatsblog.files.wordpress.com/2015/03/djia-vs-snp-with-line.png?w=750)](https://coolstatsblog.files.wordpress.com/2015/03/djia-vs-snp-with-line.png)

DJIA vs S&P with principal component line

The main principal component, depicted by the black line, will become our new X-axis. Naturally, a line perpendicular to the black line will be our new Y axis, the other principal component. The below lines are perpendicular; don’t let the aspect ratio fool you.

[![new axes](https://coolstatsblog.files.wordpress.com/2015/03/new-axes.png?w=750)](https://coolstatsblog.files.wordpress.com/2015/03/new-axes.png)

If we used the black lines as our x and y axes, our data would look simpler

Thus, we are going to rotate our data to fit these new axes. But what will the coordinates of the rotated data be?

To convert the data into the new axes, we will multiply the original DJIA, S&P data by  _eigenvectors_, which indicate the direction of the new axes (principal components).

But first, we need to deduce the eigenvectors (there are two – one per axis). Each eigenvector will correspond to an  _eigenvalue_, whose magnitude indicates how much of the data’s variability is explained by its eigenvector.

As per the definition of eigenvalue and eigenvector:

$\begin{bmatrix}  Covariance &matrix  \end{bmatrix}  \cdot  \begin{bmatrix}  Eigenvector  \end{bmatrix}  =  \begin{bmatrix}  eigenvalue  \end{bmatrix}  \cdot  \begin{bmatrix}  Eigenvector  \end{bmatrix}$ 

We know the covariance matrix from step 2. Solving the above equation by some clever math will yield the below eigenvalues (e) and eigenvectors (E):

$e_{1}=1.644$

$E_{1}= \begin{bmatrix} 0.6819 \\ -0.7314 \end{bmatrix}$

$e_{2}=0.0376$

$E_{1}= \begin{bmatrix} -0.7314 \\ 0.6819 \end{bmatrix}$

Step 4 – Re-orient data:

Since the eigenvectors indicates the direction of the principal components (new axes), we will multiply the original data by the eigenvectors to re-orient our data onto the new axes. This re-oriented data is called a  _score._

$Sc = \begin{bmatrix}  Orig. \: data  \end{bmatrix}  \cdot  \begin{bmatrix}  Eigvectors  \end{bmatrix}  =  \begin{bmatrix}  DJIA_{1} & S\&P_{1}\\  DJIA_{2} & S\&P_{2}\\  ... & ...\\  DJIA_{150} & S\&P_{150}  \end{bmatrix}  \cdot  \begin{bmatrix}  0.6819 & 0.7314\\  -0.7314 & 0.6819  \end{bmatrix}$

Step 5 – Plot re-oriented data:

We can now plot the rotated data, or scores.

[![Original data, re-oriented to fit new axes](https://coolstatsblog.files.wordpress.com/2015/03/reoriented-data.png?w=750)](https://coolstatsblog.files.wordpress.com/2015/03/reoriented-data.png)

Original data, re-oriented to fit new axes

Step 6 – Bi-plot:

A PCA would not be complete without a bi-plot. This is basically the plot above, except the axes are standardized on the same scale, and arrows are added to depict the original variables, lest we forget.

-   Axes: In this bi-plot, the X and Y axes are the principal components.
-   Points: These are the DJIA and S&P points, re-oriented to the new axes.
-   Arrows: The arrows point in the direction of increasing values for each original variable. For example, points in the top right quadrant will have higher DJIA readings than points in the bottom left quadrant. The closeness of the arrows means that the two variables are highly correlated.

[![Bi-plot](https://coolstatsblog.files.wordpress.com/2015/03/biplot.png?w=750)](https://coolstatsblog.files.wordpress.com/2015/03/biplot.png)

Bi-plot

Data from St Louis Federal Reserve; PCA performed on R, with help of ggplot2 package for graphs.

----

PCA tenta achar uma representação em poucas dimensões dos dados que contenha o máximo possível de sua variância. Cada uma das dimensões encontradas pelo PCA é uma combinação linear das p dimensões. O primeiro componente é a combinação normalizada de $Z_1 = \phi_{11} X_1 + \phi_{21} X_2 + ... + \phi_{p1}X_p$, que possui a maior variância. Por normalização dizemos que $\Sigma_j^p \phi_{j1}^2 = 1$ Nos referimos a $\phi$ como os pesos do principal componente. Junto eles formam o vetor de pesos do principal componente$\phi_1 = (\phi_{11} + \phi_{21} + ... + \phi_p1)^T$ Para calcular os principais componentes, como o que importa é a variância, devemos fazer com que nossos preditores tenham média zero. Ou seja temos que: $maximizar \bigg\{ \frac{1}{n} \Sigma_i=1^n (\Sigma_{j=1}^p \phi_{j1}x_{ij}) \bigg\}$. Ou seja, maximizar a variância sobre as n amostras ao longo de $\Sigma_{j=1}^p \phi_{j1}^2$. O primeiro componente simboliza a direção no espaço na qual os dados mais variam. Para o segundo componente usamos a combinação linear de $X_1 ... X_n$ que tenha a máxima variância, porém que não seja correlacionado com o primeiro componente, ou seja, tem que ser ortogonal ao PC1.

Os principais componentes promovem superfícies lineares em baixa dimensão que são próximas das observações.

Em PCA os dados precisam ser individualmente normalizados ou caso contrário o primeiro componente pode ficar enviesado pela variável com maior variância individual.

Para saber o quanto da variância cada componente contempla devemos primeiro considerar para os dados normalizados a variância total como $\Sigma_{j=2}^p Var(X_j) = \Sigma_{j=1}^p \frac{1}{n} \Sigma_{i=1}^n x_{ij}^2$ e a variância explicada por cada componente como $\frac{1}{n} \Sigma_{i=1}^n z_{im}^2 = \frac{1}{n} \Sigma_{i=1}^n \bigg( \Sigma_{j=1}^p \phi_{jm} x_{ij} \bigg)^2$.x

#### Pros & cons

| Algorithm               | Best at                                                                     | Pros                                                                                                                                                                     | Cons                                                                                              | 
|-------------------------|-----------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------| 
| PCA                     | remover colinearidades                                                      | pode reduzir a colinearidade                                                                                                                                             | implica em forte suposicoes lineares (componentes são somas ponderadas das features)              | 
|                         | reduz a dimensionalidade do dataset                                         |                                                                                                                                                                          |                                                                                                   | 


### PCA vs LDA (Principal Component Analysis vs. Linear Discriminant Analysis)

Both Linear Discriminant Analysis (LDA) and Principal Component Analysis (PCA) are linear transformation techniques that are commonly used for dimensionality reduction. PCA can be described as an “unsupervised” algorithm, since it “ignores” class labels and its goal is to find the directions (the so-called principal components) that maximize the variance in a dataset. In contrast to PCA, LDA is “supervised” and computes the directions (“linear discriminants”) that will represent the axes that that maximize the separation between multiple classes.

Although it might sound intuitive that LDA is superior to PCA for a multi-class classification task where the class labels are known, this might not always the case.  
For example, comparisons between classification accuracies for image recognition after using PCA or LDA show that PCA tends to outperform LDA if the number of samples per class is relatively small ([PCA vs. LDA](http://ieeexplore.ieee.org/xpl/articleDetails.jsp?arnumber=908974), A.M. Martinez et al., 2001). In practice, it is also not uncommon to use both LDA and PCA in combination: E.g., PCA for dimensionality reduction followed by an LDA.

![](https://sebastianraschka.com/images/blog/2014/linear-discriminant-analysis/lda_1.png)

![png](https://sebastianraschka.com/images/blog/2014/linear-discriminant-analysis/2014-08-03-linear_discriminant_analysis_133_0.png)

![png](https://sebastianraschka.com/images/blog/2014/linear-discriminant-analysis/2014-08-03-linear_discriminant_analysis_133_1.png)

# Reamostragem / Validacao

## **Validação Cruzada**

### Hold Out:

separa-se a base de modelagem em treino e teste, porém isso resulta em alto bias, pois o modelo aprende tudo muito específico na base de treino e erra na base de teste. Essa técnica tende a superestimar a métrica de teste.

### Leave One Out:

é um caso específico de K-Fold com K = n. Computacionalmente caro de ser processar. Em relação ao Hold-out tem um bias bem menor, porém tem uma variância maior, pois pode-se provar que realizar a média de valores correlacionados resulta em alta variância.

### K-Fold:

realiza-se K divisões na base onde usa-se K-n folds para treinar o modelo e K para validar. Esse método mostra-se empiricamente melhor que ambos os anteriores para uma base onde não se tem uma ampla distribuição no teste e treino, como por exemplo treinar o modelo na população e testa-lo também na população, aí não se torna tão necessária a re-amostragem.

## **Bagging**

Bagging é o processo de re-amostragem com finalidade de reduzir a variância dos modelos para não haver overfitting, normalmente utilizado com bootstrap. É largamente utilizado com árvores de decisão devido ao baixo bias e alta variância. No caso de regressão realiza a média dos diversos valores obtidos nas amostras do bootstrap, já para classificação realiza a maioria de votos.

### **Out-of-bag Error Estimation:**

pode-se usar o bagging para validar a métrica de teste sem ser necessário uma das técnicas conhecidas de validação cruzada. Como é sabido o bootstrap pega cerca de 2/3 da população para criar sua amostra, o restante pode ser usado para validar a métrica de teste, ou seja, se obtém um treinamento na amostra gerada pelo bootstrap, e no que não for usado aplica-se o modelo e avalia por alguma métrica, dessa forma cada amostra terá um conjunto de predições, que para um modelo de regressão pode se aplicar a média e de classificação a maioria de votos. Pode se provar que se usar o Out-of-bag error estimation com B suficientemente grande o erro estimado será próximo ou igual ao LOOCV.

### **Importância das variáveis**:

pode-se chegar a esse valor por variável somando o quanto cada vez que uma variável foi usada como quebra, o quanto ela minimizou a métrica de quebra, MSE para regressão e Gini ou Entropia para classificação.

  

## Complexidades Computacionais:

### **Kmeans**:
$O(iknd)$: n pontos d dimensões k cluster e i inicializações;

### **Hierárquico**:
$O(n^2)$: n pontos

### **DBScan**:
$O(n^2)$ pode ser reduzido $O(n * \log(n))$

### **SVM**:
$O(\text{max}(n,d) \text{min}(n,d)^2)$, onde n é o número de pontos e d o número de dimensões.

### **Tree**:
$O(d * \text{log}(n))$ $$: com n pontos e d dimensões

### **Random Forest**:
$O(e * d * \text{log}(n))$: com n pontos e d dimensões e “e” estimadores se não for paralelo

### **Boosting Tree**:
$O(e * d * \text{log}(n))$: com n pontos e d dimensões e “e” estimadores.

### **KNN**:
$O(n*d+k*n)$ou$O(n*d*k)$ com n pontos e d dimensões e k vizinhos.

### **Naive Bayes**:
$O(n*d)$

### **Linear Regression**:
$O(d^2 * (n+d))$


# Avaliacao de Modelo

## Metricas
  
![](https://cdn-images-1.medium.com/max/800/1*8VM2PELQ-oeM0O3ya7BIyQ.png)

### Regressoes

#### MSE

It is perhaps the most simple and common metric for regression evaluation, but also probably the least useful. It is defined by the equation:
$$
MAE = \frac{1}{n}\sum_{j=1}^n (y_j - \hat{y_j})^2
$$
where  _yᵢ_  is the actual expected output and  _ŷᵢ_  is the model’s prediction.

MSE basically measures average squared error of our predictions. For each point, it calculates square difference between the predictions and the target and then average those values.

The higher this value, the worse the model is. It is never negative, since we’re squaring the individual prediction-wise errors before summing them, but would be zero for a perfect model .

##### **Advantage:** 

Useful if we have unexpected values that we should care about. Vey high or low value that we should pay attention.

##### **Disadvantage:** 

If we make a single very bad prediction, the squaring will make the error even worse and it may skew the metric towards overestimating the model’s badness. That is a particularly problematic behaviour if we have noisy data (that is, data that for whatever reason is not entirely reliable) — even a “perfect” model may have a high MSE in that situation, so it becomes hard to judge how well the model is performing. On the other hand, if all the errors are small, or rather, smaller than 1, than the opposite effect is felt: we may underestimate the model’s badness.

**Note that**  if we want to have a constant prediction the best one will be the  **mean value of the target values.** It can be found by setting the derivative of our total error with respect to that constant to zero, and find it from this equation.

##### pros - cons

| Algorithm               | Best at                                                                     | Pros                                                                                                                                                                     | Cons                                                                                              | 
|-------------------------|-----------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------| 
| MSE                     | mensurar error de modelos de forma simplificada                             | util quando se tem valores que nao esperamos e devemos prestar atencao (outliers)                                                                                        | a metrica costuma overestimar o quao ruim um modelo foi.                                          | 
|                         |                                                                             |                                                                                                                                                                          | nao existe uma explicacao para o numero retornado                                                 | 
|                         |                                                                             |                                                                                                                                                                          | nao tem um limite                                                                                 | 
|                         |                                                                             |                                                                                                                                                                          | dependendo da escala da base (presenca de outliers) o resultado sera distorcido                   | 
|                         |                                                                             |                                                                                                                                                                          | ela nao evalua se todas as features estao coerentes                                               | 



#### RMSE

RMSE is just the square root of MSE. The square root is introduced to make scale of the errors to be the same as the scale of targets.

$$
RMSE = \sqrt{\frac{1}{n}\sum_{j=1}^n (y_j - \hat{y_j})^2} = \sqrt{MSE}
$$

Now, it is very important to understand in what sense RMSE is similar to MSE,and what is the difference.

First, they are similar in terms of their minimizers, every minimizer of MSE is also a minimizer for RMSE and vice versa since the square root is an non-decreasing function. For example, if we have two sets of predictions, A and B, and say MSE of A is greater than MSE of B, then we can be sure that RMSE of A is greater RMSE of B.And it also works in the opposite direction.

$$
MSE(a) > MSE(b) \Longleftrightarrow RMSE(a) > RMSE(b)
$$

##### **What does it mean for us?**

It means that, if the target metric is RMSE, we still can compare our models using MSE,since MSE will order the models in the same way as RMSE. Thus we can optimize MSE instead of RMSE.

In fact, MSE is a little bit easier to work with, so everybody uses MSE instead of RMSE. Also a little bit of difference between the two for gradient-based models.

![](https://cdn-images-1.medium.com/max/1600/1*_zMjSM14AmeF0DASdzo-pw.png)

Gradient of RMSE with respect to i-th prediction

It means that travelling along MSE gradient is equivalent to traveling along RMSE gradient but with a different flowing rate and the flowing rate depends on MSE score itself.

So even though RMSE and MSE are really similar in terms of models scoring, they can be not immediately interchangeable for gradient based methods. We will probably need to adjust some parameters like the learning rate.

##### pros - cons

| Algorithm               | Best at                                                                     | Pros                                                                                                                                                                     | Cons                                                                                              | 
|-------------------------|-----------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------| 
| RMSE                    | mensurar erro de modelos de forma simplificada                              | mesma do MSE porem com a escala trazida de volta para a original                                                                                                         | a metrica costuma overestimar o quao ruim um modelo foi.                                          | 
|                         |                                                                             |                                                                                                                                                                          | nao existe uma explicacao para o numero retornado                                                 | 
|                         |                                                                             |                                                                                                                                                                          | nao tem um limite                                                                                 | 
|                         |                                                                             |                                                                                                                                                                          | dependendo da escala da base (presenca de outliers) o resultado sera distorcido                   | 
|                         |                                                                             |                                                                                                                                                                          | ela nao evalua se todas as features estao coerentes                                               | 


#### MAE

In MAE the error is calculated as an average of absolute differences between the target values and the predictions. The MAE is a linear score which means that **all the individual differences are weighted equally** in the average. For example, the difference between 10 and 0 will be twice the difference between 5 and 0. However, same is not true for RMSE. Mathematically, it is calculated using this formula:

$$
MAE = \frac{ \Sigma_{i=1}^{n} | y_1 - x_1 |}{n} = \frac{\Sigma_{i=1}^n |e_i|}{n} 
$$

$$
MAE = \frac{1}{n}\sum_{j=1}^n|y_j - \hat{y_j}|
$$

What is important about this metric is that it  **penalizes huge errors that not as that badly as MSE does.** Thus, it’s not that sensitive to outliers as mean square error.

MAE is widely used in finance, where $10 error is usually exactly two times worse than $5 error. On the other hand, MSE metric thinks that $10 error is four times worse than $5 error. MAE is easier to justify than RMSE.

Another important thing about MAE is its gradients with respect to the predictions.The gradiend is a step function and it takes -1 when Y_hat is smaller than the target and +1 when it is larger.

Now, the gradient is not defined when the prediction is perfect,because when Y_hat is equal to Y, we can not evaluate gradient. It is not defined.

![](https://cdn-images-1.medium.com/max/1600/1*KbVmb_liXV1sP0gYVk0U8g.png)

So formally, MAE is not differentiable, but in fact, how often your predictions perfectly measure the target. Even if they do, we can write a simple IF condition and returnzero when it is the case and through gradient otherwise. Also know that second derivative is zero everywhere and not defined in the point zero.

**Note that**  if we want to have a constant prediction the best one will be the  **median value of the target values.** It can be found by setting the derivative of our total error with respect to that constant to zero, and find it from this equation.

##### pros - cons

| Algorithm               | Best at                                                                     | Pros                                                                                                                                                                     | Cons                                                                                              | 
|-------------------------|-----------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------| 
| MAE                     | mensurar errors desconsiderando outliers                                    | e robusto a outliers                                                                                                                                                     | nao existe uma explicacao para o numero retornado                                                 | 
|                         | muito usado em financas                                                     | penaliza menos os erros muito grandes em relacao ao mse                                                                                                                  | nao tem um limite                                                                                 | 
|                         |                                                                             |                                                                                                                                                                          | ela nao evalua se todas as features estao coerentes                                               | 


#### MAE vs. RMSE

As the name suggests, the mean absolute error is an average of the absolute errors  ${\displaystyle |e_{i}|=|y_{i}-x_{i}|}$, where  ${\displaystyle y_{i}}$  is the prediction and  ${\displaystyle x_{i}}$  the true value. Note that alternative formulations may include relative frequencies as weight factors. The mean absolute error uses the same scale as the data being measured. This is known as a scale-dependent accuracy measure and therefore cannot be used to make comparisons between series using different scales.[[4]](https://en.wikipedia.org/wiki/Mean_absolute_error#cite_note-4)The mean absolute error is a common measure of  [forecast error](https://en.wikipedia.org/wiki/Forecast_error "Forecast error")  in  [time series analysis](https://en.wikipedia.org/wiki/Time_series_analysis "Time series analysis"),[[5]](https://en.wikipedia.org/wiki/Mean_absolute_error#cite_note-Hyndman2005-5)  where the terms "mean absolute deviation" is sometimes used in confusion with the more standard definition of  [mean absolute deviation](https://en.wikipedia.org/wiki/Mean_absolute_deviation "Mean absolute deviation"). The same confusion exists more generally.

##### **So which one should you choose and why (MAE vs RMSE)?**

Well, it is easy to understand and interpret MAE because it directly takes the average of offsets whereas RMSE penalizes the higher difference more than MAE.

Let’s understand the above statement with the two examples:

_Case 1: Actual Values =`[2,4,6,8]` , Predicted Values = `[4,6,8,10]`_
_Case 2: Actual Values = `[2,4,6,8]` , Predicted Values = `[4,6,8,12]`_

**MAE for case 1 = 2.0, RMSE for case 1 = 2.0  
MAE for case 2 = 2.5, RMSE for case 2 = 2.65**

From the above example, we can see that RMSE penalizes the last value prediction more heavily than MAE. Generally, RMSE will be higher than or equal to MAE. The only case where it equals MAE is when all the differences are equal or zero (true for case 1 where the difference between actual and predicted is 2 for all observations).

**Edit:** One important distinction between MAE & RMSE that I forgot to mention earlier is that minimizing the squared error over a set of numbers results in finding its mean, and minimizing the absolute error results in finding its median. This is the reason why MAE is robust to outliers whereas RMSE is not.  This [answer](https://www.quora.com/How-would-a-model-change-if-we-minimized-absolute-error-instead-of-squared-error-What-about-the-other-way-around) explains this concept in detail.


![](https://cdn-images-1.medium.com/max/1600/1*HmnyRcMjgfW-Bo2_NKLYqg.png)

#### R Squared (R²)

Now, what if I told you that MSE for my models predictions is 32? Should I improve my model or is it good enough?Or what if my MSE was 0.4?Actually, it’s hard to realize if our model is good or not by looking at the absolute values of MSE or RMSE.We would probably want to measure how much our model is better than the constant baseline.

The coefficient of determination, or R² (sometimes read as R-two), is another metric we may use to evaluate a model and it is closely related to MSE, but has the advantage of being  **scale-free **— it doesn’t matter if the output values are very large or very small,  **the R² is always going to be between -∞ and 1.**

When R² is negative it means that the model is worse than predicting the mean.

$$
R^2 = 1 - \frac{MSE(model)}{MSE(baseline)}
$$

The MSE of the model is computed as above, while the MSE of the baseline is defined as:
$MSE(baseline) = \frac{1}{N} \sum_{i=1}^n (y_i - \bar{y})^2$ where the  _y_  with a bar is the mean of the observed  _yᵢ._

To make it more clear, this baseline MSE can be thought of as the MSE that the  **simplest possible**  model would get. The simplest possible model would be to  _always_  predict the average of all samples. A value close to 1 indicates a model with close to zero error, and a value close to zero indicates a model very close to the baseline.

**In conclusion, R² is the ratio between how good our model is vs how good is the naive mean model.**

##### **Common Misconception:** 

Alot of articles in the web states that the range of R² lies between 0 and 1 which is not actually true. The maximum value of R² is 1 but minimum can be minus infinity.

For example, consider a really crappy model predicting highly negative value for all the observations even though y_actual is positive. In this case, R² will be less than 0. This is a highly unlikely scenario but the possibility still exists.

##### pros - cons

| Algorithm               | Best at                                                                     | Pros                                                                                                                                                                     | Cons                                                                                              | 
|-------------------------|-----------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------| 
| r squared               | evaluer o fit de um modelo de forma geral                                   | facil entender                                                                                                                                                           | [-infinite, 1] varia o range da metrica                                                           |
|                         |                                                                             | facil explicar                                                                                                                                                           | ela nao evalua se todas as features estao coerentes                                               | 
|                         |                                                                             | compara o resultado do modelo com o resultado de um modelo aleatorio                                                                                                     |                                                                                                   | 


#### **Adjusted R Squared (adjusted R²)**

R² shows how well terms (data points) fit a curve or line. Adjusted R2 also indicates how well terms fit a curve or line, but adjusts for the number of terms in a model. If you add more and more **useless**  [variables](http://www.statisticshowto.com/types-variables/) to a model, adjusted R squared will decrease. If you add more **useful** variables, adjusted R squared will increase.Adjusted R² will always be less than or equal to R².

$$
R^2_{adj} = 1 - \bigg [  \frac{(1-R^2)(n-1)}{n-k-1} \bigg ]
$$

where n is the total number of observations and k is the number of independent regressors, i.e. the number of variables in your model, excluding the constant.

##### pros - cons

| Algorithm               | Best at                                                                     | Pros                                                                                                                                                                     | Cons                                                                                              | 
|-------------------------|-----------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------| 
| adjusted r squared      | evalua as features tambem                                                   | tudo que o r squared tem                                                                                                                                                 |                                                                                                   | 
|                         |                                                                             | evalua as features que nao explicam o target                                                                                                                             |                                                                                                   | 
|                         |                                                                             | evita o overfit (por penalizar as features)                                                                                                                              |                                                                                                   | 


#### Adjusted R² vs R²

Both R² and the adjusted R² give you an idea of how many data points fall within the line of the  [regression equation](http://www.statisticshowto.com/what-is-a-regression-equation/). However, R² assumes that every single variable explains the  _variation in the_ [_dependent variable_](http://www.statisticshowto.com/dependent-variable-definition/).  **The adjusted R² tells you the percentage of variation explained by only the**[**independent variables**](http://www.statisticshowto.com/independent-variable-definition/) **that actually affect the dependent variable.**

In reality, adjusted R² will penalize you for adding independent variables (K in the equation) that do not fit the model. Why? In  [regression analysis](http://www.statisticshowto.com/probability-and-statistics/regression-analysis/), it can be tempting to add more variables to the data as you think of them. Some of those variables will be significant, but you can’t be sure that significance is just by chance. The adjusted R² will compensate for this by that penalizing you for those extra variables.

##### **Problems with** R² **that are corrected with an adjusted** R²

1.  R² increases with every predictor added to a model. As R² always increases and never decreases, it can appear to be a better fit with the more terms you add to the model. This can be completely misleading.
2.  Similarly, if your model has too many terms and too many high-order polynomials you can run into the problem of over-fitting the data. When you over-fit data, a misleadingly high R² value can lead to misleading projections.

> All the metrics that we have examined up to this point assume that each prediction provides equally precise information about the error variation. MSPE and MAPE doesn’t follow this assumption.

##### **Why should you choose Adjusted R² over R²?**

There are some problems with normal R² which are solved by Adjusted R². An adjusted R² will consider the marginal improvement added by an additional term in your model. So it will increase if you add the useful terms and it will decrease if you add less useful predictors. However, R² increases with increasing terms even though the model is not actually improving. It will be easier to understand this with an example.

![](https://cdn-images-1.medium.com/max/800/1*2dFBX0Vz3fWBnOrbrB05mg.png)

Here, Case 1 is the simple case where we have 5 observations of (x,y). In case 2, we have one more variable which is twice of variable 1 (perfectly correlated with var 1). In Case 3, we have produced a slight disturbance in var2 such that it is no longer perfectly correlated with var1.

So if we fit simple ordinary least squares (OLS) model for each case, logically we are not providing any extra or useful information to case 2 and case 3 with respect to case 1. So our metric value should not improve for these models. However, it is actually not true for R² which gives a higher value for model 2 and 3. But your adjusted R² takes care of this problem and it is actually decreasing for both cases 2 & 3. Let’s give some numbers to these variables (x,y) and look at the results obtained in Python.

![](https://cdn-images-1.medium.com/max/800/1*C-i3nKPtHl_mkfTFgX2IQg.png)

From the above table, we can see that even though we are not adding any additional information from case 1 to case 2, still R² is increasing whereas adjusted R² is showing the correct trend (penalizing model 2 for more number of variables)

#### **Adjusted R² vs RMSE**

For the previous example, we will see that RMSE is same for case 1 and case 2 similar to R². This is the case where Adjusted R² does a better job than RMSE whose scope is limited to comparing predicted values with actual values. Also, the absolute value of RMSE does not actually tell how bad a model is. It can only be used to compare across two models whereas Adjusted R² easily does that. For example, if a model has adjusted R² equal to 0.05 then it is definitely poor.

> However, if you care only about prediction accuracy then RMSE is best. It is computationally simple, easily differentiable and present as default metric for most of the models.

##### **Common Misconception:** 

I have often seen on the web that the range of R² lies between 0 and 1 which is not actually true. The maximum value of R² is 1 but minimum can be negative infinity. Consider the case where model is predicting highly negative value for all the observations even though y_actual is positive. In this case, R² will be less than 0. This will be a highly unlikely scenario but the possibility still exists.

#### MSPE - Mean Square Percentage Error 

Let’s think about the following problem. Our goal is to predict, how many laptops two shops will sell?

-   Shop 1: predicted 9, sold 10, MSE =1
-   Shop 2: predicted 999, sold 1000, MSE =1

OR even,

-   Shop 1: predicted 9, sold 10, MSE =1
-   Shop 2: predicted 900, sold 1000, MSE =10000

MSE is the same for both shops predictions, and thus according to those metrics, these off by one errors are indistinguishable. This is basically because MSE works with absolute squared errors while relative error can be more important for us.

The relative error preference can be expressed with Mean Square Percentage Error. For each object, the absolute error is divided by the target value, giving relative error.

$$
MSPE = \frac{100\%}{N} \sum_{i=1}^N 
\bigg ( \frac{ y_i - \hat{y}_i }{ y_i } \bigg ) ^2 
$$

So, MSPE can be thought as weighted versions of MSE. The weight of its sample is inversely proportional to it’s target square. It means that, the cost we pay for a fixed absolute error, depends on the target value and as the target increases, we pay less.

Since MSPE is considered as the weighted version of MSE  **the optimal constant predictions for MSPE**  it turns out to be the  **weighted mean of the target values.**

##### pros - cons

| Algorithm               | Best at                                                                     | Pros                                                                                                                                                                     | Cons                                                                                              | 
|-------------------------|-----------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------| 
| mspe (MSE percentage)   | predicting what mse predicts                                                | tudo que o mae possui                                                                                                                                                    | por usar as porcentagens ele cria um peso muito alto classes muito raras (penaliza muito)         | 
|                         |                                                                             | cria pesos no MSE original - traz o resultado para uma metrica comparavel                                                                                                |                                                                                                   | 
|                         |                                                                             |                                                                                                                                                                          |                                                                                                   | 

#### MAPE - Mean Absolute Percentage Error 

The relative error preference can also be expressed with Mean Absolute Percentage Error, MAPE.For each object, the absolute error is divided by the target value, giving relative error. MAPE can also be thought as weighted versions of MAE.

$$
MSPE = \frac{100\%}{N} \sum_{i=1}^N 
\bigg | \frac{ y_i - \hat{y}_i }{ y_i } \bigg |
$$

For MAPE, the weight of its sample is inversely proportional to it’s target. But similarly as MSPE, the cost we pay for a fixed absolute error,depends on the target value. And as the target increases, we pay less.

Since MAPE is considered as the weighted version of MAE  **the optimal constant predictions for MAPE**  it turns out to be the  **weighted median of the target values.**

Note that if an outlier had a very, very small value, MAPE would be very biased towards it, since this outlier will have the highest weight.

![](https://cdn-images-1.medium.com/max/1600/1*Xp9ZG65N6k1Ew8wmbxHkRw.png)
_Error curves_

##### pros - cons

| Algorithm               | Best at                                                                     | Pros                                                                                                                                                                     | Cons                                                                                              | 
|-------------------------|-----------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------| 
| mape (MAE percentage)   | predicting what mae predicts                                                | tudo que o mae possui                                                                                                                                                    | por usar as porcentagens ele cria um peso muito alto classes muito raras (penaliza muito)         | 
|                         |                                                                             | cria pesos no MAE original - traz o resultado para uma metrica comparavel                                                                                                |                                                                                                   | 
|                         |                                                                             |                                                                                                                                                                          |                                                                                                   | 


#### RMSLE - Root Mean Squared Logarithmic Error

It is just an RMSE calculated in logarithmic scale. In fact, to calculate it, we take a logarithm of our predictions and the target values, and compute RMSE between them. The targets are usually non-negative but can equal to 0, and the logarithm of 0 is not defined. That is why a constant is usually added to the predictions and the targets before applying the logarithmic operation. This constant can also be chosen to be different to one depending on the problem.

$$
RMSLE = \sqrt{ \frac{1}{n} \sum_{i=1}^N ( \log (y_i + 1) - \log (\hat{y}_i + 1) )^2} = \newline
$$
$$
= RMSE  ( \log (y_i + 1) - \log (\hat{y}_i + 1) ) = \newline
$$
$$
= \sqrt{MSE  ( \log (y_i + 1) - \log (\hat{y}_i + 1) )}
$$

So, this metric is usually used in the same situation as MSPE and MAPE, as it also carries about relative errors more than about absolute ones.

![](https://cdn-images-1.medium.com/max/1600/1*lNC-LP9RneojV1VZlUHXww.png)

Error Curve for RMSLE

Note  **the asymmetry of the error curves**.From the perspective of RMSLE, it is always better to predict more than the same amount less than target. Thus, we conlude that RMSLE penalizes an under-predicted estimate greater than an over-predicted estimate.

RMSLE can be calculated without root operation but the rooted version is more widely used.

Now let’s move on to the question about the best constant. (Recall the connection between RMSLE and RMSE). First, we find the best constant for RMSE in the log space which will be the weighted mean in the log space.And after it, we need to get back from log space to the usual one with an inverse transform.

##### Example

![](https://cdn-images-1.medium.com/max/1600/1*YgnMgPeQ6hjf64D-U8RmsQ.png)

Optimal Constants for Different Regression Evaluation Metrics

##### **Observations:**

-   The optimal constant for RMSLE turns out to be 9.1 which is higher than constants for both MAPE and MSPE.
-   MSE is quite biased towards the huge value from our dataset while MAE is much less biased.
-   MSPE and MAPE are biased towards smaller targets because they assign higher weight to the object with small targets.
-   RMSLE is frequently considered as better metrics than MAPE, since it is less biased towards small targets, yet works with relative errors.

### Classificacoes

#### Acuracia - **Classification Accuracy**

Classification Accuracy is what we usually mean, when we use the term accuracy. It is the ratio of number of correct predictions to the total number of input samples.

$$
Accuracy = \frac{\text{Number of Correct predictions}}{\text{Total number of predictions made}}
$$

It works well only if there are equal number of samples belonging to each class.

For example, consider that there are 98% samples of class A and 2% samples of class B in our training set. Then our model can easily get  **98% training accuracy**  by simply predicting every training sample belonging to class A.

When the same model is tested on a test set with 60% samples of class A and 40% samples of class B, then the  **test accuracy would drop down to 60%.**Classification Accuracy is great, but gives us the false sense of achieving high accuracy.

The real problem arises, when the cost of misclassification of the minor class samples are very high. If we deal with a rare but fatal disease, the cost of failing to diagnose the disease of a sick person is much higher than the cost of sending a healthy person to more tests.

#### **Logarithmic Loss**

> penaliza faltos positivos 
> varia de `[0, ]`

Logarithmic Loss or Log Loss, works by penalising the false classifications. It works well for multi-class classification. When working with Log Loss, the classifier must assign probability to each class for all the samples. Suppose, there are N samples belonging to M classes, then the Log Loss is calculated as below :

$$
LogaritmicLoss = \frac{-1}{N} \sum_{i=1}^N \sum_{j=1}^M y_{ij} * \log (p_{ij})
$$

where,

y_ij, indicates whether sample i belongs to class j or not

p_ij, indicates the probability of sample i belonging to class j

Log Loss has no upper bound and it exists on the range [0, ∞). Log Loss nearer to 0 indicates higher accuracy, whereas if the Log Loss is away from 0 then it indicates lower accuracy.

In general, minimising Log Loss gives greater accuracy for the classifier.

#### **Area Under Curve**

Area Under Curve(AUC) is one of the most widely used metrics for evaluation. It is used for binary classification problem. AUC of a classifier is equal to the probability that the classifier will rank a randomly chosen positive example higher than a randomly chosen negative example. Before defining AUC, let us understand two basic terms :

-   **True Positive Rate (Sensitivity)** : True Positive Rate is defined as  _TP/ (FN+_TP). True Positive Rate corresponds to the proportion of positive data points that are correctly considered as positive, with respect to all positive data points.

$$ 
TruePositiveRate = \frac{TruePositive}{FalseNegative + TruePositive}
$$

-   **False Positive Rate (Specificity)** : False Positive Rate is defined as  _FP / (_FP+TN). False Positive Rate corresponds to the proportion of negative data points that are mistakenly considered as positive, with respect to all negative data points.

$$
FalePositiveRate = \frac{FalsePositive}{FalsePositive + TrueNegative}
$$

False Positive Rate and True Positive Rate both have values in the range [0, 1]. FPR and TPR bot hare computed at threshold values such as (0.00, 0.02, 0.04, …., 1.00) and a graph is drawn. AUC is the area under the curve of plot False Positive Rate vs True Positive Rate at different points in [0, 1].

![](https://cdn-images-1.medium.com/max/1600/1*zFW1Kj3e2X_mmluTW3rVeA.png)

As evident, AUC has a range of [0, 1]. The greater the value, the better is the performance of our model.

#### **F1 Score**

> F1 Score is used to measure a test’s accuracy

**F1 Score is the Harmonic Mean between precision and recall.**

The range for F1 Score is [0, 1]. It tells you how precise your classifier is (how many instances it classifies correctly), as well as how robust it is (it does not miss a significant number of instances).

High precision but lower recall, gives you an extremely accurate, but it then misses a large number of instances that are difficult to classify. The greater the F1 Score, the better is the performance of our model. Mathematically, it can be expressed as :

$$
F1 = 2 * \frac{1}{ \frac{1}{precision} + \frac{1}{recall} }
$$
_F1 Score_

F1 Score tries to find the balance between precision and recall.

##### **Precision :** 

It is the number of correct positive results divided by the number of positive results predicted by the classifier.

$$
Precision = \frac{TruePositives}{TruePositives + FalsePositives}
$$
_Precision_

#####   **Recall :** 

It is the number of correct positive results divided by the number of  **_all_** relevant samples (all samples that should have been identified as positive).

$$
Precision = \frac{TruePositives}{TruePositives + FalseNegatives}
$$
_Recall_

#### MAE - **Mean Absolute Error**

Mean Absolute Error is the average of the difference between the Original Values and the Predicted Values. It gives us the measure of how far the predictions were from the actual output. However, they don’t gives us any idea of the direction of the error i.e. whether we are under predicting the data or over predicting the data. Mathematically, it is represented as :
$$
MeanAbsoluteError = \frac{1}{N} \sum_{j=1}^{N} | y_i - \hat{y}_j |
$$

#### MSE - **Mean Squared Error**

Mean Squared Error(MSE) is quite similar to Mean Absolute Error, the only difference being that MSE takes the average of the  **square** of the difference between the original values and the predicted values. The advantage of MSE being that it is easier to compute the gradient, whereas Mean Absolute Error requires complicated linear programming tools to compute the gradient. As, we take square of the error, the effect of larger errors become more pronounced then smaller error, hence the model can now focus more on the larger errors.
$$
MeanSquaredError = \frac{1}{N} \sum_{j=1}^N (y_j - \hat{y}_j)^2
$$
_Mean Squared Error_

#### KS

K-S or Kolmogorov-Smirnov chart measures performance of classification models. More accurately, K-S is a measure of the degree of separation between the positive and negative distributions. The K-S is 100, if the scores partition the population into two separate groups in which one group contains all the positives and the other all the negatives.

On the other hand, If the model cannot differentiate between positives and negatives, then it is as if the model selects cases randomly from the population. The K-S would be 0. In most classification models the K-S will fall between 0 and 100, and that the higher the value the better the model is at separating the positive from negative cases.

For the case in hand, following is the table :

![KS](https://www.analyticsvidhya.com/wp-content/uploads/2015/01/KS.png)

We can also plot the %Cumulative Good and Bad to see the maximum separation. Following is a sample plot :

![KS_plot](https://www.analyticsvidhya.com/blog/wp-content/uploads/2015/01/KS_plot.png)

The metrics covered till here are mostly used in classification problems. Till here, we learnt about confusion matrix, lift and gain chart and kolmogorov-smirnov chart. Let’s proceed and learn few more important metrics.


### Agrupamentos

#### Elbow

This is one of the most common and technically robust methods. This is based on principle that while clustering performance as measured by _WCSS_ increases (i.e. WCSS decreases) with increase in _k_, rate of increase is usually decreasing. 

>_WCSS_ is the within cluster sum of squares

_Figure 1: Example Cluster Data (with True Clustering)_
![Example Cluster Data (with True Clustering)](https://content.edupristine.com/images/blogs/Beyond_the_k-Means_1.png)


Implicit  **objective function in k-Means**  measures sum of distances of observations from their cluster centroids, called Within-Cluster-Sum-of-Squares (WCSS). This is computed as

$$
WCSS = \sum_{i \in n} (X_i - Y_i)^2
$$

where _Yi_ is centroid for observation _Xi_. By definition, this is geared towards maximizing number of clusters, and in limiting case each data point becomes its own cluster centroid. This is, naturally, neither practical nor desirable. Fig. 2 plots _WCSS_ for k=1.20 and we can see that it continuously drops, indicating more clusters the better!

_Figure 2: WCSS decreasing with k_
![WCSS decreasing with k](https://content.edupristine.com/images/blogs/Beyond_the_k-Means_2.png)

So performance improvement for increasing number of cluster from, say, 3 to 4 is higher than that for increasing from 4 to 5. Plotting _WCSS_ against increasing _k_ can show an ‘elbow’ which demarks significant drop in rate of increase. Selecting number of clusters corresponding to elbow point achieves reasonable performance without having too many clusters. This is still judgmental since what constitutes elbow is visually determined. Further, in practice, there may not be an elbow but smooth curve, or, there may be more than one elbow. Looking at Fig. 2 we note a sharp decrease in rate of decrease in _WCSS_ for 3 clusters. 

Remember that decrease in _WCSS_ means increasing in clustering performance. In this case, this method is able to help us arrive at true number of clusters.

#### Silhueta

**Cluster Quality using Silhouette Coefficient**  – Silhouette coefficient is another quality measure of clustering – and it applies to any clustering, not just k-Means. Silhouette-Coefficient of observation  _i_ is calculated as

$$
S_i = \frac{b-a}{\max(a,b)}
$$

where  _a_  is average distance to all other observations within same cluster as that of observation  _i_ while  _b_ is minimum of average distance to all other observations from all other clusters. Silhouette coefficient of clustering result is average of  _si_ for all observations  _i_. This metric is between +1 representing best clustering and -1 representing worst clustering. While  _WCSS_  is comparable for same data for different  _k_, its number is not comparable across different clustering solutions on different data, and hence doesn’t have absolute threshold. On the other hand, Silhouette Coefficient has fixed range and hence can be used overall metric comparing quality of clustering irrespective of data or number of clusters.

Figure 3: Silhouette Coeff changing with k

  

![Silhouette  Coeff changing with k](https://content.edupristine.com/images/blogs/Beyond_the_k-Means_3.png)

Plotting SC against  _K_  we see highest coefficient of 0.63 with 3 clusters and second highest of 0.60 with 2 clusters. For higher number of clusters, SC sharply drops and stays low. This is because further fragmenting a given cluster makes both  _a_ and  _b_ closer to each other.


## Model Selection

Model selection is the task of selecting a statistical model from a set of candidate models, given data. In the simplest cases, a pre-existing set of data is considered. However, the task can also involve the design of experiments such that the data collected is well-suited to the problem of model selection. Given candidate models of similar predictive or explanatory power, the simplest model is most likely to be the best choice (Occam's razor).

Konishi & Kitagawa (2008, p. 75) state, "The majority of the problems in statistical inference can be considered to be problems related to statistical modeling". Relatedly, Cox (2006, p. 197) has said, "How [the] translation from subject-matter problem to statistical model is done is often the most critical part of an analysis".

Model selection may also refer to the problem of selecting a few representative models from a large set of computational models for the purpose of decision making or optimization under uncertainty

## Feature selection - parameter tunning

### Stepwise regression

#### **Subset Selection**

Nesse método fitamos todas as possibilidades de regressões para todos os p preditores. Ao final olhamos todas as possíveis combinações e escolhemos a com a melhor métrica. Computacionalmente `$$`. Este modelo torna-se computacionalmente inviável, além de que quando p cresce indefinidamente teremos alta variância dos coeficientes, tal como overfitting.

#### **Backward Selection (backward elimination)**

This is the simplest of all variable selection procedures and can be easily implemented without special software. In situations where there is a complex hierarchy, backward elimination can be run manually while taking account of what variables are eligible for removal. 

1. Start with all the predictors in the model 
2. Remove the predictor with highest p-value greater than $α_{crit}$ 
3. Refit the model and goto 2 
4. Stop when all p-values are less than $a_{crit}$ . 

The αcrit is sometimes called the “p-to-remove” and does not have to be 5%. If prediction performance is the goal, then a 15-20% cut-off may work best, although methods designed more directly for optimal prediction should be preferred.

----------

Assim como o Foward, o Backward é viável computacionalmente, pois sua complexidade é a`$$` Porém, diferente do Foward, esse método começa com todas as variáveis e vai retirando-as de forma a otimizar a métrica de validação. Esse método no caso precisa que `$$`, pois como se começa com todas as variáveis se `$$` então logo no primeiro modelo o Least Square dará mais de uma solução possível.

#### **Foward Selection**

This just reverses the backward method. 

1. Start with no variables in the model. 
2. For all predictors not in the model, check their p-value if they are added to the model. Choose the one with lowest p-value less than $α_{crit}$ . 
3. Continue until no new predictors can be added.

-----

Diferente do subset selection esse modelo é computacionalmente viável para p com grandes valores, pois sua complexidade computacional é 1 + p(p+1)/2. Esse método começa com apenas uma variável e vai até p para cada vez que há um incremento no modelo em alguma métrica, seja RSS ou `$$`. Esse método pode ser usado em casos em que `$$` porém, será possível apenas construir modelo até `$$`, como o método utilizado aqui é o Least Square e o mesmo não irá dar uma solução única quando `$$`.

#### Stepwise Regression

This is a combination of backward elimination and forward selection. This addresses the situation where variables are added or removed early in the process and we want to change our mind about them later. At each stage a variable may be added or removed and there are several variations on exactly how this is done.

Stepwise procedures are relatively cheap computationally but they do have some drawbacks.

1. Because of the “one-at-a-time” nature of adding/dropping variables, it’s possible to miss the “optimal” model. 
2. The p-values used should not be treated too literally. There is so much multiple testing occurring that the validity is dubious. The removal of less significant predictors tends to increase the significance of the remaining predictors. This effect leads one to overstate the importance of the remaining predictors. 
3. The procedures are not directly linked to final objectives of prediction or explanation and so may not really help solve the problem of interest. With any variable selection method, it is important to keep in mind that model selection cannot be divorced from the underlying purpose of the investigation. Variable selection tends to amplify the statistical significance of the variables that stay in the model. Variables that are dropped can still be correlated with the response. It would be wrong to say these variables are unrelated to the response, it’s just that they provide no additional explanatory effect beyond those variables already included in the model. 
4. Stepwise variable selection tends to pick models that are smaller than desirable for prediction purposes. To give a simple example, consider the simple regression with just one predictor variable. Suppose that the slope for this predictor is not quite statistically significant. We might not have enough evidence to say that it is related to y but it still might be better to use it for predictive purposes.

### Evaluating model fit

#### Regression Coefficient

Coefficients are the numbers by which the variables in an equation are multiplied. For example, in the equation y = -3.6 + 5.0X1  - 1.8X2, the variables X1  and X2  are multiplied by 5.0 and -1.8, respectively, so the coefficients are 5.0 and -1.8.

The size and sign of a coefficient in an equation affect its graph. In a simple linear equation (contains only one x variable), the coefficient is the slope of the line.

![](https://support.minitab.com/en-us/minitab-express/1/scatterplot_linear_equation_2_plus_5x.png)

The coefficient (and slope) is positive 5.

![](https://support.minitab.com/en-us/minitab-express/1/scatterplot_quad_equation_1_plus_2xsquared_minus_3x.png)

##### How Do I Interpret the Regression Coefficients for Linear Relationships?

Regression coefficients represent the mean change in the response variable for one unit of change in the predictor variable while holding other predictors in the model constant. This  [statistical control](http://blog.minitab.com/blog/adventures-in-statistics/a-tribute-to-regression-analysis)  that regression provides is important because it isolates the role of one variable from all of the others in the model.

The key to understanding the coefficients is to think of them as slopes, and they’re often called slope coefficients. I’ll illustrate this in the fitted line plot below, where I’ll use a person’s height to model their weight. First, Minitab’s session window output:

![Coefficients table for regression analysis](https://blog.minitab.com/hubfs/Imported_Blog_Media/swo_weight_height.gif?t=1541179087743)

The fitted line plot shows the same regression results graphically.

![Fitted line plot of weight by height](https://blog.minitab.com/hubfs/Imported_Blog_Media/flp_weight_height.gif?t=1541179087743)

The equation shows that the coefficient for height in meters is 106.5 kilograms. The coefficient indicates that for every additional meter in height you can expect weight to increase by an average of 106.5 kilograms.

The blue fitted line graphically shows the same information. If you move left or right along the x-axis by an amount that represents a one meter change in height, the fitted line rises or falls by 106.5 kilograms. However, these heights are from middle-school aged girls and range from 1.3 m to 1.7 m. The relationship is only valid within this data range, so we would not actually shift up or down the line by a full meter in this case.

If the fitted line was flat (a slope coefficient of zero), the expected value for weight would not change no matter how far up and down the line you go. So, a low p-value suggests that the slope is not zero, which in turn suggests that changes in the predictor variable are associated with changes in the response variable.

I used a fitted line plot because it really brings the math to life. However, fitted line plots can only display the results from simple regression, which is one predictor variable and the response. The concepts hold true for multiple linear regression, but I would need an extra spatial dimension for each additional predictor to plot the results. That's hard to show with today's technology!

#### P-Value

> P-value is the observed significance level

Hypothetical frequency called the P-value, also known as the “observed significance level” for the test hypothesis. The traditional definition of P-value and statistical significance has revolved around null hypotheses, and we treat all other assumptions that are used to calculate P-value as if they are all correct. As we are not sure about these assumptions, we will learn about a more general view of the P-value as a statistical summary of the compatibility between the observed data and what we would predict or expect to see if we knew the entire statistical model were correct.

The distance between the data and the model prediction is measured using a test statistic (such as a t-statistic or a chi-squared statistic). The P-value is then the probability that the chosen test statistic would have been at least as large  
as its observed value if every model assumption were correct, including the test hypothesis. This definition embodies a crucial point lost in traditional definitions: In logical terms, the P-value tests all the assumptions about how the data were generated (the entire model), not just the targeted hypothesis it is supposed to test (such as a null hypothesis).

By getting a small P-value we can say that the data is more unusual if all the assumptions are correct; but a very small P-value does not tell us anything about the assumptions validity. Lets take an example, when a P-value is very small because of the false hypothesis target; but it can be small because of the study protocol violation, or maybe it was analyzed with incorrect data. And Conversely, a large P-value indicates that the data are not unusual under the statistical model, but it does not tell us anything about the model validity and assumptions. It can be large because of the study protocol violation, or maybe it was analyzed with incorrect data or just for presentation purpose to make a valid point.

A best way to build a good statistical model is by calculating confidence intervals and nowadays many journals requires confidence intervals.

##### How Do I Interpret the P-Values in Linear Regression Analysis?

The p-value for each term tests the null hypothesis that the coefficient is equal to zero (no effect). A low p-value (< 0.05) indicates that you can reject the null hypothesis. In other words, a predictor that has a low p-value is likely to be a meaningful addition to your model because changes in the predictor's value are related to changes in the response variable.

Conversely, a larger (insignificant) p-value suggests that changes in the predictor are not associated with changes in the response.

In the output below, we can see that the predictor variables of South and North are significant because both of their p-values are 0.000. However, the p-value for East (0.092) is greater than the common alpha level of 0.05, which indicates that it is not statistically significant.

![Table with regression p-values](https://blog.minitab.com/hubfs/Imported_Blog_Media/regr_p_values.gif?t=1541179087743)

Typically, you use the coefficient p-values to determine which terms to keep in the regression model. In the model above, we should consider removing East.

#### **AIC**

The Akaike information criterion (AIC) is an estimator of the relative quality of statistical models for a given set of data. Given a collection of models for the data, AIC estimates the quality of each model, relative to each of the other models. Thus, AIC provides a means for model selection.

AIC is founded on information theory. When a statistical model is used to represent the process that generated the data, the representation will almost never be exact; so some information will be lost by using the model to represent the process. AIC estimates the relative information lost by a given model: the less information a model loses, the higher the quality of that model. (In making an estimate of the information lost, AIC deals with the trade-off between the goodness of fit of the model and the simplicity of the model.)

##### Definition

Suppose that we have a statistical model of some data. Let k be the number of estimated parameters in the model. Let ${\displaystyle {\hat {L}}}$ be the maximum value of the likelihood function for the model. Then the AIC value of the model is the following.

$${\displaystyle \mathrm {AIC} \,=\,2k-2\ln({\hat {L}})}$$

Given a set of candidate models for the data, the preferred model is the one with the minimum AIC value. Thus, AIC rewards goodness of fit (as assessed by the likelihood function), but it also includes a penalty that is an increasing function of the number of estimated parameters. The penalty discourages overfitting, because increasing the number of parameters in the model almost always improves the goodness of the fit.

AIC is founded in information theory. Suppose that the data is generated by some unknown process f. We consider two candidate models to represent f: g1 and g2. If we knew f, then we could find the information lost from using g1 to represent f by calculating the Kullback–Leibler divergence, $DKL(f ‖ g1)$; similarly, the information lost from using g2 to represent f could be found by calculating $DKL(f ‖ g2)$. We would then choose the candidate model that minimized the information loss.

We cannot choose with certainty, because we do not know f. Akaike (1974) showed, however, that we can estimate, via AIC, how much more (or less) information is lost by g1 than by g2. The estimate, though, is only valid asymptotically; if the number of data points is small, then some correction is often necessary (see AICc, below).

>Note that AIC does not provide a test of a model in the sense of testing a null hypothesis. It tells nothing about the absolute quality of a model, only the quality relative to other models. Thus, if all the candidate models fit poorly, AIC will not give any warning of that.

##### Facts and fallacies of the AIC

> [Hyndsight](https://robjhyndman.com/hyndsight) 3 July 2013
> [forecasting](https://robjhyndman.com/categories/forecasting),  [R](https://robjhyndman.com/categories/r),  [statistic](https://robjhyndman.com/categories/statistics)

Akaike’s Information Criterion (AIC) is a very useful model selection tool, but it is not as well understood as it should be. I frequently read papers, or hear talks, which demonstrate misunderstandings or misuse of this important tool. The following points should clarify some aspects of the AIC, and hopefully reduce its misuse.

1.  The AIC is a penalized likelihood, and so it requires the likelihood to be maximized before it can be calculated. It makes little sense to compute the AIC if estimation is done using something else (e.g., minimizing MAPE). Normally, the residuals are assumed to be Gaussian, and then ML estimates are often (but not always) equivalent to LS estimates. In these cases, computing the AIC after minimizing the MSE is ok.
    
2.  A model selected by the AIC after Gaussian MLE will give predictions equal to the conditional mean. If you then compare the predictions using MAE, or MAPE, or some other criterion, they may not perform well because these other criteria are not optimal for the conditional mean. Match the error measure to the estimation method.
    
3.  The AIC does not assume the residuals are Gaussian. It is just that the Gaussian likelihood is most frequently used. But if you want to use some other distribution, go ahead. The AIC is the penalized likelihood, whichever likelihood you choose to use.
    
4.  The AIC does not require nested models. One of the neat things about the AIC is that you can compare very different models. However, make sure the likelihoods are computed on the same data. For example, you cannot compare an ARIMA model with differencing to an ARIMA model without differencing, because you lose one or more observations via differencing. That is why auto.arima uses a unit root test to choose the order of differencing, and only uses the AIC to select the orders of the AR and MA components.
    
5.  For a similar reason, you cannot compare the AIC from an ETS model with the AIC from an ARIMA model. The two models treat initial values differently. For example, after differencing, an ARIMA model is computed on fewer observations, whereas an ETS model is always computed on the full set of data. Even when the models are equivalent (e.g., an ARIMA(0,1,1) and an ETS(A,N,N)), the AIC values will be different. Effectively, the likelihood of an ETS model is conditional on the initial state vector, whereas the likelihood of a non-stationary ARIMA model is conditional on the first few observations, even when a diffuse prior is used for the nonstationary components.
    
6.  Beware of AIC values computed using conditional likelihoods because the conditioning may be different for different models. Then the AIC values are not comparable.
    
7.  Frequently, the constant term in the AIC is omitted. That is fine for model selection as the constant is the same for all models. But be careful comparing the AIC value between software packages, or between model classes, as they may treat the constant term differently, and then the AIC values are not comparable.
    
8.  The AIC is not really an “in-sample” measure. Yes, it is computed using the training data. But asymptotically, minimizing the AIC is equivalent to minimizing the leave-one-out cross-validation MSE for cross-sectional data, and equivalent to minimizing the out-of-sample one-step forecast MSE for time series models. This property is what makes it such an attractive criterion for use in selecting models for forecasting.
    
9.  The AIC is not a measure of forecast accuracy. Although it has the above cross-validation property, comparing AIC values across data sets is essentially meaningless. If you really want to measure the cross-validated MSE, then you will need to calculate it directly.
    
10.  The AIC is not a consistent model selection method. That does not bother me as I don’t believe there is a true model to be selected. The AIC is optimal (in some senses) for forecasting, and that is much more important in my opinion

------

é um critério definido para uma larga escala de modelos que usam o Maximum Likelihood. (Para o caso de modelos com erros gaussianos o LS é igual ao Maximum Likelihood).

`$$` para modelos com LS o `$$` e o AIC são similares.

#### **BIC**

In statistics, the Bayesian information criterion (BIC) or Schwarz criterion (also SBC, SBIC) is a criterion for model selection among a finite set of models; the model with the lowest BIC is preferred. It is based, in part, on the likelihood function and it is closely related to the Akaike information criterion (AIC).

When fitting models, it is possible to increase the likelihood by adding parameters, but doing so may result in overfitting. Both BIC and AIC attempt to resolve this problem by introducing a penalty term for the number of parameters in the model; the penalty term is larger in BIC than in AIC.

##### Definition

The BIC is formally defined as:

$${\displaystyle \mathrm {BIC} ={\ln(n)k-2\ln({\hat {L}})}.\ }$$

where:

- ${\displaystyle {\hat {L}}}$ = the maximized value of the likelihood function of the model ${\displaystyle M}$, i.e. 
- ${\displaystyle {\hat {L}}=p(x|{\hat {\theta }},M)}$ , where ${\displaystyle {\hat {\theta }}}$ are the parameter values that maximize the likelihood function;
- ${\displaystyle x}$ = the observed data;
- ${\displaystyle n}$ = the number of data points in ${\displaystyle x}$ , the number of observations, or equivalently, the sample size;
- ${\displaystyle k}$ = the number of parameters estimated by the model. For example, in multiple linear regression, the estimated parameters are the intercept, the ${\displaystyle q}$ slope parameters, and the constant variance of the errors; thus, ${\displaystyle k=q+2}$.


------

esse método no caso parte de um ponto de vista Bayesiano. `$$`. Quanto menor o valor, melhor o modelo. Devida a formulação do BIC ele tende a modelos com menos variáveis do que `$$` pois o mesmo penaliza mais modelos com n > 7.

`$$`: Diferente das outra métricas aqui quanto maior o valor melhor o modelo. Essa métrica penaliza o `$$` por incluir variáveis que não melhoram o modelo, como ruídos, assim temos um preço a pagar para cada variável que adicionamos ao modelo.

### EDA - Exploratory Data Analysis

In statistics, exploratory data analysis (EDA) is an approach to analyzing data sets to summarize their main characteristics, often with visual methods. A statistical model can be used or not, but primarily EDA is for seeing what the data can tell us beyond the formal modeling or hypothesis testing task. Exploratory data analysis was promoted by John Tukey to encourage statisticians to explore the data, and possibly formulate hypotheses that could lead to new data collection and experiments. EDA is different from initial data analysis (IDA), which focuses more narrowly on checking assumptions required for model fitting and hypothesis testing, and handling missing values and making transformations of variables as needed. EDA encompasses IDA.

#### Techniques

There are a number of tools that are useful for EDA, but EDA is characterized more by the attitude taken than by particular techniques.

Typical  [graphical techniques](https://en.wikipedia.org/wiki/Statistical_graphics "Statistical graphics")  used in EDA are:

-   [Box plot](https://en.wikipedia.org/wiki/Box_plot "Box plot")
-   [Histogram](https://en.wikipedia.org/wiki/Histogram "Histogram")
-   [Multi-vari chart](https://en.wikipedia.org/wiki/Multi-vari_chart "Multi-vari chart")
-   [Run chart](https://en.wikipedia.org/wiki/Run_chart "Run chart")
-   [Pareto chart](https://en.wikipedia.org/wiki/Pareto_chart "Pareto chart")
-   [Scatter plot](https://en.wikipedia.org/wiki/Scatter_plot "Scatter plot")
-   [Stem-and-leaf plot](https://en.wikipedia.org/wiki/Stemplot "Stemplot")
-   [Parallel coordinates](https://en.wikipedia.org/wiki/Parallel_coordinates "Parallel coordinates")
-   [Odds ratio](https://en.wikipedia.org/wiki/Odds_ratio "Odds ratio")
-   [Targeted projection pursuit](https://en.wikipedia.org/wiki/Targeted_projection_pursuit "Targeted projection pursuit")
-   Glyph-based visualization methods such as PhenoPlot[[9]](https://en.wikipedia.org/wiki/Exploratory_data_analysis#cite_note-9)  and  [Chernoff faces](https://en.wikipedia.org/wiki/Chernoff_face "Chernoff face")
-   [Dimensionality reduction](https://en.wikipedia.org/wiki/Dimensionality_reduction "Dimensionality reduction"):
    -   [Multidimensional scaling](https://en.wikipedia.org/wiki/Multidimensional_scaling "Multidimensional scaling")
    -   [Principal component analysis](https://en.wikipedia.org/wiki/Principal_component_analysis "Principal component analysis")  (PCA)
    -   [Multilinear PCA](https://en.wikipedia.org/wiki/Multilinear_principal_component_analysis "Multilinear principal component analysis")
    -   [Nonlinear dimensionality reduction](https://en.wikipedia.org/wiki/Nonlinear_dimensionality_reduction "Nonlinear dimensionality reduction")  (NLDR)
-   Projection methods such as grand tour, guided tour and manual tour
-   Interactive versions of these plots

Typical  [quantitative](https://en.wikipedia.org/wiki/Quantity "Quantity")  techniques are:

-   [Median polish](https://en.wikipedia.org/wiki/Median_polish "Median polish")
-   [Trimean](https://en.wikipedia.org/wiki/Trimean "Trimean")
-   [Ordination](https://en.wikipedia.org/wiki/Ordination_(statistics) "Ordination (statistics)")

# Termos, Jargoes & Definicoes

### Modelos Deterministicos vs Stocasticos

Um modelo **deterministrico** é um modelo que tem sua variavel resposta tem total relacao com a target, é explicada 100% por elas. Ou seja, dada a formula e as features todas identicas, a variavel resposta sera a mesma. 

$$ y = ax + b $$

Por outro lado, um modelo **estocastico**  acrescenta em sua formulacao fatores/eventos aleatórios, tais como ambiente, clima etc.. Isso implica em dizer que, mesmo que todas as variaveis independentes forem identicas, ao executr o modelo duas vezes possivelmente o resultado sera diferente.

$$ y = ax + b + \text{erro aleatorio} $$

### Curse of Dimensionality (maldicao da dimensionalidade)

** A maldicao da dimensionalidade** se refere a varios fenomenos que surgem quando estamos analizando e organzando dados em um hiper-espaco de altas dimensoes ("high dimensional-space") geralmente com centenas ou milhaes de eixos. 

Dentre os problemas estao:
 - aumentando a dimensionalidade voce aumenta o espaco tao rapidamente (exponencialmente) que seus dados se tornam escasos.
 - Isso torna qualquer metodo que requeira significancia estatistica potencialmente problematico.
 
Uma analogia para tangibilizar:
Imagine que uma crianca vai ate um caminhao de biscoitos com todo tipo de biscoito, desde sabores, cores, formatos, tamanhos, espessuras, ingredientes...

A crianca tem que escolher, mas so pode levar em conta uma caracteristica.
Digamos que ela escolhe sabor, e temos 4 sabores disponiveis. Ela vai testar os 4 sabores e vai escolher qual mais lhe agrada.

$$
custo = n^d\newline
n = \text{exemplos}\newline
d = \text{dimensoes}
$$

Agora imagina que a a crianca vai levar em consideracao os sabores (4 tipos), as cores (4 tipos), os formatos (3 tipos) e as espessuras (5 tipos).

Nesse caso ele vai considerar, para a mesma quantidade de observacoes, $$ 4 * 4 * 3 * 5 = 240 \text{ possibilidades} $$

Isso afeta particularmente algoritmos que dependem de significancia estatistica no hiperespaco para realizar operacoes:
 - knn
 - kmeans
 - funcoes de distancias em geral
 - entre outros..
 
## Metricas de distâncias

### Euclideana

In mathematics, the Euclidean distance or Euclidean metric is the "ordinary" straight-line distance between two points in Euclidean space. With this distance, Euclidean space becomes a metric space. The associated norm is called the Euclidean norm. Older literature refers to the metric as the Pythagorean metric. A generalized term for the Euclidean norm is the L2 norm or L2 distance.

$${\displaystyle {\begin{aligned}d(\mathbf {p} ,\mathbf {q} )=d(\mathbf {q} ,\mathbf {p} )&={\sqrt {(q_{1}-p_{1})^{2}+(q_{2}-p_{2})^{2}+\cdots +(q_{n}-p_{n})^{2}}}\\[8pt]&={\sqrt {\sum _{i=1}^{n}(q_{i}-p_{i})^{2}}}.\end{aligned}}}$$

### Manhattan

The Manhattan distance between two vectors (or points) a and b is defined as $\Sigma_i |a_i - b_i|$ over the dimensions of the vectors.  
  
This is known as Manhattan distance because all paths from the bottom left to top right of this idealized city have the same distance:  

![](https://qph.fs.quoracdn.net/main-qimg-8d64c8344fc8364e46b9712e2c51dca4.webp)

  
This is also known as the 𝐿1L1 norm because the 𝐿𝑝Lp norm is defined as:  
$$
||x||_p = (|x_i|^p + |x_2|^p + ... + |x_n|^p)^{\frac{1}{p}})
$$
You can see how $p=1$ and $x=a-b$ leads to the first formula.

### Minkowski

The Minkowski distance is a metric in a normed vector space which can be considered as a generalization of both the Euclidean distance and the Manhattan distance.

The Minkowski distance of order p between two points

$${\displaystyle X=(x_{1},x_{2},\ldots ,x_{n}){\text{ and }}Y=(y_{1},y_{2},\ldots ,y_{n})\in \mathbb {R} ^{n}} X=(x_{1},x_{2},\ldots ,x_{n}){\text{ and }}Y=(y_{1},y_{2},\ldots ,y_{n})\in {\mathbb  {R}}^{n}$$

is defined as:

$${\displaystyle D\left(X,Y\right)=\left(\sum _{i=1}^{n}|x_{i}-y_{i}|^{p}\right)^{1/p}} {\displaystyle D\left(X,Y\right)=\left(\sum _{i=1}^{n}|x_{i}-y_{i}|^{p}\right)^{1/p}}$$

For ${\displaystyle p\geq 1}, the Minkowski distance is a metric as a result of the Minkowski inequality. When ${\displaystyle p<1}$, the distance between (0,0) and (1,1) is ${\displaystyle 2^{1/p}>2}$, but the point (0,1) is at a distance 1 from both of these points. Since this violates the triangle inequality, for ${\displaystyle p<1}$ it is not a metric.


### Mahalanobis

### Jaccard

### ShivChev

# Resumo

## Pros e contras

| Algorithm               | Best at                                                                     | Pros                                                                                                                                                                     | Cons                                                                                              | 
|-------------------------|-----------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------| 
| Arvores de Decisao      | classificacao de estrelas / galaxias                                        | rapido                                                                                                                                                                   | arvores complexas são dificeis de interpretar                                                     | 
|                         | diagnostico medico                                                          | robusta para "noise" e valores missing                                                                                                                                   | eé possivel duplicar uma sub-arvore dentro de uma mesma estrutura de arvore                       | 
|                         | analise de credito                                                          | eé bastante acurada (precisa)                                                                                                                                            | pode overfitar facilmente                                                                         | 
|                         |                                                                             | eé um modelo considerado "non-parametric" isso eé, ele não assume nenhuma caracteristica dos dados (exemplo: que seja distribuicao normal, que tenha linearidade, etc..) |                                                                                                   | 
|                         |                                                                             | no distribution requirement                                                                                                                                              |                                                                                                   | 
|                         |                                                                             | eé um metodo heiristico (isso eé, ele aprende na pratica consigo mesmo)                                                                                                  |                                                                                                   | 
|                         |                                                                             | não sofre com multicolineraidade (parametros correlacionados)                                                                                                            |                                                                                                   | 
| regressao linear        | criar predicoes de baseline                                                 | facil de entender e de explicar                                                                                                                                          | muito complicado para fitar em funcoes não lineares                                               | 
|                         | predicoes econometricas                                                     | raramente overfita (It seldom overfits)                                                                                                                                  | sofre com outliers                                                                                | 
|                         | modelar respostas de marketing                                              | usando L1 e L2 eé bom com selecao de features (feature selectin)                                                                                                         | pontos de alta influencia                                                                         | 
|                         |                                                                             | rapido para treinar                                                                                                                                                      | assume a linearidade da funcao & normalidade dos dados                                            | 
|                         |                                                                             | facil de treinar em big data gracas a sua versao estocastica                                                                                                             | assume que os erros nao sao correlacionados                                                       |
| regressao local (LOESS) | fitar uma curva linear                                                      | simples de rodar                                                                                                                                                         | nao tem uma formula especifica, o que dificulta a transfrencia                                    | 
|                         |                                                                             | capaz que fitar curvas complexas                                                                                                                                         | < tem uma analise mais superficial sobre os dados e aumenta consideravelmente o custo da funcao   | 
|                         |                                                                             |                                                                                                                                                                          | necessita de uma quantidade grande de dados para produzir resultados bons                         | 
| regressao logistica     | ordenar resultados por probabilidade                                        | facil entender e facil de explicar                                                                                                                                       | muito complicado para fitar em funcoes não lineares                                               | 
|                         | modelar respostas de marketing                                              | raramente overfita (It seldom overfits)                                                                                                                                  | sofre com outliers                                                                                | 
|                         |                                                                             | usando L1 e L2 eé bom com selecao de features (feature selectin)                                                                                                         |                                                                                                   | 
|                         |                                                                             | um dos melhores para prever probabilidade de um resultado                                                                                                                |                                                                                                   | 
|                         |                                                                             | rapido para treinar                                                                                                                                                      |                                                                                                   | 
|                         |                                                                             | facil de treinar em big data gracas a sua versao estocastica                                                                                                             |                                                                                                   | 
| Naive Bayes             | reconhecimento facial                                                       | rapido de implementar                                                                                                                                                    | assume a independencia das variaveis de maneira muito forte (por isso o nome naive - ingennuo)    | 
|                         | analise de sentimento                                                       | facil de entender                                                                                                                                                        | falha ao tentar estimar ocorrencias raras                                                         | 
|                         | deteccao de spam                                                            | leva enconta o conhecimento previo                                                                                                                                       | sofre com features irrelevantes                                                                   | 
|                         | classificacao de texto                                                      | pode ser usado para aprendizado online                                                                                                                                   |                                                                                                   | 
| K-nearest Neighbors     | visao computacional                                                         | treino rapido e preguicoso (basicamente ele armazena a base)                                                                                                             | predicao lenta e morosa (porque sempre roda a base de treino inteira)                             | 
|                         | tageamento de multilabel                                                    | Pode facilmente lidar com problemas complexos de multiclasse                                                                                                             | pode falhar ao prever corretamente, devido maldicao da dimensionalidade (curse of dimencionality) | 
|                         | sistemas de recomendacao                                                    |                                                                                                                                                                          |                                                                                                   | 
|                         | probelmas de spll checking                                                  |                                                                                                                                                                          |                                                                                                   | 
| K-means                 | segmentacao                                                                 | rapido para encontrar clusters                                                                                                                                           | sofre com multicolinearidade                                                                      | 
|                         |                                                                             | consegue detectar outliers em um hiper-espaco multidimensional                                                                                                           | clusters são sempre esfericos, não consegue detectar outro tipo de formato                        | 
|                         |                                                                             |                                                                                                                                                                          | solucoes instaveis, depende muito da sua inicializacao                                            | 
| Support Vector Machines | reconhecimento de caracteres                                                | criacao automatica de features não lineares                                                                                                                              | dificil de interpretar quando aplicado kernels não lineares                                       | 
|                         | reconheciento de imagens                                                    | consege resolver complexas funcoes nao lineares                                                                                                                          | depois de 10 000 exemplos ele sofre de morosidade por ser muito custoso computacionalmente        | 
|                         | classificacao de textos                                                     |                                                                                                                                                                          |                                                                                                   | 
| Random Forest           | apto para qualquer tipo de problema de machine learning                     | consegue trabalhar paralelizado                                                                                                                                          | dificil de interpretar                                                                            | 
|                         | Bioinformatics                                                              | raramente overfita (It seldom overfits)                                                                                                                                  | eé mais fraco para prever valores de calda da distribuicao                                        | 
|                         |                                                                             | lida automaticamente com missing value                                                                                                                                   | em problemas de multiclass ele enviesa em direcao as classes mais frequentes                      | 
|                         |                                                                             | Não precisa transformar nenhuma variavel                                                                                                                                 |                                                                                                   | 
|                         |                                                                             | Não preicsa ficar mexendo nos parametros do modelo                                                                                                                       |                                                                                                   | 
|                         |                                                                             | Pode ser usado facilmente e vai ter excelentes resultados                                                                                                                |                                                                                                   | 
| Gradient Boosting       | Apto para qualquer tipo de problema de machine learning                     | Consegue aproximar a maioria dos problemas não lineares                                                                                                                  | Se executar muitas vezes ele overfita facilmente                                                  | 
|                         | mecanismos de busca (resolvendo problemas de aprendizagem para rankeamento) | O melhor para prever classes                                                                                                                                             | sensível a noisy data e outliers                                                                  | 
|                         |                                                                             | automaticamente lida com missing values                                                                                                                                  | Não funciona legal sem parameter tunning                                                          | 
|                         |                                                                             | Não precisa transformar nenhuma variavel                                                                                                                                 |                                                                                                   | 
| Adaboost                | detecao de faces                                                            | automaticamente lida com missing values                                                                                                                                  | sensível a noisy data e outliers                                                                  | 
|                         |                                                                             | não precisa transformar nenhuma variavel                                                                                                                                 | nunca eé o melhor para prever classes                                                             | 
|                         |                                                                             | não overfita facilmente                                                                                                                                                  |                                                                                                   | 
|                         |                                                                             | tem que mexer em alguns parametros                                                                                                                                       |                                                                                                   | 
|                         |                                                                             | It can leverage many different weak-learners                                                                                                                             |                                                                                                   | 
| Neural Networks         | reconhecimento de imagem                                                    | Pode aproximar qualquer não funcao não linear                                                                                                                            | Muito complicado de construir                                                                     | 
|                         | reconhecimento de linguagem e traducao                                      | robusto a outliers                                                                                                                                                       | muito dificil tunar por conter muitos parametros                                                  | 
|                         | reconhecimento despeech                                                     | Works only with a portion of the examples (the support vectors)                                                                                                          | tem que definir a arquitetura da rede                                                             | 
|                         | Vision recognition                                                          |                                                                                                                                                                          | dificil interpretar                                                                               | 
|                         |                                                                             |                                                                                                                                                                          | overfita facilmente                                                                               | 
| SVD                     | sistemas de recomendacao                                                    | pode reconstruir dados de uma maneira que faca sentido                                                                                                                   | dificil de entender porque ele reconstroi de certa forma os dados                                 | 
| PCA                     | remover colinearidades                                                      | pode reduzir a colinearidade                                                                                                                                             | implica em forte suposicoes lineares (componentes são somas ponderadas das features)              | 
|                         | reduz a dimensionalidade do dataset                                         |                                                                                                                                                                          |                                                                                                   | 
| MSE                     | mensurar error de modelos de forma simplificada                             | util quando se tem valores que nao esperamos e devemos prestar atencao (outliers)                                                                                        | a metrica costuma overestimar o quao ruim um modelo foi.                                          | 
|                         |                                                                             |                                                                                                                                                                          | nao existe uma explicacao para o numero retornado                                                 | 
|                         |                                                                             |                                                                                                                                                                          | nao tem um limite                                                                                 | 
|                         |                                                                             |                                                                                                                                                                          | dependendo da escala da base (presenca de outliers) o resultado sera distorcido                   | 
|                         |                                                                             |                                                                                                                                                                          | ela nao evalua se todas as features estao coerentes                                               | 
| RMSE                    | mensurar erro de modelos de forma simplificada                              | mesma do MSE porem com a escala trazida de volta para a original                                                                                                         | a metrica costuma overestimar o quao ruim um modelo foi.                                          | 
|                         |                                                                             |                                                                                                                                                                          | nao existe uma explicacao para o numero retornado                                                 | 
|                         |                                                                             |                                                                                                                                                                          | nao tem um limite                                                                                 | 
|                         |                                                                             |                                                                                                                                                                          | dependendo da escala da base (presenca de outliers) o resultado sera distorcido                   | 
|                         |                                                                             |                                                                                                                                                                          | ela nao evalua se todas as features estao coerentes                                               | 
| MAE                     | mensurar errors desconsiderando outliers                                    | e robusto a outliers                                                                                                                                                     | nao existe uma explicacao para o numero retornado                                                 | 
|                         | muito usado em financas                                                     | penaliza menos os erros muito grandes em relacao ao mse                                                                                                                  | nao tem um limite                                                                                 | 
|                         |                                                                             |                                                                                                                                                                          | ela nao evalua se todas as features estao coerentes                                               | 
| r squared               | evaluer o fit de um modelo de forma geral                                   | facil entender                                                                                                                                                           | [-infinite, 1] varia o range da metrica                                                           |
|                         |                                                                             | facil explicar                                                                                                                                                           | ela nao evalua se todas as features estao coerentes                                               | 
|                         |                                                                             | compara o resultado do modelo com o resultado de um modelo aleatorio                                                                                                     |                                                                                                   | 
| adjusted r squared      | evalua as features tambem                                                   | tudo que o r squared tem                                                                                                                                                 |                                                                                                   | 
|                         |                                                                             | evalua as features que nao explicam o target                                                                                                                             |                                                                                                   | 
|                         |                                                                             | evita o overfit (por penalizar as features)                                                                                                                              |                                                                                                   | 
| mspe (MSE percentage)   | predicting what mse predicts                                                | tudo que o mae possui                                                                                                                                                    | por usar as porcentagens ele cria um peso muito alto classes muito raras (penaliza muito)         | 
|                         |                                                                             | cria pesos no MSE original - traz o resultado para uma metrica comparavel                                                                                                |                                                                                                   | 
| mape (MAE percentage)   | predicting what mae predicts                                                | tudo que o mae possui                                                                                                                                                    | por usar as porcentagens ele cria um peso muito alto classes muito raras (penaliza muito)         | 
|                         |                                                                             | cria pesos no MAE original - traz o resultado para uma metrica comparavel                                                                                                |                                                                                                   | 
|                         |                                                                             |                                                                                                                                                                          |                                                                                                   | 
