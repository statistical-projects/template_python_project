{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Bootstrap Confidence Intervals"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Calculating confidence intervals with the bootstrap involves two steps:\n",
    "- Calculate a Population of Statistics\n",
    "- Calculate Confidence Intervals"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1. Calculate a Population of Statistics\n",
    "\n",
    "The first step is to use the bootstrap procedure to resample the original data a number of times and calculate the statistic of interest.\n",
    "\n",
    "The dataset is sampled with replacement. This means that each time an item is selected from the original dataset, it is not removed, allowing that item to possibly be selected again for the sample.\n",
    "\n",
    "The statistic is calculated on the sample and is stored so that we build up a population of the statistic of interest.\n",
    "\n",
    "The number of bootstrap repeats defines the **variance** of the estimate, and **more is better**, often hundreds or thousands.\n",
    "\n",
    "We can demonstrate this step with the following pseudocode.\n",
    "\n",
    "```` python\n",
    "statistics = []\n",
    "for i in bootstraps:\n",
    "\tsample = select_sample_with_replacement(data)\n",
    "\tstat = calculate_statistic(sample)\n",
    "\tstatistics.append(stat)\n",
    "````"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2. Calculate Confidence Interval\n",
    "Now that we have a population of the statistics of interest, we can calculate the confidence intervals.\n",
    "\n",
    "This is done by first ordering the statistics, then selecting values at the chosen percentile for the confidence interval. The chosen percentile in this case is called **alpha**.\n",
    "\n",
    "For example, if we were interested in a confidence interval of 95%, then alpha would be `0.95` and we would select the value at the 2.5% percentile as the lower bound and the 97.5% percentile as the upper bound on the statistic of interest.\n",
    "\n",
    "For example, if we calculated 1,000 statistics from 1,000 bootstrap samples, then the lower bound would be the 25th value and the upper bound would be the 975th value, assuming the list of statistics was ordered.\n",
    "\n",
    "In this, we are calculating a non-parametric confidence interval that does not make any assumption about the functional form of the distribution of the statistic. This confidence interval is often called the **empirical confidence interval**.\n",
    "\n",
    "We can demonstrate this with pseudocode below.\n",
    "\n",
    "````python\n",
    "ordered = sort(statistics)\n",
    "lower = percentile(ordered, (1-alpha)/2)\n",
    "upper = percentile(ordered, alpha+((1-alpha)/2))\n",
    "````"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Bootstrap Model Performance\n",
    "\n",
    "**The bootstrap can be used to evaluate the performance of machine learning algorithms.**\n",
    "\n",
    "The size of the sample taken each iteration may be limited to 60% or 80% of the available data. This will mean that there will be some samples that are not included in the sample. These are called **out of bag (OOB)** samples.\n",
    "\n",
    "A model can then be trained on the data sample each bootstrap iteration and evaluated on the out of bag samples to give a performance statistic, which can be collected and from which confidence intervals may be calculated.\n",
    "\n",
    "We can demonstrate this process with the following pseudocode.\n",
    "\n",
    "````python\n",
    "statistics = []\n",
    "for i in bootstraps:\n",
    "\ttrain, test = select_sample_with_replacement(data, size)\n",
    "\tmodel = train_model(train)\n",
    "\tstat = evaluate_model(test)\n",
    "\tstatistics.append(stat)\n",
    "````"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Calculate Classification Accuracy Confidence Interval\n",
    "\n",
    "This section demonstrates how to use the bootstrap to calculate an empirical confidence interval for a machine learning algorithm on a real-world dataset using the Python machine learning library scikit-learn.\n",
    "\n",
    "This section assumes you have Pandas, NumPy, and Matplotlib installed. If you need help setting up your environment, see the tutorial:\n",
    "\n",
    "[How to Setup a Python Environment for Machine Learning and Deep Learning with Anaconda](http://machinelearningmastery.com/setup-python-environment-machine-learning-deep-learning-anaconda/)\n",
    "\n",
    "First, download the [Pima Indians dataset](https://raw.githubusercontent.com/jbrownlee/Datasets/master/pima-indians-diabetes.data.csv) and place it in your current working directory with the filename “pima–indians-diabetes.data.csv” (update: download here).\n",
    "\n",
    "We will load the dataset using Pandas."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([[  6.   , 148.   ,  72.   , ...,   0.627,  50.   ,   1.   ],\n",
       "       [  1.   ,  85.   ,  66.   , ...,   0.351,  31.   ,   0.   ],\n",
       "       [  8.   , 183.   ,  64.   , ...,   0.672,  32.   ,   1.   ],\n",
       "       ...,\n",
       "       [  5.   , 121.   ,  72.   , ...,   0.245,  30.   ,   0.   ],\n",
       "       [  1.   , 126.   ,  60.   , ...,   0.349,  47.   ,   1.   ],\n",
       "       [  1.   ,  93.   ,  70.   , ...,   0.315,  23.   ,   0.   ]])"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "import random\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "\n",
    "# load dataset\n",
    "data = pd.read_csv('pima-indians-diabetes.data.csv', header=None)\n",
    "values = data.values\n",
    "\n",
    "data.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we will configure the bootstrap. We will use 1,000 bootstrap iterations and select a sample that is 50% the size of the dataset."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "# configure bootstrap\n",
    "n_iterations = 1000\n",
    "n_size = int(len(data) * 0.50)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we will iterate over the bootstrap.\n",
    "\n",
    "The sample will be selected with replacement using the resample() function from sklearn. Any rows that were not included in the sample are retrieved and used as the test dataset. Next, a decision tree classifier is fit on the sample and evaluated on the test set, a classification score calculated, and added to a list of scores collected across all the bootstraps."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.utils import resample\n",
    "from sklearn.tree import DecisionTreeClassifier\n",
    "from sklearn.metrics import accuracy_score\n",
    "\n",
    "# run bootstrap\n",
    "stats = list()\n",
    "for i in range(n_iterations):\n",
    "    # prepare train and test sets\n",
    "    train = resample(values, n_samples=n_size)\n",
    "    test = np.array([x for x in values if x.tolist() not in train.tolist()])\n",
    "    # fit model\n",
    "    model = DecisionTreeClassifier()\n",
    "    model.fit(train[:,:-1], train[:,-1])\n",
    "    # evaluate model\n",
    "    predictions = model.predict(test[:,:-1])\n",
    "    score = accuracy_score(test[:,-1], predictions)\n",
    "    #print(score)\n",
    "    stats.append(score)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once the scores are collected, a histogram is created to give an idea of the distribution of scores. We would generally expect this distribution to be Gaussian, perhaps with a skew with a symmetrical variance around the mean.\n",
    "\n",
    "Finally, we can calculate the empirical confidence intervals using the [percentile() NumPy function](https://docs.scipy.org/doc/numpy-dev/reference/generated/numpy.percentile.html). A 95% confidence interval is used, so the values at the `2.5` and 97.5 percentiles are selected.\n",
    "\n",
    "Putting this all together, the complete example is listed below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAXoAAAD8CAYAAAB5Pm/hAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAADl0RVh0U29mdHdhcmUAbWF0cGxvdGxpYiB2ZXJzaW9uIDIuMi4yLCBodHRwOi8vbWF0cGxvdGxpYi5vcmcvhp/UCwAADgJJREFUeJzt3X+s3fVdx/Hna/yY0bmstRdS+bGLppgxE7t5Q9BlSw3R8SNa/AMGia4sxJqsMy7RJdWYzMwsqZq5SDZJuoDrjMDI3ARDFWrdgi6gFGH8qowOKnRtaAfbHCFRYW//ON/rLpfbntNz7rnn3A/PR3JyvudzPuf7fb9z+331e7/nnO9NVSFJatcbJl2AJGm8DHpJapxBL0mNM+glqXEGvSQ1zqCXpMYZ9JLUOINekhpn0EtS406ddAEA69atq9nZ2UmXIUmrygMPPPCtqprpN28qgn52dpZ9+/ZNugxJWlWS/Ocg8zx1I0mNM+glqXEGvSQ1zqCXpMYZ9JLUOINekhpn0EtS4wx6SWqcQS9JjZuKb8ZK/cxuv3Ni2z644/KJbVtaDh7RS1LjPKKX+pjUbxP+JqHl4hG9JDXOoJekxhn0ktQ4g16SGmfQS1LjDHpJapxBL0mNM+glqXEGvSQ1rm/QJzknyZeT7E/yWJLf7sbXJtmT5Mnufk03niTXJzmQ5OEk7xx3E5Kk4xvkiP5l4Heq6m3ARcC2JBcA24G9VbUB2Ns9BrgU2NDdtgI3LHvVkqSB9Q36qjpSVf/eLX8P2A+cBWwGdnXTdgFXdMubgc9Vz33AW5KsX/bKJUkDOalz9ElmgXcA/wqcWVVHoPefAXBGN+0s4NkFLzvUjUmSJmDgoE/yJuBvgA9X1X+daOoSY7XE+rYm2Zdk37FjxwYtQ5J0kgYK+iSn0Qv5v66qL3bDz82fkunuj3bjh4BzFrz8bODw4nVW1c6qmququZmZmWHrlyT1McinbgLcCOyvqj9b8NQdwJZueQtw+4Lx93efvrkI+O78KR5J0sob5A+PvAv4deCRJA91Y78P7ABuS3Id8AxwZffcbuAy4ADwEvCBZa1YknRS+gZ9Vf0LS593B7h4ifkFbBuxLknSMvGbsZLUOINekhpn0EtS4wx6SWqcQS9JjTPoJalxg3yOXtIEzG6/cyLbPbjj8olsV+PjEb0kNc6gl6TGGfSS1DiDXpIaZ9BLUuMMeklqnEEvSY0z6CWpcQa9JDXOoJekxhn0ktQ4g16SGmfQS1LjDHpJapxBL0mNM+glqXEGvSQ1zqCXpMYZ9JLUOINekhpn0EtS4wx6SWqcQS9JjTPoJalxBr0kNc6gl6TGGfSS1DiDXpIaZ9BLUuMMeklqnEEvSY0z6CWpcX2DPslNSY4meXTB2B8m+WaSh7rbZQue+70kB5I8keS94ypckjSYQY7oPwtcssT4J6tqY3fbDZDkAuBq4O3da/4iySnLVawk6eT1Dfqqugd4YcD1bQZurar/rqqngQPAhSPUJ0ka0Sjn6D+U5OHu1M6abuws4NkFcw51Y5KkCRk26G8AfhLYCBwBPtGNZ4m5tdQKkmxNsi/JvmPHjg1ZhiSpn6GCvqqeq6pXqur7wGf4wemZQ8A5C6aeDRw+zjp2VtVcVc3NzMwMU4YkaQBDBX2S9Qse/iow/4mcO4Crk7wxyXnABuDfRitRkjSKU/tNSHILsAlYl+QQ8FFgU5KN9E7LHAR+E6CqHktyG/A48DKwrapeGU/pkqRB9A36qrpmieEbTzD/48DHRylKkrR8/GasJDXOoJekxhn0ktQ4g16SGmfQS1LjDHpJapxBL0mNM+glqXEGvSQ1zqCXpMYZ9JLUOINekhpn0EtS4wx6SWqcQS9JjTPoJalxBr0kNc6gl6TG9f1TgpJeX2a33zmxbR/ccfnEtt0yj+glqXEGvSQ1zqCXpMYZ9JLUOINekhpn0EtS4wx6SWqcQS9JjTPoJalxBr0kNc6gl6TGGfSS1DiDXpIaZ9BLUuMMeklqnNej10mZ5LXKJQ3HI3pJapxBL0mNM+glqXEGvSQ1rm/QJ7kpydEkjy4YW5tkT5Inu/s13XiSXJ/kQJKHk7xznMVLkvob5Ij+s8Ali8a2A3uragOwt3sMcCmwobttBW5YnjIlScPqG/RVdQ/wwqLhzcCubnkXcMWC8c9Vz33AW5KsX65iJUknb9hz9GdW1RGA7v6Mbvws4NkF8w51Y6+RZGuSfUn2HTt2bMgyJEn9LPebsVlirJaaWFU7q2ququZmZmaWuQxJ0rxhg/65+VMy3f3RbvwQcM6CeWcDh4cvT5I0qmGD/g5gS7e8Bbh9wfj7u0/fXAR8d/4UjyRpMvpe6ybJLcAmYF2SQ8BHgR3AbUmuA54Bruym7wYuAw4ALwEfGEPNkqST0Dfoq+qa4zx18RJzC9g2alGSpOXjN2MlqXEGvSQ1zqCXpMYZ9JLUOINekhpn0EtS4wx6SWqcQS9JjTPoJalxBr0kNc6gl6TGGfSS1DiDXpIaZ9BLUuMMeklqnEEvSY0z6CWpcQa9JDXOoJekxhn0ktQ4g16SGmfQS1LjDHpJapxBL0mNM+glqXEGvSQ1zqCXpMYZ9JLUOINekhpn0EtS4wx6SWqcQS9JjTPoJalxBr0kNc6gl6TGGfSS1DiDXpIaZ9BLUuNOHeXFSQ4C3wNeAV6uqrkka4HPA7PAQeCqqvr2aGVKkoa1HEf0v1BVG6tqrnu8HdhbVRuAvd1jSdKEjOPUzWZgV7e8C7hiDNuQJA1o1KAv4O4kDyTZ2o2dWVVHALr7M0bchiRpBCOdowfeVVWHk5wB7EnyH4O+sPuPYSvAueeeO2IZkqTjGSnoq+pwd380yZeAC4HnkqyvqiNJ1gNHj/PancBOgLm5uRqljteb2e13TroESavI0KdukvxIkh+dXwZ+CXgUuAPY0k3bAtw+apGSpOGNckR/JvClJPPrubmq/iHJ/cBtSa4DngGuHL1MSdKwhg76qnoK+Jklxp8HLh6lKEnS8vGbsZLUOINekhpn0EtS4wx6SWrcqF+YkqRlM6nviBzccflEtrtSPKKXpMYZ9JLUOINekhpn0EtS4wx6SWqcQS9JjTPoJalxBr0kNc6gl6TGGfSS1DiDXpIaZ9BLUuMMeklqnEEvSY3zMsWSXvcmdXlkWJlLJHtEL0mNM+glqXEGvSQ1zqCXpMYZ9JLUOINekhpn0EtS4wx6SWqcX5gawSS/ZCFJg1r1QW/YStKJeepGkhpn0EtS4wx6SWqcQS9JjTPoJalxBr0kNc6gl6TGGfSS1DiDXpIaN7agT3JJkieSHEiyfVzbkSSd2FiCPskpwKeBS4ELgGuSXDCObUmSTmxcR/QXAgeq6qmq+h/gVmDzmLYlSTqBcQX9WcCzCx4f6sYkSStsXFevzBJj9aoJyVZga/fwxSRPnMT61wHfGrK2aWEP08EepkcLfZx0D/njkbb31kEmjSvoDwHnLHh8NnB44YSq2gnsHGblSfZV1dzw5U2ePUwHe5geLfQxrT2M69TN/cCGJOclOR24GrhjTNuSJJ3AWI7oq+rlJB8C7gJOAW6qqsfGsS1J0omN7S9MVdVuYPeYVj/UKZ8pYw/TwR6mRwt9TGUPqar+syRJq5aXQJCkxk1V0A9y2YQkVyV5PMljSW7uxjYmubcbezjJ+1a28tfUOFQfC557c5JvJvnUylS8ZH1D95Dk3CR3J9nfPT+7UnUvqm+UHv6kG9uf5PokS31keOz69ZDkk0ke6m5fT/KdBc9tSfJkd9uyspW/qsahepim/XqUn0P3/GT36aqaihu9N22/AfwEcDrwNeCCRXM2AA8Ca7rHZ3T35wMbuuUfB44Ab1ltfSx4/s+Bm4FPrcYegK8Av9gtvwn44dXUA/DzwFe7dZwC3AtsmsYeFs3/LXoffABYCzzV3a/pltessh6mYr8epYcFYxPdp6fpiH6Qyyb8BvDpqvo2QFUd7e6/XlVPdsuHgaPAzIpV/mpD9wGQ5GeBM4G7V6jepQzdQ3dNo1Orak83/mJVvbRypf+/UX4OBfwQvZ36jcBpwHMrUvWrneylRK4BbumW3wvsqaoXuv72AJeMtdqlDd3DFO3Xo/wcpmKfnqagH+SyCecD5yf5apL7krzmH26SC+ntoN8YW6UnNnQfSd4AfAL4yIpUenyj/CzOB76T5ItJHkzyp91F7lba0D1U1b3Al+kdQR4B7qqq/StQ82IDX0okyVuB84B/OtnXjtkoPSx8bpL79dA9TMs+PbaPVw6h72UT6NW7AdhE79u2/5zkp6tq/pzeeuCvgC1V9f0x1noiQ/cB/Bqwu6qendAp4Xmj9HAq8G7gHcAzwOeBa4Ebx1Tr8YzSwzrgbd0YwJ4k76mqe8ZU6/EM0sO8q4EvVNUrQ7x2nEbpobeCye/Xo/TwQaZgn56moO972YRuzn1V9b/A0+ldH2cDcH+SNwN3An9QVfetRMHHMUofPwe8O8kH6Z3bPj3Ji1W10tfzH6WHQ8CDVfUUQJK/BS5i5YN+lB42deMvAiT5e3o9rHTQD9LDvKuBbYteu2nRa7+yjLUNapQemJL9epQepmOfnsQbA8d5A+NUem8YnccP3vB4+6I5lwC7uuV19H6d+rFu/l7gw6u5j0VzrmVyb8aO8rM4pZs/0z33l8C2VdbD+4B/7NZxWvdv65ensYdu3k8BB+m+F9ONrQWepvdG7Jpuee0q62Eq9utRelj0/MT26ak5R19VLwPzl03YD9xWVY8l+ViSX+mm3QU8n+RxeudQP1JVzwNXAe8Brl3wEaeNE2hj1D6mwig9VO9X1t8F9iZ5hN6vvZ9ZTT0AX6B3LvgRejv116rq76a0B+i9+XdrdWnSvfYF4I/oXXfqfuBj3diKGqUHpmS/HrGHqeA3YyWpcVNzRC9JGg+DXpIaZ9BLUuMMeklqnEEvSY0z6CWpcQa9JDXOoJekxv0fmfoHuB4pzAQAAAAASUVORK5CYII=\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "p min =  2.500000000000002\n",
      "p max =  97.5 97.5 True\n",
      "95.0 confidence interval 0.64454 and 0.73291\n"
     ]
    }
   ],
   "source": [
    "from matplotlib import pyplot\n",
    "\n",
    "# plot scores\n",
    "pyplot.hist(stats)\n",
    "pyplot.show()\n",
    "\n",
    "# confidence interval\n",
    "alpha = 0.95\n",
    "\n",
    "#calculate\n",
    "p_min = ((1.0-alpha)/2.0) * 100\n",
    "print(\"p min = \", p_min)\n",
    "lower = max(min(stats), np.percentile(stats, p_min))\n",
    "p_max = (alpha+((1.0-alpha)/2.0)) * 100\n",
    "p_max_2 = 100-p_min\n",
    "print(\"p max = \", p_max, p_max_2, p_max == p_max_2)\n",
    "upper = min(max(stats), np.percentile(stats, p_max))\n",
    "print('%.1f confidence interval %.5f and %.5f' % (alpha*100, lower, upper))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
